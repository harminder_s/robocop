'''
Created on 20-Apr-2016

@author: nishantmahawar
'''

from coreApi import coreApi
from pgsqlHelper import pgsqlHelper
import json
from config import getdata
from robot.api import logger as trace
from sshbasic import *
from robot.libraries.BuiltIn import BuiltIn
from time import sleep
import random
import sys
import re
import datetime
from multiprocessing import Process, Queue, Lock



#     def add_order(self, host,username="admin",password="apj0702",filename="config_sanity.xls",spreadsheet1="orders", spreadsheet2="orderlines"):
#     def add_order_multiple_orderline(self, host,username="admin",password="apj0702",filename="config_binola.xls",spreadsheet="products_binola", list_tags=['1'], list_quantity=['1']):
#     def pick_front_process(self, host, username="admin", password="apj0702", pick_ppsid='1'):
#     def pick_back_process(self, host, username="admin", password="apj0702", pick_ppsid='1'):
#     def add_bulk_order(self, host,username="admin",password="apj0702",filename="config_binola_PBT.xls",spreadsheet="products_binola", item_tags="1"):
#     def add_exhaustive_pdea(self, host,username="admin",password="apj0702",filename="config_sanity.xls",spreadsheet="pdea"):
#     def add_single_PDEA(self,host,pdea_new_field='vertical',field_data_type='string',username="admin",password="apj0702"): 
#     def add_bulk_exhaustive_pdea(self, host,username="admin",password="apj0702",filename="config_binola.xls",spreadsheet="pdea",item_tags="1"):      
#     def getFirstAndLastElement(self, item_range):
#     def add_sku(self,host,username="admin",password="apj0702",filename="config_sanity.xls",spreadsheet="sku"):
#     def add_bulk_sku(self,host,username="admin",password="apj0702",filename="config_binola.xls",spreadsheet="sku_binola",item_tags="1"):
#     def add_bulk_tote(self,host,username="admin",password="apj0702",filename="config_binola.xls",product_spreadsheet="products_binola", tote_spreadsheet="tote_flow",item_tags="1"):
#     def add_product(self,host,username="admin",password="apj0702",filename="config_sanity.xls",spreadsheet="products"):
#     def add_bulk_product(self,host,username="admin",password="apj0702",filename="config_binola.xls",spreadsheet="products_binola",item_tags="1"):
#     def mock_put(self,host,username="gor",password="apj0702",filename="config_sanity.xls",spreadsheet="products"):
#     def put_product(self,host,username="admin",password="apj0702",filename="config_sanity.xls",spreadsheet="put"):
#     def loginApi(self, host, username="admin", password="apj0702", ppsid="1", pps_side="back"):
#     def processBarcode(self, host, username="admin", password="apj0702", barcode="2001", ppsid="1", pps_side="back"):
#     def cancelScan(self, host, username="admin", password="apj0702", ppsid="1", pps_side="back"):
#     def getPpsSelectedBinId(self, host, username="admin", password="apj0702", ppsid="1", pps_side="back"):
#     def processPpsbinEvent(self, host, username, password, ppsbin_state, ppsbin_event, ppsid, pps_side):
#     def stageAllEvent(self, ppsid, pps_side):
#     def stageOneItem(self, host, username, password, ppsid, pps_side, pps_bin_id=1):   
#     def waitForMSU(self, ppsid, pps_side):
#     def getPpsIdMode(self, host, username="admin", password="apj0702"):
#     def getRackSlotBarcode(self, host, username="admin", password="apj0702", ppsid="1", pps_side="front"):
#     def getProductBarcodeToScan(self, host, username="admin", password="apj0702", ppsid="1", pps_side="front"):
#     def getPpsSeatStatus(self, host, username="admin", password="apj0702", ppsid="1", pps_side="front"):
#     def isScanAllowed(self, host, username="admin", password="apj0702", ppsid="1", pps_side="front"):
#     def getItemUid(self,host, username="admin", password="apj0702", ppsid="1", pps_side="front"):
#     def getKQ(self, host, username="admin", password="apj0702", ppsid="1", pps_side="front"):
#     def updateKQ(self,host, username="admin", password="apj0702", ppsid="1", pps_side="front", qty=1):
#     def getErrorPpsBin(self, host, username="admin", password="apj0702", ppsid="2", pps_side="front"):
#     def clearErrorPpsBin(self, host, username="admin", password="apj0702", ppsid="1", pps_side="back", pps_bin_id='1'):
#     def clearAllErrorPpsBin(self, host, username="admin", password="apj0702", ppsid="1", pps_side="back"):
#     def getEmptyPpsBin(self, host, username="admin", password="apj0702", ppsid="1", pps_side="front"):
#     def getInUsePpsBin(self, host, username="admin", password="apj0702", ppsid="1", pps_side="front"):
#     def getStagedPpsBin(self, host, username="admin", password="apj0702", ppsid="1", pps_side="front"):
#     def getSelectedPpsBin(self, host, username="admin", password="apj0702", ppsid="1", pps_side="front"):
#     def getPpsIdList(self, host, username="admin", password="apj0702"):
#     def getPickProcessedBin(self, host, username="admin", password="apj0702", ppsid="1", pps_side="front"):
#     def changePpsMode(self,host, username="admin", password="apj0702", ppsid="1", new_pps_mode="audit"):
#     def alignPpsMode(self, host, username, password, put, pick, audit):
#     def createUsers(self, host, username, password, ppsid, ppsSide):
#     def getAllUsers(self, host, username, password):
#     def navigateToException(self, host, username, password, ppsid, pps_side, event_name, exception_name):
#     def selectBinForException(self, host, username, password, ppsid, pps_side, event_name, exception_name, evnet_action, bin_id="1"):
#     def confirmQuantityException(self, host, username, password, ppsid, pps_side, event_name, exception_name, evnet_action, quantity=1):
#     def finishException(self, host, username, password, ppsid, pps_side, event_name, exception_name, evnet_action):
#     def cancelException(self, host, username, password, ppsid, pps_side):
#     def finishItemUnscannableException(self, host, username, password, ppsid, pps_side, event_name, exception_name, evnet_action, quantity):
#     def finishDamagedOrMissingException(self, host, username, password, ppsid, pps_side, event_name, exception_name, evnet_action, good_quantity=1, damaged_quantity=1, missing_quantity=1):
#     def put_back_operations(self,host,username="admin",password="apj0702",barcodes="2001",qty=500,ppsid=2,pps_side="back"):
#     def put_front_operations(self,host,username="admin",password="apj0702",ppsid=1,pps_side="front",):
#     def get_current_sku_processing(self, host, username="admin", password="apj0702", ppsid="1", pps_side="front"):
#     def is_system_idle(self, host, username="admin", password="apj0702", ppsid="3", pps_side="front"):
#     def add_single_order(self,host,username="admin",password="apj0702",pdfavalues="product_sku:2001",qty=1):
#     def create_audit(self, host, username="admin", password="apj0702", audit_type = "sku", audit_value = "2001"):
#     def start_audit(self, host, audit_id, username="admin", password="apj0702"):
#     def delete_audit(self, host, audit_id, username="admin", password="apj0702"):
#     def sendAuditEvent(self,host, ppsid="3", pps_side="front", event_data="generate_report", username="admin", password="apj0702"):
#     def verify_sku_audit(self,host="192.168.8.206", ppsside="front", ppsid="3", username="admin", password="apj0702",barcode="product1"):
#     def verifyLocationAudit(self,host="192.168.8.206", ppsid="3", username="admin", password="apj0702",storage_node="%031.1.A.01-02%"):

class apiHelper:

    def add_order(self, host,username="admin",password="apj0702",filename="config_sanity.xls",spreadsheet1="orders", spreadsheet2="orderlines"):
        config_dict_orders = getdata(filename)[spreadsheet1]
        config_dict_orderlines = getdata(filename)[spreadsheet2]
#         order_obj = coreApi(host,username,password)
        if not hasattr(self, "obj"):
            self.obj = coreApi(host,username,password)

        api_url = '/api/orders?sync=false'
        myjson = ""
        order_str = ""
#         order_dict = {}
        orderline_dict = {}
        orders_list = []
        
#         print config_dict_orders
        
        
        for tag,tag_value in config_dict_orderlines.iteritems():
            orderline_str = '''{"orderline_id": "%s", "qty": %d, "filter_parameters": [%s], "preference_params": [{%s}] '''%(tag_value['orderline_id'], int(tag_value['qty']), tag_value['filter_parameters'], tag_value['preference_params'] )
            if(tag_value['pickorder_checklist']=='None'):
                orderline_str = orderline_str + '''"pickorder_checklist": [{%s}]'''%(tag_value['pickorder_checklist'])
            orderline_str = orderline_str + '''}'''
            
            if(tag_value['order_id'] in orderline_dict.keys()):
                orderline_dict[tag_value['order_id']] = orderline_dict[tag_value['order_id']] + ''', %s'''%(orderline_str)
            else:
                orderline_dict[tag_value['order_id']] = orderline_str
#         print orderline_dict
        
        for tag,tag_value in config_dict_orders.iteritems():
            if(tag_value['order_id'] in orderline_dict.keys()):
                random_value = random.randint(0,1000000)
                order_str = order_str + '''{"order_id": "%s-%d", "order_options": {'''%(tag_value['order_id'],random_value)
                trace.info("Added order : %s-%d"%(tag_value['order_id'],random_value), also_console=True)
                orders_list.append("%s-%d"%(tag_value['order_id'],random_value))
                if(tag_value['shipment_type'] != 'NONE'):
                    order_str = order_str + '''"shipment_type": "%s",'''%tag_value['shipment_type']
                if(tag_value['case_pick_count'] != 'NONE'):
                    order_str = order_str + '''"case_pick_count": "%s",'''%tag_value['case_pick_count']
                if(tag_value['item_pick_scan'] != 'NONE'):
                    order_str = order_str + '''"item_pick_scan": "%s",'''%tag_value['item_pick_scan']
                if(tag_value['invoice_needed'] != 'NONE'):
                    order_str = order_str + '''"invoice_needed": "%s",'''%tag_value['invoice_needed']
                if(tag_value['auto_release'] != 'NONE'):
                    order_str = order_str + '''"auto_release": "%s",'''%tag_value['auto_release']
                order_str = order_str.strip(",")
                order_str = order_str + "},"
                
                if(tag_value['priority'] != 'NONE'):
                    order_str = order_str + '''"priority": %d,'''%int(tag_value['priority'])
#                 if(tag_value['packing_materials'] != 'None'):
#                     order_str = order_str + '''"packing_materials": [%s],'''%tag_value['packing_materials']
                if(tag_value['order_date'] != 'NONE'):
                    order_str = order_str + '''"order_date": "%s",'''%tag_value['order_date']
                if(tag_value['pick_before_time'] != 'NONE'):
                    order_str = order_str + '''"pick_before_time": "%s",'''%tag_value['pick_before_time']
                if(tag_value['pick_after_time'] != 'NONE'):
                    order_str = order_str + '''"pick_after_time": "%s",'''%tag_value['pick_after_time']
                   
                order_str = order_str + '''"orderlines": [%s]'''%(orderline_dict[tag_value['order_id']])
                order_str = order_str + '''},'''
#         print order_str 
        order_str = order_str.strip(",")       
        myjson = '''{"orders": [%s]}'''%order_str
        myjson = myjson.encode()
        
        print myjson
        (status, response_json) = self.obj.executeApi(api_url, myjson)
        print "Status :" + str(status)
        print "add_order response :" + str(response_json)
        
        return orders_list

    def add_order_multiple_orderline(self, host,username="admin",password="apj0702",filename="config_binola.xls",spreadsheet="products_binola", list_tags=['1'], list_quantity=['1']):
        config_dict = getdata(filename)[spreadsheet]
        trace.info("In add_order_multiple_orderline, Host: %s, username: %s, password: %s "%(host,username,password), also_console=True)
        
        if not hasattr(self, "obj"):
            self.obj = coreApi(host,username,password)

        api_url = '/api/orders?sync=true'
        orders_json = ""
#        orders = ""
        preference_parameter = '''{"param_name": "order by", "param_value": "product_weight", "extra_key": "ASC"}'''
        random_value = random.randint(0,1000000)
        order_id = "OD%d-%d"%(len(list_tags),random_value)
        orderline_count = 0
        orderlines_json = ""
        
        for item_tag in list_tags:
            orderline_count = orderline_count + 1
            orderline_id = "%s-%d"%(str(order_id), orderline_count)
            quantity = int(list_quantity[orderline_count - 1])
            filter_parameters = ""
            pdfa_fields = config_dict[str(item_tag)]['pdfa_values'].split(",")
            for pdfa_field in pdfa_fields:
                pdfa_key, pdfa_value = (pdfa_field.split(":")[0].strip("\""), pdfa_field.split(":")[1].strip().strip("\""))
                filter_parameters = filter_parameters + '''"%s in ['%s']",'''%(pdfa_key, pdfa_value)
                filter_parameters = filter_parameters.strip(",")
            
            orderlines_json = orderlines_json + '''{"orderline_id": "%s","qty": %d,"filter_parameters": [%s],"preference_params": [%s]},'''%(orderline_id, quantity, filter_parameters, preference_parameter)
         
        orderlines_json = orderlines_json.strip(",")      
        orders_json = orders_json + '''{"orders": [{"order_id": "%s", "orderlines": [%s] }] }'''%(order_id, orderlines_json)
        
        orders_json = orders_json.encode()
        trace.info(orders_json, also_console=True)
        (status, response_json) = self.obj.executeApi(api_url, orders_json)
        trace.info("Status: %s"%(str(status)), also_console=True)
        trace.info("add_order_multiple_orderline response: %s"%(str(response_json)), also_console=True) 
        return order_id       
            
            
    def pick_front_process(self, host, username="admin", password="apj0702", pick_ppsid='1'):
        dict_items = {}
        time = 0
        time_out = 300
        if not hasattr(self, "obj"):
            self.obj = coreApi(host,username,password)
        self.loginApi(host, username, password, ppsid=pick_ppsid, pps_side="front")
        while time < time_out:
            sleep(2)
            scan_allowed_flag = self.isScanAllowed(host, username, password, pick_ppsid, pps_side="front")
            if(scan_allowed_flag):
                time = 0
                sleep(2)
                barcode = self.getProductBarcodeToScan(host,username,password,pick_ppsid, pps_side="front")
                self.processBarcode(host, username, password, barcode, pick_ppsid, pps_side="front")
                sleep(2)
                
                qty = self.getKQ(host, username, password, pick_ppsid, pps_side="front")
                if dict_items.has_key(barcode):
                    picked_qty = dict_items[barcode]
                    dict_items[barcode] = picked_qty + qty
                else:
                    dict_items[barcode] = qty
                

                if qty > 1:
                    self.updateKQ(host, username, password, pick_ppsid, pps_side="front", qty=qty)
                    sleep(2)
                
                pps_bin_id = self.processPpsbinEvent(host, username, password, ppsbin_state="IN USE", ppsbin_event="primary_button_press", ppsid=pick_ppsid, pps_side="front")
                trace.info("PPSID %s: Placed item: %s, PPS Bin ID: %s, Quantity: %d"%(pick_ppsid, barcode, pps_bin_id, qty), also_console=True)
                    
            else:
                trace.info("PPSID %s: No Item available in Pick Front, Sleeping for 5 seconds. Idle Time: %d seconds"%(pick_ppsid, time), also_console=True)
                sleep(2)
                time = time + 5
                    
        trace.info("Items Picked %s"%(str(dict_items)))        
        trace.info("PPSID %s: Successfully Completed Pick Front Operations"%(pick_ppsid), also_console=True)
        return dict_items
        
    def pick_back_process(self, host, username="admin", password="apj0702", pick_ppsid='1'):
        time = 0
        if not hasattr(self, "obj"):
            self.obj = coreApi(host,username,password)
        self.loginApi(host, username, password, ppsid=pick_ppsid, pps_side="back")
        sleep(2)
        pps_bin_id_list = self.getPickProcessedBin(host, username, password, pick_ppsid, pps_side="back")
            
        if(pps_bin_id_list):
            time = 0
            for pps_bin_id in pps_bin_id_list:
                bin_id = self.processPpsbinEvent(host, username, password, ppsbin_state="pick_processed", ppsbin_event="secondary_button_press", ppsid=pick_ppsid, pps_side="back")   
                trace.info("PPSID %s: Removed item from PPS Bin ID: %s"%(pick_ppsid, bin_id), also_console=True)
                sleep(2)
                                        
        else:
            BuiltIn().fail("PPS ID: %s Pick Back Operation Failed"%(pick_ppsid))
#             trace.info("PPSID %s: No Item available in Pick Back, Sleeping for 5 seconds. Idle Time: %d seconds"%(pick_ppsid, time), also_console=True)
#             sleep(5)
#             time = time + 5
                
        trace.info("PPSID %s: Successfully Completed Pick Back Operations"%(pick_ppsid), also_console=True)
        
    

    def add_bulk_order(self, host,username="admin",password="apj0702",filename="config_binola_PBT.xls",spreadsheet="products_binola", item_tags="1"):
        item_list = item_tags.split(",")
        default_batch_size = 100
        current_batch_count = 0
        config_dict = getdata(filename)[spreadsheet]
        
        if not hasattr(self, "obj"):
            self.obj = coreApi(host,username,password)

        api_url = '/api/orders?sync=false'
        max_qty = 5
        orders_json = ""
        orders = ""
        optional_order_parameters=''
        preference_parameter = '''{"param_name": "order by", "param_value": "product_weight", "extra_key": "ASC"}'''
        
        for item_range in item_list:
            if(item_range.lower() == "all"):
                first_item = 1
                last_item = len(config_dict.keys())
            else:
                (first_item, last_item) = self.getFirstAndLastElement(item_range)
            for tag_id in range (first_item,last_item+1):
                current_batch_count = current_batch_count + 1
                random_value = random.randint(0,1000000)
                order_id = "Order-3-%s-%d"%(str(tag_id),random_value)
                orderline_id = "%s-1"%(str(tag_id))
                qty = (tag_id%max_qty)/2 or 1
#                 qty = 2
                filter_parameters = ""
                pdfa_fields = config_dict[str(tag_id)]['pdfa_values'].split(",")
                for pdfa_field in pdfa_fields:
                    pdfa_key, pdfa_value = (pdfa_field.split(":")[0].strip("\""), pdfa_field.split(":")[1].strip().strip("\""))
                    filter_parameters = filter_parameters + '''"%s in ['%s']",'''%(pdfa_key, pdfa_value)
                filter_parameters = filter_parameters.strip(",")
                if(config_dict[str(tag_id)].has_key("pick_before_time")):
                    if(config_dict[str(tag_id)]["pick_before_time"] != None):
                        converted_date = str(config_dict[str(tag_id)]["pick_before_time"]).strip("\"")
                        optional_order_parameters='"pick_before_time": "%s" '%(str(converted_date))
        
                
                if(optional_order_parameters):
                    orders = orders + '''{"order_id": "%s", %s, "orderlines": [{"orderline_id": "%s","qty": %d,"filter_parameters": [%s],"preference_params": [%s]}]},'''%(order_id,optional_order_parameters, orderline_id, qty, filter_parameters, preference_parameter)
                    print orders_json
                else:
                    orders = orders + '''{"order_id": "%s", "orderlines": [{"orderline_id": "%s","qty": %d,"filter_parameters": [%s],"preference_params": [%s]}]},'''%(order_id, orderline_id, qty, filter_parameters, preference_parameter)
             
             
                
                if(current_batch_count ==  default_batch_size or tag_id == last_item):
                    orders = orders.strip(",")
                    orders_json = '''{"orders": [%s]}'''%(orders)
                    orders_json = orders_json.encode()
                    trace.info(orders_json, also_console=True)
                    (status, response_json) = self.obj.executeApi(api_url, orders_json)
                    trace.info("Status: %s"%(str(status)), also_console=True)
                    trace.info("Add_bulk_order_response: %s"%(str(response_json)), also_console=True)
                    orders = ""
                    orders_json = ""
                    current_batch_count = 0
                    
                       
    def add_exhaustive_pdea(self, host,username="admin",password="apj0702",filename="config_sanity.xls",spreadsheet="pdea"):
        
        config_dict = getdata(filename)[spreadsheet]
        if not hasattr(self, "obj"):
            self.obj = coreApi(host,username,password)
        api_url = '/api/exhaustive_pdea'
        pdea_new_fields = ""
    
  
        for tag,tag_value in config_dict.iteritems():
            pdea_new_fields = pdea_new_fields + '''{"field_name":"%s", "field_data_type": "%s"},'''%(tag_value["field_name"],tag_value["field_data_type"])
        pdea_new_fields = pdea_new_fields.strip(",")     
        myjson = '''{"exhaustive_pdea": [%s]}'''%pdea_new_fields
        myjson = myjson.encode()
     
        print myjson
        (status, response_json) = self.obj.executeApi(api_url, myjson)
        print "Status :" + str(status)
        print "add_exhaustive_pdea response :" + str(response_json)
        
        
    def add_single_PDEA(self,host,pdea_new_field='vertical',field_data_type='string',username="admin",password="apj0702"): 
        api_url = '/api/exhaustive_pdea'
        if not hasattr(self, "obj"):
            self.obj = coreApi(host,username,password)
        pdea_new_fields ='''{"field_name":"%s", "field_data_type": "%s"}''' %(pdea_new_field,field_data_type)  
        myjson = '''{"exhaustive_pdea": [%s]}'''%pdea_new_fields
        myjson = myjson.encode()
        trace.info(myjson, also_console=True)
        (status, response_json) = self.obj.executeApi(api_url, myjson)
        trace.info("Status: %s"%(str(status)), also_console=True)
        trace.info("add PDEA: %s"%(str(response_json)), also_console=True)    


    def add_bulk_exhaustive_pdea(self, host,username="admin",password="apj0702",filename="config_binola.xls",spreadsheet="pdea",item_tags="1"):      
        item_list = item_tags.split(",")
        default_batch_size = 10
        current_batch_count = 0
        config_dict = getdata(filename)[spreadsheet]
        if not hasattr(self, "obj"):
            self.obj = coreApi(host,username,password)
        api_url = '/api/exhaustive_pdea'
        pdea_new_fields = ""
    
        for item_range in item_list:
            if(item_range.lower() == "all"):
                first_item = 1
                last_item = len(config_dict.keys())
            else:
                (first_item, last_item) = self.getFirstAndLastElement(item_range)
            for tag_id in range (first_item,last_item+1):
                current_batch_count = current_batch_count + 1
                pdea_new_fields = pdea_new_fields + '''{"field_name":"%s", "field_data_type": "%s"},'''%(config_dict[str(tag_id)]["field_name"],config_dict[str(tag_id)]["field_data_type"])
                if(current_batch_count ==  default_batch_size or tag_id == last_item):
                    pdea_new_fields = pdea_new_fields.strip(",")
                    myjson = '''{"exhaustive_pdea": [%s]}'''%pdea_new_fields
                    myjson = myjson.encode()
                    
                    trace.info(myjson, also_console=True)
                    (status, response_json) = self.obj.executeApi(api_url, myjson)
                    trace.debug(status)
                    trace.debug("Response: %s"%(response_json))
                    pdea_new_fields = ""
                    current_batch_count = 0
        
    
    def getFirstAndLastElement(self, item_range):
        pattern = '^(\d+)\-(\d+)$'
        match_object =  re.match(pattern, item_range)
        if(match_object):
            return(int(match_object.group(1)), int(match_object.group(2)))
        
        pattern = '^(\d+)$'
        match_object = re.match(pattern,item_range)
        if(match_object):
            return(int(match_object.group(1)), int(match_object.group(1)))
        
        trace.error("item_tags value is incorrect")
        sys.exit()


    def add_sku(self,host,username="admin",password="apj0702",filename="config_sanity.xls",spreadsheet="sku"):
        if not hasattr(self, "obj"):
            self.obj = coreApi(host,username,password)
        api_url = '/api/sku'
        product_sku = ""
        pdfa_fields = ""
        sku_json = ""
        
        config_dict = getdata(filename)[spreadsheet]
        for tag,tag_value in config_dict.iteritems():
            product_sku = tag_value["product_sku"]
            pdfa_fields = tag_value["pdfa_fields"]
            sku_json = sku_json + '''{"product_sku": "%s","field_list": [%s]},'''%(product_sku, pdfa_fields)
        sku_json = sku_json.strip(",") 
                    
        myjson = '''{"sku": [%s]}'''%(sku_json)
        myjson = myjson.encode()
    
        print myjson
        (status, response_json) = self.obj.executeApi(api_url, myjson)
        print "Status :" + str(status)
        print "add_sku response :" + str(response_json)


    def add_bulk_sku(self,host,username="admin",password="apj0702",filename="config_binola.xls",spreadsheet="sku_binola",item_tags="1"):
        item_list = item_tags.split(",")
        default_batch_size = 100
        current_batch_count = 0
        config_dict = getdata(filename)[spreadsheet]
        if not hasattr(self, "obj"):
            self.obj = coreApi(host,username,password)
        api_url = '/api/sku'
        product_sku = ""
        pdfa_fields = ""
        sku_json = ""
    
        for item_range in item_list:
            if(item_range.lower() == "all"):
                first_item = 1
                last_item = len(config_dict.keys())
            else:
                (first_item, last_item) = self.getFirstAndLastElement(item_range)
            for tag_id in range (first_item,last_item+1):
                current_batch_count = current_batch_count + 1
                product_sku = config_dict[str(tag_id)]["product_sku"]
                pdfa_fields = config_dict[str(tag_id)]["pdfa_fields"]
                sku_json = sku_json + '''{"product_sku": "%s","field_list": [%s]},'''%(product_sku, pdfa_fields)
                
                if(current_batch_count ==  default_batch_size or tag_id == last_item):
                    sku_json = sku_json.strip(",")
                    myjson = '''{"sku": [%s]}'''%(sku_json)
                    myjson = myjson.encode()
                    
                    trace.info(myjson, also_console=True)
                    (status, response_json) = self.obj.executeApi(api_url, myjson)
                    trace.info(status, also_console=True)
                    trace.info("Response: %s"%(response_json), also_console=True)
                    sku_json = ""
                    current_batch_count = 0


    def add_bulk_tote(self,host,username="admin",password="apj0702",filename="config_binola.xls",product_spreadsheet="products_binola", tote_spreadsheet="tote_flow",item_tags="1"):
        item_list = item_tags.split(",")
        tote_config_dict = getdata(filename)[tote_spreadsheet]
        product_config_dict = getdata(filename)[product_spreadsheet]
        if not hasattr(self, "obj"):
            self.obj = coreApi(host,username,password)
        api_url = '/api/put_expectation'
        
     
        for item_range in item_list:
            if(item_range.lower() == "all"):
                first_item = 1
                last_item = len(tote_config_dict.keys())
            else:
                (first_item, last_item) = self.getFirstAndLastElement(item_range)
            for tag_id in range (first_item,last_item+1):
                product_json = ""
                myjson = ""
                expectation_id = tote_config_dict[str(tag_id)]["expectation_id"].strip()
                tote_id = tote_config_dict[str(tag_id)]["tote_id"].strip()
                str_tote_items = tote_config_dict[str(tag_id)]["items"].strip()
                list_tote_items = str_tote_items.split('\n')
                for entry in list_tote_items:
                    pattern = '\s*product_tag\s*:\s*(\w),\s*quantity\s*:\s*(\d)\s*'
                    m_obj = re.match(pattern,entry)
                    if(m_obj):
                        product_tag = m_obj.group(1)
                        quantity = int(m_obj.group(2))
                    else:
                        BuiltIn().fail("Data in spreadsheet: %s having tag: %s is incorrect. Please correct it and run it again"%(tote_spreadsheet, tag_id))
                    
                    pdfa_values = product_config_dict[str(product_tag)]['pdfa_values']
                    product_json =  product_json + '''{"pdfa_values": {%s}, "quantity": %d, "type": "item"},'''%(pdfa_values, quantity)
                product_json = product_json.strip(",")
                myjson = myjson + '''{"put_container_id": "%s", "product_details": [%s], "expected_put_id": "%s"}'''%(tote_id, product_json, expectation_id)
                trace.info(myjson, also_console=True)
                (status, response_json) = self.obj.executeApi(api_url, myjson)
                trace.info(status, also_console=True)
                trace.info(response_json, also_console=True)
                
                
        
    def add_product(self,host,username="admin",password="apj0702",filename="config_sanity.xls",spreadsheet="products"):
        if not hasattr(self, "obj"):
            self.obj = coreApi(host,username,password)
        api_url = '/api/products?sync=true'
        product_dimensions = ""
        product_weight = 0.0
        product_barcodes = ""
        pdfa_values = ""
        product_list = ""
        
        config_dict = getdata(filename)[spreadsheet]
        for tag,tag_value in config_dict.iteritems():
            product_dimensions = tag_value["product_dimensions"]
            product_weight = tag_value["product_weight"]
            product_barcodes = tag_value["product_barcodes"]
            pdfa_values = tag_value["pdfa_values"]
            display_list = tag_value["display_list"]
            product_list = product_list + '''{
            "extra_fields": {"product_dimensions": [%s],"product_weight": %f,"product_barcodes": ["%s"]},
            "pdfa_values": {%s},
            "display_list": [%s]
            },'''%(product_dimensions, float(product_weight), product_barcodes, pdfa_values, display_list)
        product_list = product_list.strip(",")
        
        myjson = '''{"product_type": [%s]}'''%(product_list)  
        myjson = myjson.encode()
             
        print myjson
        (status, response_json) = self.obj.executeApi(api_url, myjson)
        print "Status :" + str(status)
        print "add_product response :" + str(response_json)


    def add_bulk_product(self,host,username="admin",password="apj0702",filename="config_binola.xls",spreadsheet="products_binola",item_tags="1"):
        item_list = item_tags.split(",")
        default_batch_size = 100
        current_batch_count = 0
        config_dict = getdata(filename)[spreadsheet]
        if not hasattr(self, "obj"):
            self.obj = coreApi(host,username,password)
        api_url = '/api/products?sync=true'
        product_dimensions = ""
        product_weight = 0.0
        product_barcodes = ""
        pdfa_values = ""
        product_list = ""
        storage_strategy = ""
        storage_custom_tag = "vertical"
    
        for item_range in item_list:
            if(item_range.lower() == "all"):
                first_item = 1
                last_item = len(config_dict.keys())
            else:
                (first_item, last_item) = self.getFirstAndLastElement(item_range)
            for tag_id in range (first_item,last_item+1):
                current_batch_count = current_batch_count + 1
                product_dimensions = config_dict[str(tag_id)]["product_dimensions"]
                product_weight = config_dict[str(tag_id)]["product_weight"]
                product_barcodes = config_dict[str(tag_id)]["product_barcodes"]
                pdfa_values = config_dict[str(tag_id)]["pdfa_values"]
                display_list = config_dict[str(tag_id)]["display_list"]
                product_category = config_dict[str(tag_id)]["product_category"]
                product_name = config_dict[str(tag_id)]["product_name"]
                product_price = config_dict[str(tag_id)]["product_price"]
                
                if config_dict[str(tag_id)].has_key("storage_strategy"):
                    storage_strategy = config_dict[str(tag_id)]["storage_strategy"]
                
                if(storage_strategy and storage_strategy!="NONE"):
                    product_list = product_list + '''{
            "extra_fields": {"product_name": "%s", "product_price": %d, "product_dimensions": [%s],"product_weight": %f,"product_barcodes": ["%s"], "product_category": "%s", "%s": "%s"},
            "pdfa_values": {%s},
            "display_list": [%s]
            },'''%(product_name, int(product_price), product_dimensions, float(product_weight), product_barcodes, product_category, storage_custom_tag, storage_strategy, pdfa_values, display_list)
            
                else:
                    product_list = product_list + '''{
            "extra_fields": {"product_name": "%s", "product_price": %d, "product_dimensions": [%s],"product_weight": %f,"product_barcodes": ["%s"], "product_category": "%s"},
            "pdfa_values": {%s},
            "display_list": [%s]
            },'''%(product_name, int(product_price), product_dimensions, float(product_weight), product_barcodes, product_category, pdfa_values, display_list)


                if(current_batch_count ==  default_batch_size or tag_id == last_item):
                    product_list = product_list.strip(",")
                    myjson = '''{"product_type": [%s]}'''%(product_list) 
                    myjson = myjson.encode()
                    
                    trace.info(myjson, also_console=True)
                    (status, response_json) = self.obj.executeApi(api_url, myjson)
                    trace.info(status)
                    trace.info("Response: %s"%(response_json), also_console=True)
                    product_list = ""
                    current_batch_count = 0

       
    def mock_put(self,host,username="gor",password="apj0702",filename="config_sanity.xls",spreadsheet="products"):
        ssh_obj = sshbasic(host, un=username, pw=password)
        product_uid = ""
        product_barcodes = ""
        quantity = 1
        put_list = ""
        
        config_dict = getdata(filename)[spreadsheet]
        for tag,tag_value in config_dict.iteritems():
            product_uid = tag_value["product_uid"]
            product_barcodes = tag_value["product_barcodes"]
            quantity = int(tag_value["quantity"])
            put_list = put_list + '''{"barcode":"UID:%s", "uid":"UID:%s", "quantity":%d},'''%(product_barcodes,product_uid,quantity)
            
        put_list = put_list.strip(",")
        cmd = '''curl -H "Content-Type: application/json"  -X "POST" -d '[%s]' localhost:8181/api/populate_inventory'''%(put_list)
        print cmd
            
        ssh_obj.executeCli(cmd)
        
    def put_product(self,host,username="admin",password="apj0702",filename="config_sanity.xls",spreadsheet="put"):
        if not hasattr(self, "obj"):
            self.obj = coreApi(host,username,password)
        api_url = '/api/sku'
        product_sku = ""
        pdfa_fields = ""
        sku_json = ""
        
#         config_dict = getdata(filename)[spreadsheet]
#         for tag,tag_value in config_dict.iteritems():
#             product_sku = tag_value["product_sku"]
#             pdfa_fields = tag_value["pdfa_fields"]
#             sku_json = sku_json + '''{"product_sku": "%s","field_list": [%s]},'''%(product_sku, pdfa_fields)
#         sku_json = sku_json.strip(",") 
                    
        myjson = '''{"sku": [%s]}'''%(sku_json)
        myjson = myjson.encode()
    
        print myjson
        (status, response_json) = self.obj.executeApi(api_url, myjson)
        
    def loginApi(self, host, username="admin", password="apj0702", ppsid="1", pps_side="back"):
        try:
            self.obj = coreApi(host,username,password)
        except:
            return False
        api_url = "/api/auth/token"
        myjson = '''{"username":"%s","password":"%s", "data_type":"auth","data":{"auth-token":"%s","seat_name":"%s_%s"}}'''%(username, password, self.obj.authToken, pps_side, ppsid)      
        (status,response_json) = self.obj.executeApi(api_url, myjson)
        trace.info(status,also_console=True)
        trace.info(response_json, also_console=True)
        return(int(status), response_json)
#         if (int(status) != 200):
#             errorMsg = "Unable to login to host: %s"%(host)
#             BuiltIn().fail(errorMsg)
    
    def processBarcode(self, host, username="admin", password="apj0702", barcode="2001", ppsid="1", pps_side="back"):
        if not hasattr(self, "obj"):
            self.obj = coreApi(host,username,password)
        api_url = "/api/pps_seats/%s_%s/send_data"%(pps_side,ppsid)
        myjson = '''{"event_name":"process_barcode","event_data":{"barcode":"%s"}}'''%(barcode)

        try:
            (status,response_json) = self.obj.executeApi(api_url, myjson)
        except:
            sleep(2)
            (status,response_json) = self.obj.executeApi(api_url, myjson)

        trace.debug(status)
        trace.debug(response_json)
        sleep(2)

        response_json = self.getPpsSeatStatus(host, username, password, ppsid, pps_side)
        notification_list = response_json['state_data']['notification_list']
        print notification_list
        for l in notification_list:
            if l.has_key('description'):
                print l['description']
#                 print "matched:" + str(l['description'])
                m = re.search('successful', l['description'], re.IGNORECASE)
                if m:
                    return True
                else:
                    return False
                    
    
    def cancelScan(self, host, username="admin", password="apj0702", ppsid="1", pps_side="back"):
        response_json = self.getPpsSeatStatus(host, username, password, ppsid, pps_side)
        item_uid = response_json['state_data']['item_uid']
        
        if not hasattr(self, "obj"):
            self.obj = coreApi(host,username,password)     
        api_url = "/api/pps_seats/%s_%s/send_data"%(pps_side,ppsid)
        myjson = '''{"event_name":"cancel_barcode_scan","event_data":{"barcode":"%s"}}'''%(item_uid)
        
        (status,response_json) = self.obj.executeApi(api_url, myjson)
        
        trace.debug(status)
        trace.debug(response_json)
        
        return(item_uid)
        
    
    def getPpsSelectedBinId(self, host, username="admin", password="apj0702", ppsid="1", pps_side="back"):
        response_json = self.getPpsSeatStatus(host, username, password, ppsid, pps_side)
        pps_list = response_json['state_data']['ppsbin_list']
        for pps in pps_list:
            if pps['selected_state'] == True or pps['ppsbin_state'] == "pick_processed":
                return str(pps['ppsbin_id'])
            
    def processPpsbinEvent(self, host, username, password, ppsbin_state, ppsbin_event, ppsid, pps_side):
        if not hasattr(self, "obj"):
            self.obj = coreApi(host,username,password)
        api_url = "/api/pps_seats/%s_%s/send_data"%(pps_side, ppsid)
#         pps_bin_id = "1"
        pps_bin_id = self.getPpsSelectedBinId(host, username, password, ppsid, pps_side)
        myjson = '''{"event_name":"process_ppsbin_event","event_data":{"ppsbin_id":"%s","ppsbin_state":"%s","ppsbin_event":"%s"}}'''%(pps_bin_id, ppsbin_state, ppsbin_event)   
    
        (status,response_json) = self.obj.executeApi(api_url, myjson)
        trace.debug(status)
        trace.debug(response_json)
        if (int(status) != 200):
            errorMsg = "Error in PPS Bin Event: %s"%(ppsbin_event)
            BuiltIn().fail(errorMsg)
        return pps_bin_id
    
    def stageAllEvent(self, ppsid, pps_side):
        api_url = "/api/pps_seats/%s_%s/send_data"%(pps_side, ppsid)
        myjson = '''{"event_name":"stage_all","event_data":""}'''   
    
        (status,response_json) = self.obj.executeApi(api_url, myjson)
        trace.debug(status)
        trace.debug(response_json)
        if (int(status) != 200):
            errorMsg = "Error in Stage All event"
            BuiltIn().fail(errorMsg)
            
    def stageOneItem(self, host, username, password, ppsid, pps_side, pps_bin_id=1):   
        if not hasattr(self, "obj"):
            self.obj = coreApi(host,username,password)
        
        api_url = "/api/pps_seats/%s_%s/send_data"%(pps_side, ppsid)
        myjson = '''{"event_name":"stage_ppsbin","event_data":{"ppsbin_id":"%s"}}'''%(pps_bin_id)
    
        (status,response_json) = self.obj.executeApi(api_url, myjson)
        trace.debug(status)
        trace.debug(response_json)
        if (int(status) != 200):
            errorMsg = "Error in Stage All event"
            BuiltIn().fail(errorMsg)
            
    def waitForMSU(self, ppsid, pps_side):
        api_url = "/api/pps_seats/%s_%s/data"%(pps_side,ppsid)
        timeout = 600
        time = 0
        (status,response_json) = self.obj.executeApi(api_url)
        description = response_json['state_data']['header_msge_list'][0]['description']
        while description == "Wait for MSU" or description == "Waiting for MSU":
#             trace.info("Wait For MSU: Sleeping for 2 seconds", also_console=True)
            trace.info("%s-%s: Wait For MSU: Sleeping for 2 seconds, total time %d seconds"%(pps_side, ppsid, time),also_console=True)
            sleep(2)
            time = time + 2
            if(time >= timeout):
#                 trace.info("Timeout occurred while wait for MSU",also_console=True)
#                 sys.exit()
                BuiltIn().fail("Timeout occurred while wait for MSU")
            (status,response_json) = self.obj.executeApi(api_url)
            description = response_json['state_data']['header_msge_list'][0]['description']
                    
    def getPpsIdMode(self, host, username="admin", password="apj0702"):
        if not hasattr(self, "obj"):
            print "Not present"
            self.obj = coreApi(host,username,password) 
        api_url = "/api/pps"
        (status,response_json) = self.obj.executeApi(api_url)
        trace.debug(status)
        trace.debug(response_json)
        
        pps_matrix = response_json['pps']
        put = []
        pick = []
        audit = []
     
        for i in range(len(pps_matrix)):
            if pps_matrix[i]['pps_mode'] == "put":
                put.append( pps_matrix[i]['pps_id'])
            if pps_matrix[i]['pps_mode'] == "pick":
                pick.append( pps_matrix[i]['pps_id'])
            if pps_matrix[i]['pps_mode'] == "audit":
                audit.append( pps_matrix[i]['pps_id'])
        
        trace.info("Put:" + str(put))
        trace.info("Pick:" + str(pick))
        trace.info("Audit:" + str(audit))
        
        mode_id_dict = {'put': put, 'pick': pick, 'audit': audit}
        trace.info(mode_id_dict, also_console=True)
        return mode_id_dict
    
    def getRackSlotBarcode(self, host, username="admin", password="apj0702", ppsid="1", pps_side="front"):
        response_json = self.getPpsSeatStatus(host, username, password, ppsid, pps_side)
        timeout = 30
        while timeout >0:
            try:
                rack_slots = response_json['state_data']['rack_details']['slot_barcodes']
                trace.info(rack_slots, also_console=True)
                return rack_slots
            except Exception:
                timeout -= 5
                sleep(5)
        return False
        
    def getProductBarcodeToScan(self, host, username="admin", password="apj0702", ppsid="1", pps_side="front"):
        response_json = self.getPpsSeatStatus(host, username, password, ppsid, pps_side)
        timeout = 30
        while timeout > 0:
            try:
                temp_list = response_json['state_data']['product_info']
                for i in temp_list:
                    for j in i:
                        if(j.has_key('product_barcodes')):
                            barcode = j['product_barcodes']
                            trace.info(barcode, also_console=True)
                            return barcode[0]
            except Exception:
                timeout -= 5
                sleep(5)
        return False
            
    
    def getPpsSeatStatus(self, host, username="admin", password="apj0702", ppsid="1", pps_side="front"):
        if not hasattr(self, "obj"):
            self.obj = coreApi(host,username,password) 
        api_url = "/api/pps_seats/%s_%s/data"%(pps_side,ppsid)
        (status,response_json) = self.obj.executeApi(api_url)
        trace.debug(status)
        trace.debug(response_json)
        return (response_json)
    
    def isScanAllowed(self, host, username="admin", password="apj0702", ppsid="1", pps_side="front"):
        response_json = self.getPpsSeatStatus(host, username, password, ppsid, pps_side)
        scan_flag = response_json['state_data']['scan_allowed']
        return scan_flag
    
    def getItemUid(self,host, username="admin", password="apj0702", ppsid="1", pps_side="front"):
        pps_status_json = self.getPpsSeatStatus(host, username, password, ppsid, pps_side)
        item_uid = pps_status_json['state_data']['item_uid']
        return (item_uid)

    def getKQ(self, host, username="admin", password="apj0702", ppsid="1", pps_side="front"):
        pps_status_json = self.getPpsSeatStatus(host, username, password, ppsid, pps_side)
        timeout = 30
        while timeout >0:
            try:
                total_qty = pps_status_json['state_data']['scan_details']['total_qty']
                return int(total_qty)
            except Exception:
                timeout -= 5
                sleep(5)
        return False

    def updateKQ(self,host, username="admin", password="apj0702", ppsid="1", pps_side="front", qty=1):
        if not hasattr(self, "obj"):
            self.obj = coreApi(host,username,password)
        api_url = "/api/pps_seats/%s_%s/send_data"%(pps_side,ppsid)
        item_uid = self.getItemUid(host, username, password, ppsid, pps_side)
        myjson = '''{"event_name":"quantity_update_from_gui","event_data":{"item_uid":"%s","quantity_updated":%d}}'''%(item_uid,int(qty))
        (status,response_json) = self.obj.executeApi(api_url,myjson)
        trace.debug(status)
        trace.debug(response_json)
    
    def getErrorPpsBin(self, host, username="admin", password="apj0702", ppsid="2", pps_side="front"):
        pps_status_json = self.getPpsSeatStatus(host, username, password, ppsid, pps_side)
        pps_bin_list = pps_status_json['state_data']['ppsbin_list']
        pps_bin_error = {}
        
        for pps in pps_bin_list:
            if pps['ppsbin_state'] == "error":
                pps_bin_error[str(pps['ppsbin_id'])] = pps['bin_info'][0]['product_sku']
        trace.info("PPS Bin Error: %s"%(str(pps_bin_error)), also_console=True)        
        return pps_bin_error

    def clearErrorPpsBin(self, host, username="admin", password="apj0702", ppsid="1", pps_side="back", pps_bin_id='1'):
        if not hasattr(self, "obj"):
            self.obj = coreApi(host,username,password)
        api_url = "/api/pps_seats/%s_%s/send_data"%(pps_side, ppsid)
        ppsbin_state = "error"
        ppsbin_event = "secondary_button_press"
        myjson = '''{"event_name":"process_ppsbin_event","event_data":{"ppsbin_id":"%s","ppsbin_state":"%s","ppsbin_event":"%s"}}'''%(pps_bin_id, ppsbin_state, ppsbin_event)     
        (status,response_json) = self.obj.executeApi(api_url, myjson)
        trace.debug(status)
        trace.debug(response_json)
        if (int(status) != 200):
            errorMsg = "Error in PPS Bin Event: %s"%(ppsbin_event)
            BuiltIn().fail(errorMsg)
        return pps_bin_id
    
    def clearAllErrorPpsBin(self, host, username="admin", password="apj0702", ppsid="1", pps_side="back"):
        if not hasattr(self, "obj"):
            self.obj = coreApi(host,username,password)
        ppsbin_state = "error"
        ppsbin_event = "secondary_button_press"
        pps_error_bins=self.getErrorPpsBin(host, username, password, ppsid, pps_side)
        for pps_bin_id in pps_error_bins.keys():
            api_url = "/api/pps_seats/%s_%s/send_data"%(pps_side, ppsid)
            myjson = '''{"event_name":"process_ppsbin_event","event_data":{"ppsbin_id":"%s","ppsbin_state":"%s","ppsbin_event":"%s"}}'''%(pps_bin_id, ppsbin_state, ppsbin_event)   
            (status,response_json) = self.obj.executeApi(api_url, myjson)
            trace.debug(status)
            trace.debug(response_json)
            if (int(status) != 200):
                errorMsg = "Error in PPS Bin Event: %s"%(ppsbin_event)
                BuiltIn().fail(errorMsg)
        return pps_error_bins    
         
    
    def getEmptyPpsBin(self, host, username="admin", password="apj0702", ppsid="1", pps_side="front"):
        pps_status_json = self.getPpsSeatStatus(host, username, password, ppsid, pps_side)
        pps_bin_list = pps_status_json['state_data']['ppsbin_list']
        pps_id_list = []
        
        for pps in pps_bin_list:
            if pps['ppsbin_state'] == "empty":
                pps_id_list.append(str(pps['ppsbin_id']))
                
        return pps_id_list
    
    def getInUsePpsBin(self, host, username="admin", password="apj0702", ppsid="1", pps_side="front"):
        pps_status_json = self.getPpsSeatStatus(host, username, password, ppsid, pps_side)
        pps_bin_list = pps_status_json['state_data']['ppsbin_list']
        pps_id_list = []
        
        for pps in pps_bin_list:
            if pps['ppsbin_state'] == "IN USE":
                pps_id_list.append(str(pps['ppsbin_id']))
                
        return pps_id_list
    
    def getStagedPpsBin(self, host, username="admin", password="apj0702", ppsid="1", pps_side="front"):
        pps_status_json = self.getPpsSeatStatus(host, username, password, ppsid, pps_side)
        pps_bin_list = pps_status_json['state_data']['ppsbin_list']
        pps_id_list = []
        
        for pps in pps_bin_list:
            if pps['ppsbin_state'] == "staged":
                pps_id_list.append(str(pps['ppsbin_id']))
                
        return pps_id_list
    
    def getSelectedPpsBin(self, host, username="admin", password="apj0702", ppsid="1", pps_side="front"):
        pps_status_json = self.getPpsSeatStatus(host, username, password, ppsid, pps_side)
        #print pps_status_json
        pps_bin_list = pps_status_json['state_data']['ppsbin_list']
        pps_id_list = []
        
        for pps in pps_bin_list:
            if pps['selected_state'] == True:
                pps_id_list.append(str(pps['ppsbin_id']))
                
        return pps_id_list
    
    def getPpsIdList(self, host, username="admin", password="apj0702"):
        if not hasattr(self, "obj"):
            self.obj = coreApi(host,username,password)
        
        api_url = '/api/pps'
        pps_id_list = []
        (status, response_json) = self.obj.executeApi(api_url)
        pps_info = response_json['pps']
        for pps in pps_info:
            pps_id = pps['pps_id']
            pps_id_list.append(str(pps_id))
        
        trace.info("PPS ID available on setup are: %s"%(pps_id_list),also_console=True)          
        return pps_id_list
    
    def getPickProcessedBin(self, host, username="admin", password="apj0702", ppsid="1", pps_side="front"):
        pps_status_json = self.getPpsSeatStatus(host, username, password, ppsid, pps_side)
        pps_bin_list = pps_status_json['state_data']['ppsbin_list']
        pps_id_list = []
        
        for pps in pps_bin_list:
            if pps['ppsbin_state'] == "pick_processed":
                pps_id_list.append(str(pps['ppsbin_id']))
                
        return pps_id_list
    
    def changePpsMode(self,host, username="admin", password="apj0702", ppsid="1", new_pps_mode="audit"):
        if not hasattr(self, "obj"):
            self.obj = coreApi(host,username,password) 
        api_url = "/api/pps/%s/pps_mode"%(ppsid)
        myjson = '''{"requested_pps_mode":"%s"}'''%(new_pps_mode)
        (status,response_json) = self.obj.executePutApi(api_url, myjson)
        trace.debug(status)
        trace.debug(response_json)
        if (int(status) != 200):
            errorMsg = "Unable to change Pps Mode"
            BuiltIn().fail(errorMsg)
        else:
            trace.info("PPS Mode of PPS ID: %s changed sucessfully to %s"%(ppsid, new_pps_mode),also_console=True)

    def alignPpsMode(self, host, username, password, put, pick, audit):
        put_list = put.split(",")
        pick_list = pick.split(",")
        audit_list = audit.split(",")
        
        mode_id_dict = self.getPpsIdMode(host, username, password)
        for ppsid in put_list:
            if ppsid not in mode_id_dict['put'] and ppsid != "":
                self.changePpsMode(host, username, password, ppsid, "put")
                sleep(5)
                
        for ppsid in pick_list:
            if ppsid not in mode_id_dict['pick'] and ppsid != "":
                self.changePpsMode(host, username, password, ppsid, "pick")
                sleep(5)
                
        for ppsid in audit_list:
            if ppsid not in mode_id_dict['audit'] and ppsid != "":
                self.changePpsMode(host, username, password, ppsid, "audit")
                sleep(5)

    def createUsers(self, host, username, password, ppsid, ppsSide):
        new_user = "%s_%s"%(ppsSide, ppsid)
        if self.getAllUsers(host, username, password).has_key(new_user):
            trace.info("User : %s, already exists"%new_user,also_console=True)
            return new_user
        if not hasattr(self, "obj"):
            self.obj = coreApi(host,username,password)
        pass_new_user = "apj0702"
        api_url = '/api/user'
        
        myjson = '''{"username":"%s","role_id":"3","password":"%s","password_confirm":"%s"}'''%(new_user, pass_new_user, pass_new_user)
        myjson = myjson.encode()
        (status,response_json) = self.obj.executeApi(api_url, myjson)
        if self.getAllUsers(host, username, password).has_key(new_user):
            trace.info("User : %s, created successfully"%new_user,also_console=True)
            return new_user
    
    def getAllUsers(self, host, username, password):
        if not hasattr(self, "obj"):
            self.obj = coreApi(host,username,password)
        
        api_url = '/api/user'
        userData = self.obj.executeApi(api_url, authRequired = True)
        userDict ={}
        if userData[0] == 200:
            for users in userData[1]["users"]:
                userDict[users['username']] = users['logged_in']
        return userDict
    
    def navigateToException(self, host, username, password, ppsid, pps_side, event_name, exception_name):
        if not hasattr(self, "obj"):
            self.obj = coreApi(host,username,password)
        api_url = "/api/pps_seats/%s_%s/send_data"%(pps_side, ppsid)
        myjson = '''{"event_name":"%s","event_data":{"event":"%s"}}'''%(event_name, exception_name)
        (status,response_json) = self.obj.executeApi(api_url, myjson)
        trace.debug(status)
        trace.debug(response_json)
        if (int(status) != 200):
            errorMsg = "Unable to navigate to Exception Page"
            BuiltIn().fail(errorMsg)
        else:
            trace.info("Successfully navigated to Exception Page, PPS ID: %s \t PPS Side: %s"%(ppsid, pps_side),also_console=True)
    
    def selectBinForException(self, host, username, password, ppsid, pps_side, event_name, exception_name, evnet_action, bin_id="1"):
        if not hasattr(self, "obj"):
            self.obj = coreApi(host,username,password)
        api_url = "/api/pps_seats/%s_%s/send_data"%(pps_side, ppsid)
        myjson = '''{"event_name":"%s","event_data":{"action":"%s","event":"%s","bin_id":"%s"}}'''%(event_name, evnet_action, exception_name, bin_id)
        (status,response_json) = self.obj.executeApi(api_url, myjson)
        trace.debug(status)
        trace.debug(response_json)
        if (int(status) != 200):
            errorMsg = "Unable to select bin to raise Exception"
            BuiltIn().fail(errorMsg)
        else:
            trace.info("Selected bin_id: %s to raise Exception %s"%(bin_id, exception_name),also_console=True)

    def confirmQuantityException(self, host, username, password, ppsid, pps_side, event_name, exception_name, evnet_action, quantity=1):
        if not hasattr(self, "obj"):
            self.obj = coreApi(host,username,password)
        api_url = "/api/pps_seats/%s_%s/send_data"%(pps_side, ppsid)
        myjson = '''{"event_name":"%s","event_data":{"action":"%s","event":"%s","quantity":%d}}'''%(event_name, evnet_action, exception_name, quantity)
        (status,response_json) = self.obj.executeApi(api_url, myjson)
        trace.debug(status)
        trace.debug(response_json)
        if (int(status) != 200):
            errorMsg = "Unable to confirm quantity to raise Exception"
            BuiltIn().fail(errorMsg)
        else:
            trace.info("Successfully confirmed %d quantity to raise Exception %s"%(quantity, exception_name),also_console=True)


    def finishException(self, host, username, password, ppsid, pps_side, event_name, exception_name, evnet_action):
        if not hasattr(self, "obj"):
            self.obj = coreApi(host,username,password)
        api_url = "/api/pps_seats/%s_%s/send_data"%(pps_side, ppsid)
        myjson = '''{"event_name":"%s","event_data":{"action":"%s","event":"%s"}}'''%(event_name, evnet_action, exception_name)
        (status,response_json) = self.obj.executeApi(api_url, myjson)
        trace.debug(status)
        trace.debug(response_json)
        print response_json
        if (int(status) != 200):
            errorMsg = "Unable to raise Exception"
            BuiltIn().fail(errorMsg)
        else:
            trace.info("Successfully raised Exception %s"%(exception_name),also_console=True)

    def cancelException(self, host, username, password, ppsid, pps_side):
        if not hasattr(self, "obj"):
            self.obj = coreApi(host,username,password)
        api_url = "/api/pps_seats/%s_%s/send_data"%(pps_side, ppsid)
        myjson = '''{"event_name":"cancel_exception","event_data":{}} '''
        (status,response_json) = self.obj.executeApi(api_url, myjson)
        trace.debug(status)
        trace.debug(response_json)
    
    def finishItemUnscannableException(self, host, username, password, ppsid, pps_side, event_name, exception_name, evnet_action, quantity):
        if not hasattr(self, "obj"):
            self.obj = coreApi(host,username,password)
        api_url = "/api/pps_seats/%s_%s/send_data"%(pps_side, ppsid)
        myjson = '''{"event_name":"%s","event_data":{"action":"%s","event":"%s","quantity": %d}}'''%(event_name, evnet_action, exception_name, int(quantity))
        (status,response_json) = self.obj.executeApi(api_url, myjson)
        trace.debug(status)
        trace.debug(response_json)
        print response_json
        if (int(status) != 200):
            errorMsg = "Unable to raise Exception"
            BuiltIn().fail(errorMsg)
        else:
            trace.info("Successfully raised Exception %s"%(exception_name),also_console=True)


    def finishDamagedOrMissingException(self, host, username, password, ppsid, pps_side, event_name, exception_name, evnet_action, good_quantity=1, damaged_quantity=1, missing_quantity=1):
        if not hasattr(self, "obj"):
            self.obj = coreApi(host,username,password)
        api_url = "/api/pps_seats/%s_%s/send_data"%(pps_side, ppsid)
        myjson = '''{"event_name":"%s","event_data":{"action":"%s","event":"%s","quantity":{"good":%d, "damaged":%d, "missing":%d}  }}'''%(event_name, evnet_action, exception_name, int(good_quantity), int(damaged_quantity), int(missing_quantity))
        (status,response_json) = self.obj.executeApi(api_url, myjson)
        trace.debug(status)
        trace.debug(response_json)
        print response_json
        if (int(status) != 200):
            errorMsg = "Unable to raise Exception"
            BuiltIn().fail(errorMsg)
        else:
            trace.info("Successfully raised Exception %s"%(exception_name),also_console=True)
    
    
    def put_back_operations(self,host,username="admin",password="apj0702",barcodes="2001",qty=500,ppsid=2,pps_side="back"):
        self.loginApi(host, username, password, ppsid=ppsid, pps_side="back")
        barcode_list=barcodes.split(",")
        for barcode in barcode_list:
            self.processBarcode(host, username, password, barcode, ppsid, pps_side="back")
            if qty > 1:
                lock_update_kq = Lock()
                lock_update_kq.acquire()
                self.updateKQ(host, username, password, ppsid, pps_side="back",qty=qty)
                lock_update_kq.release()
            pps_bin_id = self.processPpsbinEvent(host, username, password, ppsbin_state="empty", ppsbin_event="secondary_button_press", ppsid=ppsid, pps_side="back")       
            pps_bin_error = self.getErrorPpsBin(host, username, password, ppsid, pps_side="back") 
            self.stageAllEvent(ppsid=ppsid, pps_side="back")    
        if len(pps_bin_error.keys()) >0 :
            trace.error("Inside putBackOperation. return value: %s"%(str(pps_bin_error)))
            return "False"
        else: 
            trace.info("Inside putBackOperation. return value: %s"%(str(pps_bin_error)), also_console=True)
            return "True"
            

         

    def put_front_operations(self,host,username="admin",password="apj0702",ppsid=1,pps_side="front",):
        time = 0
        timeout = 300
        self.loginApi(host, username, password, ppsid=ppsid, pps_side="front")        
        while time < timeout:
            
            lock_bin_list = Lock()
            lock_bin_list.acquire()
            pps_bin_id_list = self.getInUsePpsBin(host, username, password, ppsid, pps_side="front")
            lock_bin_list.release()
            
            if(pps_bin_id_list):
                time = 0
                for pps_bin in pps_bin_id_list:
                    lock_wait_msu = Lock()
                    lock_wait_msu.acquire()
                    self.waitForMSU(ppsid, "front")
                    lock_wait_msu.release()
                    
                    sleep(2)
                    
                    lock_get_barcode = Lock()
                    lock_get_barcode.acquire()
                    barcode = self.getProductBarcodeToScan(host,username,password,ppsid, pps_side="front")
                    self.processBarcode(host, username, password, barcode, ppsid, pps_side="front")
                    lock_get_barcode.release()
                    
                    sleep(2)
                    
                    lock_get_kq = Lock()
                    lock_get_kq.acquire()
                    qty = self.getKQ(host, username, password, ppsid, pps_side="front")
                    lock_get_kq.release()
                    
                    if qty > 1:
                        lock_update_kq = Lock()
                        lock_update_kq.acquire()
                        self.updateKQ(host, username, password, ppsid, pps_side="front", qty=qty)
                        lock_update_kq.release()
                        
                        sleep(2)
                    
                    lock_get_rack_slot = Lock()
                    lock_get_rack_slot.acquire()        
                    rack_slots = self.getRackSlotBarcode(host,username,password,ppsid, pps_side="front")
                    self.processBarcode(host, username, password, rack_slots[0], ppsid, pps_side="front")
                    lock_get_rack_slot.release()
                    
                    sleep(2)
                    
                    lock_trace1 = Lock()
                    lock_trace1.acquire()
                    trace.info("Placed item: %s, Rack Slot: %s, Quantity: %d"%(barcode, rack_slots, qty), also_console=True)
                    lock_trace1.release()
                    
            else:
                lock_trace2 = Lock()
                lock_trace2.acquire()
                trace.info("No Item available in Put Front, Sleeping for 10 seconds. Idle Time: %d seconds"%(time), also_console=True)
                lock_trace2.release()
                sleep(10)
                time = time + 10   
                
    def get_current_sku_processing(self, host, username="admin", password="apj0702", ppsid="1", pps_side="front"):
        response_json = self.getPpsSeatStatus(host, username, password, ppsid, pps_side)
        list_productbarcodes = []
        for i in range(0, len(response_json['state_data']['product_info'])):
            for j in range(0, len(response_json['state_data']['product_info'][i])):
                try:
                    if not response_json['state_data']['product_info'][i][j]['product_barcodes']:
                        continue
                    else:
                        a = response_json['state_data']['product_info'][i][j]['product_barcodes']
                        a = str(a)
                        a = a.strip("[]'u'")
                        list_productbarcodes.append(a)
                except KeyError:
                    continue

        return list_productbarcodes

    def is_system_idle(self, host, username="admin", password="apj0702", ppsid="3", pps_side="front"):
        response_json = self.getPpsSeatStatus(host, username, password, ppsid, pps_side)
        if response_json['state_data']['is_idle'] is True:
            return True
        else:
            return False



        
    def add_single_order(self,host,username="admin",password="apj0702",pdfavalues="product_sku:2001",qty=1):
        if not hasattr(self, "obj"):
            self.obj = coreApi(host,username,password)
        api_url = '/api/orders?sync=true'
        orders_json = ""
        orders = ""
        optional_order_parameters=''
        preference_parameter = '''{"param_name": "order by", "param_value": "product_weight", "extra_key": "ASC"}'''
        random_value = random.randint(0,1000000)
        order_id = "Order-%d%d"%(random_value,random_value)
        orderline_id = "Orderline-%d"%(random_value)
        filter_parameters = ""
        pdfa_fields =pdfavalues.split(",")
        for pdfa_field in pdfa_fields:
            pdfa_key, pdfa_value = (pdfa_field.split(":")[0].strip("\""), pdfa_field.split(":")[1].strip().strip("\""))
            filter_parameters = filter_parameters + '''"%s in ['%s']",'''%(pdfa_key, pdfa_value)   
        filter_parameters = filter_parameters.strip(",")
        orders_json ='''{"orders":[{"order_id": "%s", "orderlines": [{"orderline_id": "%s","qty": %d,"filter_parameters": [%s],"preference_params": [%s]}]}]}'''%(order_id, orderline_id, qty, filter_parameters, preference_parameter)
        orders_json = orders_json.encode()
        trace.info(orders_json, also_console=True)
        (status, response_json) = self.obj.executeApi(api_url, orders_json)
        trace.info("Status: %s"%(str(status)), also_console=True)     
        trace.info("Add_order_response: %s"%(str(response_json)), also_console=True)
        return order_id,orderline_id

    def create_audit(self, host, username="admin", password="apj0702", audit_type = "sku", audit_value = "2001"):
        if not hasattr(self, "obj"):
            self.obj = coreApi(host,username,password)
        api_url = "/api/audit"
        my_json = '{"audit_param_type": "%s", "audit_param_value": "%s"}' % (audit_type, audit_value)

        (status, response_json) = self.obj.executeApi(api_url, my_json)

        trace.info(status, also_console=True)
        trace.info(response_json, also_console=True)

        trace.info("Created Audit, Audit ID: %s" % response_json['audit_id'], also_console=True)
        try:
            return response_json['audit_id']
        except:
            return False

    def start_audit(self, host, audit_id, username="admin", password="apj0702"):
        if not hasattr(self, "obj"):
            self.obj = coreApi(host, username, password)
        api_url = "/api/audit/start"
        pps_list = self.getPpsIdMode(host)
        pps_audit_id_list = pps_list['audit']
        if not pps_audit_id_list:
            BuiltIn().fail("No PPS in Audit Mode")

        pps_audit_id = pps_audit_id_list[0]
        my_json = '{"audit_id_list": ["%s"], "pps_list": ["%s"]}' % (audit_id, pps_audit_id)


        (status, response_json) = self.obj.executeApi(api_url, my_json)

        trace.info(status, also_console=True)
        trace.info(response_json, also_console=True)

        trace.info("****** Audit for Audit ID: %s started at PPS: %s" % (audit_id, pps_audit_id), also_console=True)
        try:
            return status
        except:
            return False

    def delete_audit(self, host, audit_id, username="admin", password="apj0702"):
        if not hasattr(self, "obj"):
            self.obj = coreApi(host, username, password)
        api_url = "/api/audit/delete/%s" % audit_id
        print api_url
        if audit_id is None:
            BuiltIn.fail(msg="Audit ID does not exist")
        (status, response_json) = self.obj.executeDeleteApi(api_url)
        trace.info("****** Status: %s " % status, also_console=True)
        trace.info("****** Audit Deleted :%s " % response_json, also_console=True)
        trace.info("****** Audit ID: %s" % audit_id, also_console=True)

        return status

    def sendAuditEvent(self,host, ppsid="3", pps_side="front", event_data="generate_report", username="admin", password="apj0702"):
        if not hasattr(self, "obj"):
            self.obj = coreApi(host, username, password)
        api_url = "/api/pps_seats/%s_%s/send_data" % (pps_side, ppsid)
     #   if event_type == "Finish":
        my_json = '''{"event_name":"audit_actions","event_data":{"type":"%s"}}''' % event_data
     #   elif event_type == "OK":
     #   my_json = '''{"event_name":"audit_actions","event_data":{"type":"%s"}}''' % "finish_current_audit"
        try:
            (status,response_json) = self.obj.executeApi(api_url, my_json)
        except:
            sleep(5)
            (status, response_json) = self.obj.executeApi(api_url, my_json)

        return status, response_json


    def verify_sku_audit(self,host="192.168.8.206", ppsside="front", ppsid="3", username="admin", password="apj0702",barcode="product1"):
        obj = apiHelper()
        obj.loginApi(host=host, username="admin", password="apj0702", ppsid=ppsid, pps_side=ppsside)
        obj.waitForMSU(ppsid=ppsid, pps_side=ppsside)
        # while apiHelper().getRackSlotBarcode(host, ppsid=ppsid) != False :
        while apiHelper().is_system_idle(host=host,ppsid=ppsid,pps_side=ppsside) is False:
            obj.waitForMSU(ppsid=ppsid, pps_side=ppsside)
            slot_barcode = self.getRackSlotBarcode(host, ppsid=ppsid)
            skus_at_location = pgsqlHelper().get_sku_at_location(host, location=slot_barcode[0])
            if len(skus_at_location) > 1:
                storage_node = slot_barcode[0] + '-02'
                quantity_of_product_per_slot = pgsqlHelper().get_product_by_sku_location(host=host, sku=barcode,
                                                                                         location=storage_node)
                self.processBarcode(host=host, barcode=slot_barcode[0], ppsid=ppsid, pps_side=ppsside)
                # print "quantity_of_product_per_slot:        ", quantity_of_product_per_slot
                for each in quantity_of_product_per_slot:
                    product_barcode = self.get_current_sku_processing(host=host, ppsid=ppsid)[0]
                    # print "processing sku:       ",self.get_current_sku_processing(host=host, ppsid=ppsid)[0]
                    pb = each['product_barcodes'].strip('[]"')
                    # print "pb:      ", pb
                    if pb == product_barcode:
                        # print "each-sum:      ", int(each['sum'])
                        for i in range(0,int(each['sum'])):
                            self.processBarcode(host=host, barcode=product_barcode, ppsid=ppsid, pps_side=ppsside)
                        break
                self.sendAuditEvent(host=host, event_data="generate_report")
                sleep(2)
                self.sendAuditEvent(host=host, event_data="finish_current_audit")
            else:
                obj.waitForMSU(ppsid=ppsid, pps_side=ppsside)
                storage_node = '%' + slot_barcode[0] + '-02%'
                # print storage_node
                self.verifyLocationAudit(host=host, ppsid=ppsid, storage_node=storage_node)
        return True


    def verifyLocationAudit(self,host="192.168.8.206", ppsid="3", username="admin", password="apj0702",storage_node="%031.1.A.01-02%"):
        location = pgsqlHelper().getSlotBarcodeByStorageNode("192.168.8.206", storage_node)[0]
        print location
        # self.processBarcode(host, username, password, barcode=location, ppsid="3", pps_side="front")
        a = pgsqlHelper().get_product_and_qty_by_location("192.168.8.206", storage_node)
        obj = apiHelper()
        obj.loginApi(host=host, username="admin", password="apj0702", ppsid="3", pps_side="front")
        obj.waitForMSU(ppsid, pps_side='front')
        print a
        self.processBarcode(host, username, password, barcode=location, ppsid="3", pps_side="front")

        for each in a:
            barcode = each['barcode'].strip('[]"')
            qty = int(each['qty'])
            print barcode, " : ", qty
            for i in range(0, qty):
                self.processBarcode(host, username, password, barcode=barcode, ppsid="3", pps_side="front")
        self.sendAuditEvent(host=host, event_data="generate_report")
        sleep(2)
        self.sendAuditEvent(host=host, event_data="finish_current_audit")


if  __name__ == "__main__":
    #print apiHelper().get_current_sku_processing(host="192.168.8.206",ppsid="3")
    #apiHelper().pick_front_process(host="192.168.8.212")
    # apiHelper().start_audit(host="192.168.8.206", audit_id=apiHelper().create_audit(host="192.168.8.206",audit_value='product1'))
    # apiHelper().verify_sku_audit("192.168.8.206")
    # apiHelper().start_audit(host="192.168.8.206", audit_id=apiHelper().create_audit(host="192.168.8.206",audit_type='location',audit_value="031.1.A.01"))
    # print apiHelper().getRackSlotBarcode(host='192.168.8.206',ppsid="3")
    # apiHelper().verifyLocationAudit()
    #apiHelper().clearAllErrorPpsBin(host="192.168.8.56",username="admin", password="apj0702", ppsid="3", pps_side="back")  
    #     apiHelper().getErrorPpsBin(host="192.168.8.109")
    # apiHelper().add_single_order(host="192.168.8.212",username="admin",password="apj0702",pdfavalues='"product_sku:2001"',qty=5)
    #     apiHelper().put_front_operations(host="192.168.8.56",username="admin",password="apj0702",ppsid=3,pps_side="front")
    # apiHelper().loginApi(host='192.168.8.206', ppsid="3", pps_side="front")
    # apiHelper().waitForMSU(ppsid=3, pps_side="front")
    # apiHelper().create_audit(host="192.168.8.206",audit_type="location",audit_value="031.1.A.01")
    # apiHelper().getPpsIdMode("192.168.8.157")
    # apiHelper().start_audit(host="192.168.8.206", audit_id=apiHelper().create_audit(host="192.168.8.206",audit_value='product1'))
    # apiHelper().verifyAudit(host="192.168.8.206", barcode='product1')
    # apiHelper().create_audit("192.168.8.206")
    # apiHelper().start_audit(host="192.168.8.206", audit_id="F3fG2qW2vK")
    # apiHelper().start_audit(host="192.168.8.206",audit_id=apiHelper().create_audit("192.168.8.206"))
    # (s,r) = apiHelper().getPpsSeatStatus(host="192.168.8.175", username="admin", password="apj0702", ppsid="1", pps_side="back")
    # apiHelper().delete_audit("192.168.8.175", 'ik2pTWU8j')
#     apiHelper().getErrorPpsBin(host="192.168.8.109")
#     apiHelper().putFrontOperations(host="192.168.8.109", username="admin", password="apj0702", pick_ppsid='2')
#     apiHelper().add_single_order(host="192.168.8.109",username="admin",password="apj0702",pdfavalues='"product_sku:Gravity4"',qty=1)
#      apiHelper().alignPpsMode("192.168.8.46", "admin", "apj0702", put="2,3", pick="1,4,5", audit="")
#     apiHelper().add_exhaustive_pdea(host="192.168.8.50", username="admin", password="apj0702", filename="../conf/config_sanity.xls", spreadsheet="pdea")         
#     apiHelper().add_sku(host="192.168.8.50", username="admin", password="apj0702", filename="../conf/config_sanity.xls", spreadsheet="sku")     
#     apiHelper().add_product(host="192.168.8.76", username="admin", password="apj0702", filename="../conf/config_sanity.xls", spreadsheet="products_test")
#     apiHelper().mock_put(host="192.168.3.58", username="gor", password="apj0702", filename="../conf/config_sanity.xls", spreadsheet="mock_put")    
#     apiHelper().add_order(host="192.168.8.145", username="admin", password="apj0702", filename="../conf/config_sanity.xls", spreadsheet1="orders", spreadsheet2="orderlines")
#     apiHelper().add_sku(host="192.168.8.42", username="admin", password="apj0702", filename="../conf/config_storage.xls", spreadsheet="sku_binola")
#     apiHelper().add_product(host="192.168.8.76", username="admin", password="apj0702", filename="../conf/config_sanity.xls", spreadsheet="products_test2")

#     apiHelper().add_bulk_exhaustive_pdea(host="192.168.8.76", username="admin", password="apj0702", filename="../conf/config_binola.xls", spreadsheet="pdea", item_tags="1-15,19-23,25")
#     apiHelper().add_bulk_order(host="192.168.8.157", username="admin", password="apj0702", filename="../conf/config_binola.xls", spreadsheet="products_test", item_tags="401-550")

#     apiHelper().add_bulk_sku(host="192.168.9.20", username="admin", password="apj0702", filename="../conf/config_binola_56.xls", spreadsheet="sku_binola", item_tags="1-4056")
    apiHelper().add_bulk_product(host="192.168.9.20", username="admin", password="apj0702", filename="../conf/config_binola_56.xls", spreadsheet="products_binola", item_tags="1-4056")

#     apiHelper().add_bulk_sku(host="192.168.8.121", username="admin", password="apj0702", filename="../conf/config_binola.xls", spreadsheet="sku_binola", item_tags="1-50")
#     apiHelper().add_bulk_product(host="192.168.8.121", username="admin", password="apj0702", filename="../conf/config_binola.xls", spreadsheet="products_binola", item_tags="1-50")
#     apiHelper().add_bulk_tote(host="192.168.8.121", username="admin", password="apj0702", filename="../conf/config_binola.xls", product_spreadsheet="products_binola", tote_spreadsheet="tote_flow", item_tags="all")

#     apiHelper().add_bulk_sku(host="192.168.8.182", username="admin", password="apj0702", filename="../conf/config_storage.xls", spreadsheet="sku_drawer", item_tags="41-41")
#     apiHelper().add_bulk_product(host="192.168.8.182", username="admin", password="apj0702", filename="../conf/config_storage.xls", spreadsheet="products_drawer", item_tags="41-41")

#     apiHelper().add_bulk_sku(host="192.168.8.73", username="admin", password="apj0702", filename="../conf/config_storage.xls", spreadsheet="sku_binola", item_tags="301-320")
#     apiHelper().add_bulk_product(host="192.168.8.73", username="admin", password="apj0702", filename="../conf/config_storage.xls", spreadsheet="test", item_tags="all")
#     apiHelper().add_single_PDEA(host="192.168.8.109",pdea_new_field='vertical',field_data_type='string')
    #apiHelper().getAllUsers("192.168.8.42", username="admin", password="apj0702")
    # apiHelper().getPpsIdList("192.168.8.157", username="admin", password="apj0702")
#     apiHelper().getRackSlotBarcode("192.168.8.117", "admin", "apj0702", "2", "front")

#     apiHelper().add_order_multiple_orderline(host="192.168.8.212", username="admin", password="apj0702", filename="../conf/config_binola.xls", spreadsheet="products_test", list_tags=[1,2], list_quantity=[2,1])
#     apiHelper().add_bulk_sku(host="192.168.8.145", username="admin", password="apj0702", filename="../conf/config_orderlines.xls", spreadsheet="sku_orderline", item_tags="1")
#     apiHelper().processBarcode(host="192.168.8.157", username="admin", password="apj0702", barcode="161.1.A.01", ppsid="3", pps_side="front")
#     obj = apiHelper()
#     a = obj.getRackSlotBarcode("192.168.8.157", ppsid="3")
#     barcode = a[0]
#     print barcode
# apiHelper().processBarcode(host="192.168.8.157", username="admin", password="apj0702", barcode="2001", ppsid="2", pps_side="back")