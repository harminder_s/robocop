'''
Created on 20-Apr-2016

@author: nishantmahawar
'''

from apiHelper import apiHelper
from time import sleep
from robot.api import logger as trace
#from robot.libraries.BuiltIn import BuiltIn
#from config import getdata
from multiprocessing import Process, Queue
from multiprocessing import Lock


class bulkPick:
    
    def bulk_pick_operations(self, host, username, password, pick, put):
        ppspick = pick.split(",")
        self.process = []
        self.queue = []
        
        for ppsid in ppspick:
            processname = "pickBack-%s"%(ppsid)
            queue_obj = Queue()
            process_obj = Process(target=self.pickBackOperations, args=(queue_obj, processname, host,username,password,ppsid,"back"))
            self.process.append(process_obj)
            self.queue.append(queue_obj)
            process_obj.start()
            sleep(2)
            
            processname = "pickFront-%s"%(ppsid)
            queue_obj = Queue()
            process_obj = Process(target=self.pickFrontOperations, args=(queue_obj, processname, host,username,password,ppsid,"front"))
            self.process.append(process_obj)
            self.queue.append(queue_obj)
            process_obj.start()
            sleep(2)
            
            
        for p_obj in self.process:
            p_obj.join()    
        
        for q_obj in self.queue:
            trace.info("%s"%str(q_obj.get()), also_console=True)
            
#         return self.error_sku

       
        
    def pickBackOperations(self, queue, processname,host,username,password,ppsid,pps_side):
        time = 0
        obj = apiHelper()
        obj.loginApi(host, username, password, ppsid=ppsid, pps_side="back")
        while time < 300:
            sleep(2)
            lock_processed_bin = Lock()
            lock_processed_bin.acquire() 
            pps_bin_id_list = obj.getPickProcessedBin(host, username, password, ppsid, pps_side)
            lock_processed_bin.release()
            
            if(pps_bin_id_list):
                time = 0
                for pps_bin_id in pps_bin_id_list:
                    print pps_bin_id
                    lock_process_event = Lock()
                    lock_process_event.acquire()
                    bin_id = obj.processPpsbinEvent(host, username, password, ppsbin_state="pick_processed", ppsbin_event="secondary_button_press", ppsid=ppsid, pps_side="back")   
                    trace.info("%s: Removed item from PPS Bin ID: %s"%(processname, bin_id), also_console=True)
                    lock_process_event.release()
                    sleep(2)
                                        
            else:
                lock_trace1 = Lock()
                lock_trace1.acquire()
                trace.info("%s: No Item available in Pick Back, Sleeping for 5 seconds. Idle Time: %d seconds"%(processname, time), also_console=True)
                lock_trace1.release()
                sleep(5)
                time = time + 5
                
        queue.put("%s: Successfully Completed Pick Back Operations"%(processname))
                 
    def pickFrontOperations(self, queue, processname,host,username,password,ppsid,pps_side):
        time = 0
        obj = apiHelper()
        obj.loginApi(host, username, password, ppsid=ppsid, pps_side="front")
        while time < 300:
            sleep(2)
            
            lock_scan_allowed = Lock()
            lock_scan_allowed.acquire()
            scan_allowed_flag = obj.isScanAllowed(host, username, password, ppsid, pps_side="front")
            lock_scan_allowed.release()
            
            if(scan_allowed_flag):
                time = 0
                sleep(2)
                lock_get_barcode = Lock()
                lock_get_barcode.acquire()   
                barcode = obj.getProductBarcodeToScan(host,username,password,ppsid, pps_side="front")
                obj.processBarcode(host, username, password, barcode, ppsid, pps_side="front")
                lock_get_barcode.release()
                sleep(2)
                
                lock_get_kq = Lock()
                lock_get_kq.acquire()    
                qty = obj.getKQ(host, username, password, ppsid, pps_side="front")
                lock_get_kq.release()
                    
                if qty > 1:
                    lock_update_kq = Lock()
                    lock_update_kq.acquire() 
                    obj.updateKQ(host, username, password, ppsid, pps_side="front", qty=qty)
                    lock_update_kq.release()
                    sleep(2)
                
                lock_process_bin_event = Lock()
                lock_process_bin_event.acquire()            
                pps_bin_id = obj.processPpsbinEvent(host, username, password, ppsbin_state="IN USE", ppsbin_event="primary_button_press", ppsid=ppsid, pps_side="front")
                trace.info("%s: Placed item: %s, PPS Bin ID: %s, Quantity: %d"%(processname, barcode, pps_bin_id, qty), also_console=True)
                lock_process_bin_event.release()
                    
            else:
                lock_trace2 = Lock()
                lock_trace2.acquire()
                trace.info("%s: No Item available in Pick Front, Sleeping for 5 seconds. Idle Time: %d seconds"%(processname, time), also_console=True)
                lock_trace2.release()
                
                sleep(2)
                time = time + 5
                
        queue.put("%s: Successfully Completed Pick Front Operations"%(processname))
            
            
if __name__ == "__main__":
    bulkPick().bulk_pick_operations(host="192.168.8.157", username="admin", password="apj0702", pick='3', put='4')
            
        