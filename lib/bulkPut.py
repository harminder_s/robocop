'''
Created on 20-Apr-2016

@author: nishantmahawar
'''

from apiHelper import apiHelper
from time import sleep
from robot.api import logger as trace
from robot.libraries.BuiltIn import BuiltIn
from config import getdata
from multiprocessing import Process, Queue, Lock

class bulkPut:

       
    def bulk_put_operations(self, host, username, password, filename, spreadsheet, pick, put, item_tags="all"):
        ppsput = put.split(",")
        config_dict = getdata(filename)[spreadsheet]
        num_products = len(config_dict.keys())
        num_pps_put = len(ppsput)
#         item_list = item_tags.split(",")
#         for item_range in item_list:
        if(item_tags.lower() == "all"):
            first_item = 1
            last_item = int(num_products/num_pps_put)
        else:
            first_item = int(item_tags.split('-')[0])
            last_item = int(item_tags.split('-')[1])
        
        self.process = []
        self.queue = []
        self.error_sku = []
    # self.obj = apiHelper()
       
        for ppsid in ppsput:
            processname = "putBack-%s"%(ppsid)
            queue_obj = Queue()
            config_dict1 = getdata(filename)[spreadsheet]
            process_obj = Process(target=self.bulkputBackOperations, args=(queue_obj, processname, host,username,password,ppsid,"back",config_dict1,first_item,last_item))
            self.process.append(process_obj)
            self.queue.append(queue_obj)
            first_item = last_item + 1
            last_item = last_item + int(num_products/num_pps_put)
            process_obj.start()
            sleep(2)
              
            processname = "putFront-%s"%(ppsid)
            queue_obj = Queue()
            process_obj = Process(target=self.bulkputFrontOperations, args=(queue_obj, processname, host,username,password,ppsid,"front"))
            self.process.append(process_obj)
            self.queue.append(queue_obj)
            process_obj.start()
            sleep(2)
            
        for p_obj in self.process:
            p_obj.join()    
        
        for q_obj in self.queue:
            trace.info("%s"%str(q_obj.get()), also_console=True)
        return self.error_sku

       
        
    def bulkputBackOperations(self, queue, processname,host,username,password,ppsid,pps_side,config_dict1,first_item,last_item):
#         self.obj = apiHelper()
        tag = first_item
        max_qty = 5
        obj = apiHelper()
        obj.loginApi(host, username, password, ppsid=ppsid, pps_side="back")
#         self.obj.loginApi(host, username, password, ppsid=ppsid, pps_side="back")
        sleep(2)
        count = 1
        while tag <= last_item:
            
            lock_get_error_pps = Lock()
            lock_get_error_pps.acquire()
            pps_bin_error = obj.getErrorPpsBin(host, username, password, ppsid, pps_side="back")
            lock_get_error_pps.release()
            
            for pps_bin_id, product_sku in pps_bin_error.iteritems():
                self.error_sku.append(product_sku)
                trace.info("%s: Error Sku - %s"%(processname, product_sku), also_console=True)
                lock_clear_error_pps = Lock()
                lock_clear_error_pps.acquire()
                pps_id = obj.clearErrorPpsBin(host, username, password, ppsid, "back", pps_bin_id)
                trace.info("%s: Cleared PPS Bin ID: %s having SKU: %s"%(processname, pps_id, product_sku), also_console=True)
                lock_clear_error_pps.release()
            
            lock_get_empty_bin = Lock()
            lock_get_empty_bin.acquire()   
            pps_bin_id_list = obj.getEmptyPpsBin(host, username, password, ppsid, pps_side="back")
            lock_get_empty_bin.release()
            
            count = count + 1
            if(pps_bin_id_list):
                for pps_bin in pps_bin_id_list:
                    if(tag<=last_item):
                        if(config_dict1[str(tag)].has_key('total_quantity')):
                            qty = config_dict1[str(tag)]['total_quantity']
                        else:
                            qty = tag%max_qty or 1
                        
                        lock_process_barcode = Lock()
                        lock_process_barcode.acquire()
                        trace.info("%s: Putting Item against tag: %d having barcode: %s"%(processname, tag, config_dict1[str(tag)]['product_barcodes']), also_console=True)
                        obj.processBarcode(host, username, password, config_dict1[str(tag)]['product_barcodes'], ppsid, pps_side="back")
                        lock_process_barcode.release()
                        
                        sleep(2)
                        
                        if qty > 1:
                            lock_update_kq = Lock()
                            lock_update_kq.acquire()
                            obj.updateKQ(host, username, password, ppsid, pps_side="back",qty=qty)
                            lock_update_kq.release()
                            
                            sleep(2)
                        
                        lock_process_bin_event = Lock()
                        lock_process_bin_event.acquire()
                        pps_bin_id = obj.processPpsbinEvent(host, username, password, ppsbin_state="empty", ppsbin_event="secondary_button_press", ppsid=ppsid, pps_side="back")
                        trace.info("%s: Placed item: %s on PPS Bin: %s, Quantity: %d"%(processname, config_dict1[str(tag)]['product_barcodes'], str(pps_bin_id), qty), also_console=True)
                        lock_process_bin_event.release()
                        
                        sleep(2)     
                        
                        ###
                        lock_stage_all = Lock()
                        lock_stage_all.acquire()
                        obj.stageAllEvent(ppsid=ppsid, pps_side="back")
                        trace.info("%s: Staged all Items"%(processname), also_console=True)
                        lock_stage_all.release()
                        ###
                        tag = tag + 1
                
#                 lock_stage_all = Lock()
#                 lock_stage_all.acquire()
#                 obj.stageAllEvent(ppsid=ppsid, pps_side="back")
#                 trace.info("%s: Staged all Items"%(processname), also_console=True)
#                 lock_stage_all.release()
#                 
#                 sleep(10)
            else:
                lock_trace = Lock()
                lock_trace.acquire()
                trace.info("%s: No Bins Available, Sleeping for 30 seconds"%(processname), also_console=True)
                lock_trace.release()        
                sleep(30)
        trace.info("%s"%(str(self.error_sku)),also_console=True)
        queue.put("%s: Successfully Completed PutBack Operations"%(processname))
                 
    def bulkputFrontOperations(self, queue, processname,host,username,password,ppsid,pps_side):
         self.obj = apiHelper()
         time = 0
         self.obj.loginApi(host, username, password, ppsid=ppsid, pps_side="front")
         sleep(2)
         self.obj.waitForMSU(ppsid, "front")
         sleep(2)
         obj = apiHelper()
         obj.loginApi(host, username, password, ppsid=ppsid, pps_side="front")        
         while time < 300:
            
            lock_bin_list = Lock()
            lock_bin_list.acquire()
            pps_bin_id_list = obj.getInUsePpsBin(host, username, password, ppsid, pps_side="front")
            lock_bin_list.release()
            
            if(pps_bin_id_list):
                time = 0
                for pps_bin in pps_bin_id_list:
                    lock_wait_msu = Lock()
                    lock_wait_msu.acquire()
                    obj.waitForMSU(ppsid, "front")
                    lock_wait_msu.release()
                    
                    sleep(2)
                    
                    lock_get_barcode = Lock()
                    lock_get_barcode.acquire()
                    barcode = obj.getProductBarcodeToScan(host,username,password,ppsid, pps_side="front")
                    obj.processBarcode(host, username, password, barcode, ppsid, pps_side="front")
                    lock_get_barcode.release()
                    
                    sleep(2)
                    
                    lock_get_kq = Lock()
                    lock_get_kq.acquire()
                    qty = obj.getKQ(host, username, password, ppsid, pps_side="front")
                    lock_get_kq.release()
                    
                    if qty > 1:
                        lock_update_kq = Lock()
                        lock_update_kq.acquire()
                        obj.updateKQ(host, username, password, ppsid, pps_side="front", qty=qty)
                        lock_update_kq.release()
                        
                        sleep(2)
                    
                    lock_get_rack_slot = Lock()
                    lock_get_rack_slot.acquire()        
                    rack_slots = obj.getRackSlotBarcode(host,username,password,ppsid, pps_side="front")
                    obj.processBarcode(host, username, password, rack_slots[0], ppsid, pps_side="front")
                    lock_get_rack_slot.release()
                    
                    sleep(2)
                    
                    lock_trace1 = Lock()
                    lock_trace1.acquire()
                    trace.info("%s: Placed item: %s, Rack Slot: %s, Quantity: %d"%(processname, barcode, rack_slots, qty), also_console=True)
                    lock_trace1.release()
                    
            else:
                lock_trace2 = Lock()
                lock_trace2.acquire()
                trace.info("%s: No Item available in Put Front, Sleeping for 10 seconds. Idle Time: %d seconds"%(processname, time), also_console=True)
                lock_trace2.release()
                sleep(10)
                time = time + 10
            queue.put("%s: Successfully Completed PutFront Operations"%(processname))
            
            
            
            
            
if __name__ == "__main__":
    bulkPut().bulk_put_operations(host="192.168.8.157", username="admin", password="apj0702", filename="../conf/config_storage.xls", spreadsheet="Stacking_Drawers", pick='3,1,4', put='4')
        