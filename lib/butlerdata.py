import httplib, json, re
from coreApi import coreApi
from StringIO import StringIO

class getinventory:
    def __init__(self,host, username = "admin", password ="apj0702"):
        self.webservice = coreApi(host,username,password)
        
    def getProductbyID(self,ids):
        api_url = "/api/products/%s"%ids
        (status,response_json) = self.webservice.executeApi(api_url)
        res = response_json["product"]
        try:
            storage_strategy = res['extra_fields']['vertical']
        except:
            storage_strategy =False
        try:
            storage_tag_list = res['extra_fields']['storage_tag_list']
        except:
            storage_tag_list = False
        productsku = res["pdfa_values"]["product_sku"]
        return productsku,storage_strategy,storage_tag_list
    
class influx:
    def __init__(self, host, username = "admin", password ="apj0702"):
        self.webservice = httplib.HTTPConnection("%s:8086"%host)
        
    def __queryconvertor(self,query):
        tmpStr = "+".join(re.split("\s+",query))
        tmpStr = tmpStr.replace("'", "%27")
        tmpStr = tmpStr.replace("=", "%3D")
        apiStr = "/query?q=%s&db=GreyOrange"%tmpStr
        return apiStr
   
    def getinfluxdata(self, query):
        apiURL = self.__queryconvertor(query)
        self.webservice.putrequest("GET", apiURL)
        self.webservice.putheader("Content-type", "application/json")
        self.webservice.endheaders()
        out =  self.webservice.getresponse()                                   
        status = int(out.status)
        if status == 200:
            data = out.read()
            io = StringIO(data)
            response_json =  json.load(io)
            res = response_json["results"][0]["series"][0]
            mainList = []
            for val in res["values"]:
                mainList.append({k: v for k, v in zip(res['columns'], val)})
            return mainList
        else:
            return False

class getdata:
    def __addtodict(self,mydict,mykey, myval):
        if mydict.has_key(mykey):
            val = mydict[mykey]
            val.append(myval)
            mydict[mykey] = val
        else:
            mydict[mykey] = [myval]
            
    def getOrderData(self,ip,order="all",username="admin", password="apj0702"):
        obj = influx(ip, username, password)
        invobj = getinventory(ip, username, password)
        if order == "all":
            query = "select * from item_picked"
        else:
            query = "select * from item_picked  where order_id='%s'"%order
        data = obj.getinfluxdata(query)
        mainDict = {}
        for item in data:
            tmpDict= {}
            for key in  item.keys():
                if key != "order_id":
                    if key == "item_id":
                        tmpDict[key] = item[key]
                        itemval = invobj.getProductbyID(item[key])
                        tmpDict["sku"] = itemval[0]
                        tmpDict["storage_strategy"] = itemval[1]
                        tmpDict["storage_tag_list"] = itemval[2]
                    else:
                        tmpDict[key] = item[key]
            self.__addtodict(mainDict,item["order_id"],tmpDict)
        return mainDict    
    
if  __name__ == "__main__":
    a = getdata().getOrderData("192.168.8.212","OD5-54461")
    for key in a.keys():
        print key,len(a[key])
        if len(a[key]) > 1:
            print key, a[key]
    #auth_key = obj.authToken
    # print obj.executeApi("/api/pps")
    #a = obj.getdata("select * from item_picked")
    #print a
    #obj1 = getinventory("192.168.8.140")
    #print obj1.getProductbyID("712c4cc2-b87f-4f49-a0fb-135d1ccf7e1a")