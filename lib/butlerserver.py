import random
import re, itertools
import ast, json
from robot.api import logger as trace
from time import sleep

import sshbasic
class butlerserver:
    def __init__(self):
        self.serviceList = ["butler_server","nginx","butler_remote"]
        self.supervisorList = ["butler_interface","websocket","bi_celery","bi_celerybeat"]
        self.interfaceCount =10
        self.serviceDict = {"butler_server": False,
                            "nginx": False,
                            "butler_remote": False,
                            "butler_interface": False,
                            "websocket": False,
                            "bi_celery": False,
                            "bi_celerybeat": False}
    def allservices(self,ip):
        for service in self.serviceList:
            out = self.getServiceStatus(service, ip)
            self.serviceDict[service] = out
#         
        notRunning = []
        self.getSupervisorStatus(ip)
        for keys in self.serviceDict.keys():
            if keys == "butler_interface":
                if self.serviceDict[keys]:
                    for ikey in self.serviceDict[keys]:
                        if self.serviceDict[keys][ikey] not in ["RUNNING","active"]:
                            notRunning.append(ikey)
                else:
                    notRunning.append(keys)
            else:
                if self.serviceDict[keys] not in ["RUNNING","running"]:
                    notRunning.append(keys)
        if len(notRunning) >0 :
            trace.info("Following services/processes not running :\n%s"%notRunning,also_console=True)
            trace.info("All services/processes status :\n%s"%str(self.serviceDict),also_console=True)
            return False
        else:
            trace.info("All services/processes up and running :\n%s"%str(self.serviceDict),also_console=True)
            return True
       
    def getServiceStatus(self,serviceName,ip,un="gor",pw="apj0702"):
        targetServer = sshbasic.sshbasic(ip,un,pw)
        cmd = "sudo service %s status"%serviceName
        targetServer.ssh.set_client_configuration(timeout = "3 sec")
        out = targetServer.executeCli(cmd)
        m1 = re.search("\S+\s*Active:\s+\S+\s+\((?P<status>\S+)\)\s*\S+",out.strip(),re.MULTILINE)
        if m1:
            status = m1.group("status")
            return status
        else:
            return False
        
    def restartService(self,serviceName,ip,un="gor",pw="apj0702"):
        targetServer = sshbasic.sshbasic(ip,un,pw)
        cmd = "sudo service %s restart"%serviceName
        targetServer.executeCli(cmd)
        cmd = "sudo service %s status"%serviceName
        targetServer.ssh.set_client_configuration(timeout = "5 sec")
        out = targetServer.executeCli(cmd)
        m1 = re.search("\S+\s*Active:\s+\S+\s+\((?P<status>\S+)\)\s*\S+",out.strip(),re.MULTILINE)
        if m1:
            status = m1.group("status")
            return status
        else:
            return False
    
    def startService(self,serviceName,ip,un="gor",pw="apj0702"):
        targetServer = sshbasic.sshbasic(ip,un,pw)
        cmd = "sudo service %s start"%serviceName
        targetServer.executeCli(cmd)
        cmd = "sudo service %s status"%serviceName
        targetServer.ssh.set_client_configuration(timeout = "5 sec")
        out = targetServer.executeCli(cmd)
        m1 = re.search("\S+\s*Active:\s+\S+\s+\((?P<status>\S+)\)\s*\S+",out.strip(),re.MULTILINE)
        if m1:
            status = m1.group("status")
            return status
        else:
            return False
        
    def stopService(self,serviceName,ip,un="gor",pw="apj0702"):
        targetServer = sshbasic.sshbasic(ip,un,pw)
        cmd = "sudo service %s stop"%serviceName
        targetServer.executeCli(cmd)
        cmd = "sudo service %s status"%serviceName
        targetServer.ssh.set_client_configuration(timeout = "5 sec")
        out = targetServer.executeCli(cmd)
        m1 = re.search("\S+\s*Active:\s+\S+\s+\((?P<status>\S+)\)\s*\S+",out.strip(),re.MULTILINE)
        if m1:
            status = m1.group("status")
            return status
        else:
            return False
        
    def getSupervisorStatus(self,ip,un="gor",pw="apj0702"):
        targetServer = sshbasic.sshbasic(ip,un,pw)
        cmd = "sudo supervisorctl status"
        targetServer.ssh.set_client_configuration(timeout = "10 sec")
        out = targetServer.executeCli(cmd)
        interfaceDict ={}
        for lines in out.splitlines():
            m1 = re.search("^\s*(?P<name>\S+)\s+(?P<status>\S+)\s+.+",lines.strip(),re.MULTILINE)
            if m1:
                status = m1.group("status")
                name = m1.group("name")
                name1 = name.split(":")[0]
                if name1 in self.supervisorList:
                    if name1 == "butler_interface":
                        interfaceDict[name.split(":")[1]] = status
                    else:
                        self.serviceDict[name1] = status
        self.serviceDict["butler_interface"] = interfaceDict
        
    def getGridInfo(self,ip,un="gor",pw="apj0702"):
        targetServer = sshbasic.sshbasic(ip,un,pw)
        cmd = "sudo /opt/butler_server/bin/butler_server rpcterms gridinfo get_all"
        targetServer.ssh.set_client_configuration(timeout = "60 sec")
        out = targetServer.executeCli(cmd)
        coordinateDict ={}
        '''{gridinfo,{26,16},
               "016.026",null,
               [[1,1,1],[1,1,1],[1,1,1],[0,0,0]],
               undefined,false,false,<<"defzone">>,undefined,undefined},'''
        tempList= re.findall(".+\,{(?P<coord>\d{2},\d{2})\},\s+\"(?P<barcode>\d{3}.\d{3})\",.*,\s+(?P<coordList>\[\S+\]),\s+.+",out,re.MULTILINE)
        for i in tempList:
            coords = ast.literal_eval(i[2])
            coordinateDict[i[1]] = coords
        return coordinateDict
    
    def getRackInfo(self,ip,un="gor",pw="apj0702"):
        ### returning the dict {rack_id : {barcode:<BARCODE>,face:<FACE>,type:<RACK_TYPE>}
        targetServer = sshbasic.sshbasic(ip,un,pw)
        cmd = "sudo /opt/butler_server/bin/butler_server rpcterms rackinfo get_all"
        targetServer.ssh.set_client_configuration(timeout = "60 sec")
        out = targetServer.executeCli(cmd)
        coordinateDict ={}
        # {rackinfo,<<"12">>,2,"015.033","015.033",true,undefined,null,"29",[],msu},
        tempList= re.findall(".+\{rackinfo,<<\"(?P<id>\d+)\">>,(?P<face>\d{1}),\"(?P<barcode>\d{3}.\d{3})\",.*,.*,.*,.*,\"(?P<type>\d+)\".+",out,re.MULTILINE)
        for i in tempList:
            coordinateDict[i[2]] = {"face":i[1],"id":i[0],"rackType" : i[3]}
        return coordinateDict
    
    def getStorageInfo(self,ip,un="gor",pw="apj0702"):
        targetServer = sshbasic.sshbasic(ip,un,pw)
        cmd = "sudo /opt/butler_server/bin/butler_server rpcterms storage_info get_all"
        targetServer.ssh.set_client_configuration(timeout = "60 sec")
        out = targetServer.executeCli(cmd)
        tempList= re.findall(".*\{storage_info,\{(?P<barcode>\d+,\d+)\},(?P<avail>\d{1}),(?P<status>.+)\}.*",out,re.MULTILINE)
        allValid = []
        available = []
        undefined = []
        stored = {}
        wronglyplaced = []
        for item in tempList:
            tmp  = [int(i) for i in item[0].split(",")]
            barcode = "%03d.%03d"%(tmp[1],tmp[0])
            if item[1] == '0':
                if item[2] == 'undefined':
                    undefined.append(barcode)
                else:
                    wronglyplaced.append(barcode)
                
            elif item[1] == '1':
                allValid.append(barcode)
                if item[2] == 'available':
                    available.append(barcode)
                else:
                    ids = re.findall(".*\{<<\"(?P<id>\d+)\">>,.*",item[2],re.MULTILINE)
                    if len(ids)>0:
                        stored[ids[0]]= barcode
       # print len(allValid),len(stored),len(available), len(wronglyplaced),len(undefined)
        #print allValid,stored,available, wronglyplaced,undefined
        return allValid,stored,available, wronglyplaced,undefined
      
    def getButlerInfo(self,ip,un="gor",pw="apj0702"):
        targetServer = sshbasic.sshbasic(ip,un,pw)
        cmd = "sudo /opt/butler_server/bin/butler_server rpcterms butlerinfo get_all"
        targetServer.ssh.set_client_configuration(timeout = "60 sec")
        out = targetServer.executeCli(cmd)
        butlerDict ={}
        tempList= re.findall(".+\{butlerinfo,(?P<id>\d{1,2}),.+,\"(?P<barcode>\d{3}.\d{3})\",(?P<status>\w+)",out,re.MULTILINE)
        for info in tempList:
            butlerDict[info[0]] = {"coordinates":info[1],"status":info[2]}
        return butlerDict
    
    def purgeButler(self,ip,un="gor",pw="apj0702"):
        targetServer = sshbasic.sshbasic(ip,un,pw)
        cmd = "sudo /opt/butler_server/bin/butler_server rpcterms butler_test_functions purge_all_butlers"
        targetServer.ssh.set_client_configuration(timeout = "60 sec")
        out = targetServer.executeCli(cmd)
        return out
    
    def executingButlerRemoteCommands(self,ip,cmd, un="gor",pw="apj0702"):
        targetServer = sshbasic.sshbasic(ip,un,pw)
        targetServer.ssh.set_client_configuration(timeout = "60 sec")
        out = targetServer.executeCli(cmd)
        if out.find("failed") != -1:
            print "cmd '%s' failed due to reason :\n %s"%(cmd,out)
            return False
        return out
    
    def unpauseButlers(self,ip,un="gor",pw="apj0702"):
        cmd = "sudo /opt/butler_server/bin/butler_server rpcterms butler_functions butler_unpause_all_butlers_override"
        out = self.executingButlerRemoteCommands(ip, cmd, un, pw)
        if out:
            return True
        else:
            cmd = "sudo /opt/butler_server/bin/butler_server rpcterms butler_functions butler_unpause_all_free_or_paused_butlers"
            return self.executingButlerRemoteCommands(ip, cmd, un, pw)
        
    def getStorageNode(self,ip,un="gor",pw="apj0702"):
        ###storage_node:get_by_id("030.0.B.04-05-06.02")
        fName = "%s_storage.txt"%ip
        cmd = "sudo /opt/butler_server/bin/butler_server rpcterms storage_node get_all > %s"%fName
        self.executingButlerRemoteCommands(ip, cmd, un, pw)
        targetServer = sshbasic.sshbasic(ip,un,pw)
        
        targetServer.copyFromRemote("/home/gor/%s"%fName,"../tmp/" )
        fp =  open('../tmp/%s'%fName,"r")  
        dat1 =fp.readlines()
        fp.close()
        mainDict = {}
        for lines in dat1:
            #m = re.search("\{storage_node,\s*\"(?P<storage_node>\S+)\".*\n[\S\s]+slot_tags\s*\S+\s*\[(?P<slot_tags>\S+)\],\n\s*slot_type\s*\S+\s*(?P<slot_type>\S+),",dat1.strip(),re.MULTILINE)
            m = re.search("\{storage_node,\s*\"(?P<storage_node>\S+)\"",lines,re.MULTILINE)
            if m:
                storageNode =  m.groups()[0]
                
            m = re.search("slot_tags\s*\S+\s*\[(?P<slot_tags>\S+)\],",lines,re.MULTILINE)
            if m:
                slotTag =  m.groups()[0]
                
            m = re.search("slot_type\s*\S+\s*(?P<slot_type>\S+),",lines,re.MULTILINE)
            if m:
                slotType =  m.groups()[0]
                mainDict[storageNode] = {"slotTag":slotTag,"slotType":slotType}
        return mainDict
      
    def getSlotOccupancy(self,slotList,ip,un="gor",pw="apj0702"):
        targetServer = sshbasic.sshbasic(ip,un,pw)
        fileList =  targetServer.ssh.list_directory("/opt/butler_server")
        for eFileName in fileList:
            if eFileName.find("erts-") != -1:
                eFile = eFileName
        
        if not targetServer.fileExist("/tmp/slotOccupancy.escript"):
            print "File Not exist, creating and uploading"
            escript = '''#!/opt/butler_server/%s/bin/escript
%%%%! -sname butler -setcookie butler_server
     
     main(String) ->
        Node = 'butler_server@localhost',
        Mod = 'storage_node',
        Fun = 'slot_occupancy',
        Args = String,
        R = rpc:call(Node, Mod, Fun, Args),
       io:format("Storage Node ~p~n",[R]).'''%eFile
            fp = open("../tmp/slotOccupancy.escript","w")
            fp.write(escript)
            fp.close()
            targetServer.copyToRemote("../tmp/slotOccupancy.escript","/tmp/","0755")
        slotDict ={}
        for slot in slotList:
            out = targetServer.executeCli("/tmp/slotOccupancy.escript %s"%slot)
            mat = re.match(".*\{(?P<total>\S+),(?P<used>\S+),(?P<ratio>\S+)\}",out,re.MULTILINE)
            if mat:
                #total = "%0.2f"%float(mat.group("total"))
                #used = "%0.2f"%float(mat.group("used"))
                ratio = "%0.2f"%(float(mat.group("ratio"))*100)
                slotDict[slot] = ratio
                # return total,used,ratio
        print slotDict
        return slotDict
        
    def addBots(self, ip, botCount, un="gor",pw="apj0702"):
        botCount = int(botCount)
        status = self.stopService("butler_remote", ip, un, pw)
        print "Butler remote status : %s"%status
        self.purgeButler(ip, un, pw)
        targetServer = sshbasic.sshbasic(ip,un,pw)
        cmd = "cat /etc/butler_remote/sys.config"
        targetServer.ssh.set_client_configuration(timeout = "60 sec")
        out = targetServer.executeCli(cmd)
        m = re.search("\{num_butlers, \d+\},",out,re.MULTILINE)
        if m:
            initCountStr = m.group()
            initCountStr.lstrip().strip()
        m = re.search("\{start_locations,.+\},",out,re.MULTILINE)
        if m:
            initLocStr = m.group()
            initLocStr.lstrip().strip()
            initLocStr = initLocStr.replace("[","\[")
            initLocStr = initLocStr.replace("]","\]")
        else:
            print "Not Found"
        finalCountStr = "{num_butlers, %d}, "%botCount
        locList = random.sample(self.getStorageInfo(ip,un,pw)[0],botCount)
        #locList = [u'043.015', u'032.013', u'025.021', u'044.025', u'027.011']
        locListStr = ",".join(["{\"%s\", 0}"%x for x in locList])
        finalLocStr = '{start_locations, \[%s\]},'%locListStr
       
        cmd = "sudo sed --follow-symlinks -i.bak -e 's/%s/%s/' -e 's/%s/%s/' /etc/butler_remote/sys.config"%(initCountStr,finalCountStr,initLocStr,finalLocStr)
        targetServer.ssh.set_client_configuration(timeout = "60 sec")
        out = targetServer.executeCli(cmd)
        
        status = self.startService("butler_remote", ip, un, pw)
        print "Butler remote status : %s"%status
        
        butlerInfo = self.getButlerInfo(ip, un, pw)
        
        if len(butlerInfo.keys()) == botCount:
            print "Butlers Added Successfully : %s"%str(butlerInfo)
            return self.unpauseButlers(ip, un, pw)
        else:
            print "Not able to add butler"
        return False

    def getpgsqlOutput(self,ip,query,un="gor",pw="apj0702"):
        targetServer = sshbasic.sshbasic(ip,un,pw)
        queryCommand = 'sudo -u postgres psql -d butler_interface -c "%s" >/tmp/query.out'%query
        targetServer.executeCli(queryCommand)
        targetServer.copyFromRemote("/tmp/query.out","../tmp/" )
        fp =  open('../tmp/query.out',"r")  
        dat1 =fp.readlines()
        fp.close()
        header = [x.strip() for x in dat1[0].split("|")]
        m = re.search("\((?P<total>\d+) row.*",dat1[-2])
        mainList = []
        total = 0
        if m:
            total =  int(m.groups()[0])
        if total > 0:
            for rows in range(2,total+2):
                values =  [x.strip() for x in dat1[rows].split("|")]
                tmpDict = dict(itertools.izip(header,values))
                mainList.append(tmpDict)
        return mainList
 
    def getRackDirection(self,barcode,allDict):
        if barcode in allDict:
            coordsAll = allDict[barcode]
            coords = [reduce(lambda a,b: a and b, x, True) for x in coordsAll]
            
            try:
                ind = coords.index(1)
                if ind in [0,2]:
                    return 1
                elif ind in [1,3]:
                    return 2
            except:
                print "Please correct your Map"+str(barcode) +str(coordsAll)
            return False
            
    def getRackType(self,ip,un="gor",pw="apj0702"):  
        targetServer = sshbasic.sshbasic(ip,un,pw)
        cmd = "sudo -k /opt/butler_server/bin/butler_server rpcterms racktype_rec get_all"
        targetServer.ssh.set_client_configuration(timeout = "60 sec")
        out = targetServer.executeCli(cmd) 
        tempList= re.findall(".*racktype_rec,\"(?P<racktype>\S+)\",.*",out,re.MULTILINE)
        racktypeList = tempList
        return racktypeList
        
    def createRackjson(self,ip, racks = {"auto":"Max"},upgrade = True ,jsonout = True, un="gor",pw="apj0702"):
        excludecoords = []
        allDict  = self.getGridInfo(ip,un,pw)
        allValidLoc = self.getStorageInfo(ip,un,pw)[0]
        for coord in excludecoords:
            allValidLoc.remove(coord)
        allRacks = self.getRackInfo(ip,un,pw).keys()
        if racks.has_key("auto"):
            count = racks["auto"]
        else:
            count = reduce(lambda a,b: a + b, racks.values())
        
        if count == "Max":
            racks.pop("auto")
            rackTypes = self.getRackType(ip, un, pw)
            for rackT in rackTypes:
                racks[rackT] = len(allValidLoc)/len(rackTypes)
            rem = len(allValidLoc)%len(rackTypes)
            racks[rackTypes[0]] = racks[rackTypes[0]]+rem
            if len(allRacks) != 0:
                if not upgrade:
                    print "Unable to create %s Racks but available %s only,either delete all racks Manually or use upgrade = True"%(str(len(allValidLoc)),str(len(list(set(allValidLoc)-set(allRacks)))))
                    return False
                else:
                    count = len(allValidLoc)
                    print "Will create %s Racks "%(str(count))
        else:
            if not upgrade:
                allValidLoc = list(set(allValidLoc)-set(allRacks))
                if len(allValidLoc) < count:
                
                    print "Unable to create %s Racks but available %s only,either delete all racks Manually or use upgrade = True"%(str(count),str(len(allValidLoc)))
                    return False
        
        tmp = 0
        mainList = []
        print len(allValidLoc),allValidLoc,count
        for r in racks.keys():
            for i in range(racks[r]):
                if tmp < len(allValidLoc):
                    position = allValidLoc[tmp]
                #print tmp, position
                    direction =  self.getRackDirection(position, allDict)
                    if direction:
                        tmpDict = {
                                   "position": position,
                                   "direction": direction,
                                   "last_store_position": position,
                                   "id": "%03d"%(tmp+1),
                                   "racktype": "%s"%r,
                                   "reserved_store_position": "undefined",
                                   "is_stored": True,
                                   "lifted_butler_id": None
                                   }
                    mainList.append(tmpDict)
                    tmp += 1
        #print mainList
        with open('racks.json', 'w') as fp:
            json.dump(mainList, fp,indent=2)
        print "rack.json created"
        
        
    def updateDefaultTypesRequired(self, ip, un="gor",pw="apj0702"):
        status = self.stopService("butler_server", ip, un, pw)
        trace.info("Butler server status : %s"%status, also_console=True)
        
        targetServer = sshbasic.sshbasic(ip,un,pw)
        cmd = "cat /etc/butler_server/sys.config"
        targetServer.ssh.set_client_configuration(timeout = "60 sec")
        out = targetServer.executeCli(cmd)
        m = re.search("{default_types_required,.*}",out)
        if m:
            initSeatTypes = m.group()
            initSeatTypes.lstrip().strip()
            initSeatTypes = re.sub('\[','\\[',initSeatTypes)
            initSeatTypes = re.sub('\]','\\]',initSeatTypes)
            print "initSeatTypes: %s abcd"%(initSeatTypes)
        else:
            trace.info("Not Found: default_types_required", also_console=True)
            return False
        
        finalSeatTypes = "{default_types_required, \[{front,0},{back,0}\]}"
        print "Final Seat Type: %s"%(finalSeatTypes)
        
        cmd = "sudo sed --follow-symlinks -i.bak -e 's/%s/%s/' /etc/butler_server/sys.config"%(initSeatTypes,finalSeatTypes)
        print cmd
        trace.info("Command To be executed : %s"%(cmd), also_console=True)
        targetServer.ssh.set_client_configuration(timeout = "60 sec")
        out = targetServer.executeCli(cmd)
         
        self.restartSupervisorctl(ip, un, pw)
         
        status = self.startService("butler_server", ip, un, pw)
        trace.info("Butler server status : %s"%status, also_console=True)
         
        return True
    
    def restartSupervisorctl(self,ip,un="gor",pw="apj0702"):
        targetServer = sshbasic.sshbasic(ip,un,pw)
        cmd = "sudo supervisorctl reload"
        targetServer.executeCli(cmd)
        sleep(30)
        
        
if __name__ == "__main__":
    
#     butlerserver().addBots("192.168.9.88", 21)
    butlerserver().updateDefaultTypesRequired("192.168.9.38")
#     rack_dict = {"MSU SODIMAC 1":42, "MSU SODIMAC 2":50, "MSU SODIMAC 3":109, "MSU SODIMAC 4":289, "MSU SODIMAC 5":17, "MSU SODIMAC 6":79, "MSU SODIMAC 7":31, "MSU SODIMAC 8":73, }
#     butlerserver().createRackjson("192.168.9.42", racks = rack_dict, upgrade=True)
#     r = { 29:15,11:26,12:58}
#     '''Type 29    Qty - 15
# Type 11    Qty - 26
# Type 12    Qty - 58
#     {
#         "position": "027.031",
#         "direction": 0,
#         "last_store_position": "027.031",
#         "id": "030",
#         "racktype": "11",
#         "reserved_store_position": "undefined",
#         "is_stored": true,
#         "lifted_butler_id": null
#     },'''
     #print butlerserver().getpgsqlOutput("192.168.8.157","select product_uid as uid from products where pdfa_values = '{\\\"product_sku\\\": \\\"20\\\"}'")
    #butlerserver().addBots("192.168.8.206",7)
#     myDict = butlerserver().getRackInfo("172.104.43.21")
#     print len(myDict.keys())
#     cnt = {}
#     for i in myDict.keys():
#         t = myDict[i]["rackType"]
#         if cnt.has_key(t):
#             cnt[t] = cnt[t]+1
#         else:
#             cnt[t] = 1
#     print cnt
#     print butlerserver().createRackjson("172.104.43.21")
#     myDict = butlerserver().getRackInfo("172.104.43.21")
#     print len(myDict.keys())
#     cnt = {}
#     for i in myDict.keys():
#         t = myDict[i]["rackType"]
#         if cnt.has_key(t):
#             cnt[t] = cnt[t]+1
#         else:
#             cnt[t] = 1
#     print cnt
    #butlerserver().getRackType("192.168.8.131")
    #print butlerserver().getStorageInfo("192.168.8.131")[0]
    #print butlerserver().getRackInfo("192.168.8.131")
    #butlerserver().getStorageNode("192.168.8.108")
    #butlerserver().getSlotOccupancy("150.1.B.04-05-06.01", "192.168.8.181")
   
