#===============================================================================
# File name : config.py
# Description : To  parse config file

#===============================================================================
import os, sys
from pyExcelerator import parse_xls
#import gtraces
#import traceback
#trace = gtraces.logger
from robot.libraries.BuiltIn import BuiltIn
filename = "config_sanity.xls"
def datasheet(filename ="config_sanity.xls"):   
    #=======================================================================
    # Function datasheet
    # Description: To Parse excel sheet
    # Parameters:
    # filename : name of the excel file  
    # Return: dictionary of all the values
    #=======================================================================   
    config_dict = {}
    config_dict_csv = {} 
    for sheet_name, values in parse_xls(filename, 'cp1251'):
        sheet_dict = {}
        sheet_dict_csv = {}
        #print values          
        maxrow = 0
        maxcol = 0        
        for row, col in values.keys():
            if col == 0:
                if row > maxrow:
                    maxrow = row
            if row == 0:
                if col > maxcol:
                    maxcol = col                 
        for i in range(1,maxrow+1):
            row_dict = {}
            row_str = ""
            for j in range(0,maxcol+1):
                #print sheet_name,j  
                if values[(0,j)] == "tag" :
                    key =  str(values[(i,j)] )
                else: 
                    try:
                        value =  values[(i,j)]
                        if isinstance(value, float):
                            value = str(value)
                    except:
                        value = "NONE"                                   
                    row_dict[str(values[(0,j)])] = value
                    row_str += str(value) + ","
                    
            sheet_dict[key] = row_dict
            sheet_dict_csv[key] = row_str.strip(",")
        config_dict[sheet_name] = sheet_dict
        config_dict_csv[sheet_name] = sheet_dict_csv
    return config_dict,config_dict_csv

def getdata_old(filename,spreadsheet):
    #=======================================================================
    # Function getdata
    # Description: To get the data from parsed excel file, read the excel sheet filename form argument.
    # Parameters:none
    # Return: dictionary of all the values
    #=======================================================================  
    if len(sys.argv) > 1:
        filename = sys.argv[1]
        try:
            config_dict = datasheet(filename)[0]
        except IOError:
#	    trace.error("Wrong Config File")
            print "Wrong Config File"
            
            sys.exit()        
        else:
            return config_dict        
    else:
        errorMsg = "Config File is missing, ignore if not required " 
        print errorMsg
# 	trace.warning(errorMsg)

def getdata(filename):
    #=======================================================================
    # Function getdata
    # Description: To get the data from parsed excel file, read the excel sheet filename form argument.
    # Parameters:none
    # Return: dictionary of all the values 
    #======================================================================= 
    fileSplit = os.path.split(filename)
    if len(fileSplit[0]) == 0:
        filename = "%s/%s"%(BuiltIn().get_variable_value("${conf_pth}"),filename)
    config_dict = datasheet(filename)[0]
    return config_dict        

def getdataCSV():
    #=======================================================================
    # Function getdata
    # Description: To get the data from parsed excel file, read the excel sheet filename form argument.
    # Parameters:none
    # Return: dictionary of all the values
    #=======================================================================  
    if len(sys.argv) > 1:
        filename = sys.argv[1]
        try:
            config_dict = datasheet(filename)[1]
        except IOError:
# 	    trace.error("Wrong Config File")
            print "Wrong Config File"
            
            sys.exit()        
        else:
            return config_dict        
    else:
        errorMsg = "Config File is missing, ignore if not required " 
        print errorMsg
# 	trace.warning(errorMsg)
        
#====================================================================
#  test code below for unit test, will not execute at higher level
#====================================================================
if __name__ == "__main__":
    print getdata(filename="/home/demo/workspace/test/goal/conf/config_sanity.xls")["pdea"]
#     print getdata()["ipfix_exporter"]
#    config = datasheet("config_sanity.xls")[1]
#    for i in config.keys():
#        for tags in config[i]:
#            print tags,config[i][tags],type(tags)
