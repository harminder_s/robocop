import ConfigParser
import traceback
import base64
import getpass
import io
from robot.libraries.BuiltIn import BuiltIn


class configparser:
    def __init__(self, confPath, confFileName):
        self.confPath = confPath
        self.confFileName = "%s/%s" % (confPath, confFileName)
        self.confDict = {}
        self.config = ConfigParser.RawConfigParser()
        try:
            fp = open(self.confFileName, "r")
        except IOError:
            BuiltIn().fatal_error("%s not found, please check the name and try again" % self.confFileName)

        data = fp.read()
        fp.close()
        self.config.readfp(io.BytesIO(data))

    def parseConfig(self):
        self.moduleList = self.config.sections()
        for module in self.moduleList:
            if module == "pps":
                username = self.config.get("pps", "username")
                passwd = self.config.get("pps", "passwd")
                if passwd == "hidden":
                    value = "encoded%s" % base64.b64encode(self.userPassword(username))

                ppsList = self.config.items("pps")

                for (key, value) in ppsList:
                    if key not in ["username", "passwd"]:
                        self.confDict["%s.%s" % (module, key)] = value
            else:
                for (key, value) in self.config.items(module):
                    self.confDict["%s.%s" % (module, key)] = value

        return self.confDict

    def userPassword(self, txt):
        try:
            return getpass.getpass("\n Please Enter password-> user:%s  and press enter: " % (txt))
        except Exception:
            errorMsg = "Error:  %s" % traceback.format_exc()
            BuiltIn().fail(errorMsg)
            return False

if __name__ == "__main__":
    a = configparser("/home/demo/workspace/test/goal/conf", "main.cfg")
    print a.parseConfig()
