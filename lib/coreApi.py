'''
Created on 20-Apr-2016

@author: nishantmahawar
'''

from StringIO import StringIO
import httplib, json
import ssl
#from time import sleep


#class 
class coreApi:
    """coreApi class consists of API's used by almost all API call """ 
    def __init__(self, host, username = "admin", password ="apj0702"):
        """This constructor is used to initialize the coreApi object and attach authKey, username, password and host IP to the object"""
        
#         self.webservice = httplib.HTTPSConnection(host)
        try:
            self.webservice = httplib.HTTPSConnection(host, timeout=60, context=ssl._create_unverified_context())
        except Exception:
            self.webservice = httplib.HTTPSConnection(host, timeout=60)
            
        
        self.username = username
        self.password = password
        self.host = host
        self.genAuth()
        
    def genAuth(self):
        api_url = '/api/auth/token'
        myjson = '''{
          "username": \"%s\",
          "password": \"%s\"
        }'''%(self.username,self.password)
        out = self.executeApi(api_url,myjson, False)
        self.authToken = str(out[1]['auth_token'])
        print "Authtoken Generated"
        
    def executeApi(self, api_url, myjson = False ,authRequired = True):
        """ Execute the api. Input needed is : api_url, json and request_type """
        if myjson:
            self.webservice.putrequest("POST", api_url)
        else:
            self.webservice.putrequest("GET", api_url)
        self.webservice.putheader("Host", self.host)
        self.webservice.putheader("Content-type", "application/json")
        if authRequired:
            self.webservice.putheader("Authentication-Token",self.authToken)
        if myjson:
            self.webservice.putheader("Content-length", "%d" % len(myjson))
        self.webservice.endheaders()
        if myjson:
            self.webservice.send(myjson)
        out =  self.webservice.getresponse() 
        status = int(out.status)
        data = out.read()
        #print data
        if len(data) > 0:
            io = StringIO(data)
            #print io
            response_json =  json.load(io)
            return status,response_json
        else:
            return status,False
        
        
    def executePutApi(self, api_url, myjson = False ,authRequired = True):
        """ Execute the api. Input needed is : api_url, json and request_type """
        print api_url,myjson
#         if myjson:
        self.webservice.putrequest("PUT", api_url)
#          else:
#              self.webservice.putrequest("GET", api_url)
        self.webservice.putheader("Host", self.host)
        self.webservice.putheader("Content-type", "application/json")
        if authRequired:
            self.webservice.putheader("Authentication-Token",self.authToken)
        if myjson:
            self.webservice.putheader("Content-length", "%d" % len(myjson))
        self.webservice.endheaders()
        if myjson:
            self.webservice.send(myjson)
        out =  self.webservice.getresponse() 
        status = int(out.status)
        data = out.read()
        print status,data
        if len(data) > 0:
            io = StringIO(data)
            response_json =  json.load(io)
            return status,response_json
        else:
            return status,False

    def executeDeleteApi(self, api_url, authRequired = True):

        self.webservice.putrequest("DELETE", api_url)

        self.webservice.putheader("Host", self.host)
        self.webservice.putheader("Content-type", "application/json")
        if authRequired:
            self.webservice.putheader("Authentication-Token", self.authToken)

        self.webservice.endheaders()

        out = self.webservice.getresponse()
        status = int(out.status)
        data = out.read()


        if len(data) > 0:
            io = StringIO(data)
            response_json = json.load(io)
            return status, response_json
            return status
        else:
            return status, False


if  __name__ == "__main__":
    obj = coreApi("192.168.8.175", "admin", "apj0702")
    #auth_key = obj.authToken
    # print obj.executeApi("/api/pps")
    a = obj.executeDeleteApi("/api/audit/delete/ut6qwBNmtB")
    print a