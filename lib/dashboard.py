import time
import uievents


class dashboard:
    def __init__(self, serverIP, username="admin", password="apj0702", execution="remote"):
        self.username = username
        self.password = password

    def openBrowser(self, serverIP, username, password, execution):
        self.ui = uievents.uiEvents(serverIP, username, password, execution)
        
    def loginDashboard(self):
        self.ui.inputText("login.username",self.username)
        self.ui.inputText("login.password",self.password)
        assert self.ui.awaitForElementPresence("login.loginbutton", 10), "Page Load taking long time."
        self.ui.submitButton("login.loginbutton")
        assert self.ui.awaitForElementPresence("header.mainname", 30), "Page Load taking a long time."
        self.verifyDashboard()

    def verifyDashboard(self):
        assert self.ui.awaitForElementPresence("header.itemstocked", 30), "Page Load taking long time."
        assert self.ui.awaitForElementPresence("header.itemsaudited", 30), "Page Load taking long time."
        assert self.ui.awaitForElementPresence("header.orderstofullfull", 30), "Page Load taking long time."

    def verifyAllTabs(self):
        ################# Verify Overview tab #################
        assert self.ui.awaitForElementPresence("tabs.overviewtab", 30), "Overview Tab Load is taking long time."
        self.ui.clickButton("tabs.overviewtab")
        assert self.ui.awaitForElementPresence("header.itemstocked", 30), "Overview Tab Load taking long time."
        assert self.ui.awaitForElementPresence("header.itemsaudited", 30), "Overview Tab Load taking long time."
        assert self.ui.awaitForElementPresence("header.orderstofullfull", 30), "Overview Tab Load taking long time."

        ################# Verify System tab #################
        self.ui.clickButton("tabs.systemtab")
        assert self.ui.awaitForElementPresence("header.toolbar", 30), "System Tab Load taking long time."
        assert self.ui.awaitForElementPresence("header.filterbtndata", 30), "System Tab Load taking long time."
        assert self.ui.awaitForElementPresence("header.systembutletbotslink", 30), "Butler Bots sub tab is not present."
        assert self.ui.awaitForElementPresence("header.systemppslink", 30), "PPS sub tab is not present."
        assert self.ui.awaitForElementPresence("header.systemcslink", 30), "Charging Station sub tab is not present."

        ################# Verify Orders tab #################
        self.ui.clickButton("tabs.orderstab")
        assert self.ui.awaitForElementPresence("header.toolbar", 30), "Order Tab Load taking long time."
        assert self.ui.awaitForElementPresence("header.filterbtndata", 30), "Order Tab Load taking long time."
        assert self.ui.awaitForElementPresence("header.orderwaveslink", 30), "Waves sub tab is not present."
        assert self.ui.awaitForElementPresence("header.orderlistlink", 30), "Order List sub tab is not present."

        ################# Verify Audit tab #################
        self.ui.clickButton("tabs.audittab")
        assert self.ui.awaitForElementPresence("header.toolbar", 30), "Order Tab Load taking long time."
        assert self.ui.awaitForElementPresence("header.filterbtndata", 30), "Order Tab Load taking long time."
        assert self.ui.awaitForElementPresence("header.auditcreatebtndata", 30), "Create Audit Button is not present."
        self.ui.clickButton("header.auditcreatebtn")
        assert self.ui.awaitForElementPresence("header.createnewauditlabel", 30), "Create New Audit Page Load taking long time."
        self.ui.clickButton("header.createauditclose")
        assert self.ui.awaitForElementToBeVisible("header.toolbar", 30), "Order Tab Load taking long time."

        ################# Verify Inventory tab #################
        self.ui.clickButton("tabs.inventorytab")
        assert self.ui.awaitForElementPresence("header.inventoryheader", 30), "Inventory Tab Load taking long time."
        assert self.ui.awaitForElementPresence("header.inventorysnapshot", 30), "Snap Shot hasn't loaded on Inventory Tab."
        assert self.ui.awaitForElementPresence("header.inventorystockhistory", 30), "Stock Level History hasn't loaded on Inventory Tab."
        assert self.ui.awaitForElementPresence("header.inventoryitemmovementgraph", 30), "Items Movement Graph hasn't loaded on Inventory Tab"

        ################# Verify Users tab tab #################
        self.ui.clickButton("tabs.userstab")
        assert self.ui.awaitForElementPresence("header.toolbar", 30), "Users Tab Load taking long time."
        assert self.ui.awaitForElementPresence("header.newuserbtn", 30), "Add New User Button is not present"
        self.ui.clickButton("header.newuserbtn")
        assert self.ui.awaitForElementPresence("header.addnewuserlabel", 30), "Add New User label is not present"
        self.ui.clickButton("header.addnewuserclosebtn")
        assert self.ui.awaitForElementToBeVisible("header.userslabel", 30), "Users Tab Load taking long time"

    def logoutDashboard(self):
        self.ui.hoverTo("header.profile")
        self.ui.clickButton("login.logoutbutton")
        self.ui.acceptAlert("login.logoutaccept")

    def closeBrowser(self):
        self.logoutDashboard()
        self.ui.driver.close()

if __name__ == "__main__":
    db = dashboard("192.168.8.155",execution = "remote")