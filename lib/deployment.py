import os, time
#from urlparse import urlparse

#from jenkinsapi.jenkins import Jenkins
from robot.libraries.BuiltIn import BuiltIn

import sshbasic
class deployment:
    
    def __init__(self):
        pass

    def initEnv(self,targetIP,targetUN="gor",targetPW="apj0702"):
        targetServer = sshbasic.sshbasic(targetIP,targetUN,targetPW)
        extra_path = BuiltIn().get_variable_value("${extra_pth}")
        #if not targetServer.fileExist("/etc/apt/detect-http-proxy"):
        targetServer.copyToRemote("%s/detect-http-proxy"%extra_path, "/home/gor")
        targetServer.executeCli("sudo cp /home/gor/detect-http-proxy /etc/apt/")
        targetServer.executeCli("sudo chmod +x /etc/apt/detect-http-proxy")
        #if not targetServer.fileExist("/etc/apt/apt.conf.d/02proxy"):
        targetServer.copyToRemote("%s/02proxy"%extra_path, "/home/gor")
        targetServer.executeCli("sudo cp /home/gor/02proxy /etc/apt/apt.conf.d")
        targetServer.executeCli("sudo chmod +x /etc/apt/apt.conf.d/02proxy")
        targetServer.executeCli("sudo apt-get update")
        targetServer.executeCli("sudo apt-get upgrade -y -q")
    
    def installBuild(self,build,targetIP,targetUN="gor",targetPW="apj0702",):
        targetServer = sshbasic.sshbasic(targetIP,targetUN,targetPW)
        targetServer.copyToRemote(build, "/tmp")
        targetServer.executeCli("cd /tmp")
        buildName = os.path.basename(build)
        targetServer.ssh.set_client_configuration(timeout = "30 sec")
        targetServer.executeCli("tar -xvzf %s"%buildName)
        targetServer.ssh.set_client_configuration(timeout = "30 min")
        targetServer.executeCli("cd /tmp/butler_installation_scripts")
        targetServer.executeCli("sudo ./install.sh %s"%targetIP)
        time.sleep(10)
        
    def configureAuth(self,authIP, targetIP,targetUN="gor",targetPW="apj0702"):
        targetServer = sshbasic.sshbasic(targetIP,targetUN,targetPW)
        targetServer.ssh.set_client_configuration(timeout = "30 min")     #   /api-gateway/auth-service/platform-auth/"
        cmd = 'sudo sed -i --follow-symlinks "s/auth_server_url.*$/auth_server_url      => \\\"http:\/\/%s:8080\/api-gateway\/auth-service\/platform-auth\/\\\",/" /etc/butler_server/sys.config'%authIP
        targetServer.executeCli(cmd)
        cmd = "sudo service butler_server restart"
        targetServer.executeCli(cmd)
        cmd = 'sudo sed -i --follow-symlinks "s/\'URL\'.*$/\'URL\': \'http:\/\/%s:8080\/api-gateway\/auth-service\/platform-auth\',/" /opt/butler_interface/butler_interface/config.py'%authIP
        targetServer.executeCli(cmd)
        cmd = "sudo supervisorctl reload"
        targetServer.executeCli(cmd)
        
    def configureBuild(self,targetIP,targetUN="gor",targetPW="apj0702"):
        targetServer = sshbasic.sshbasic(targetIP,targetUN,targetPW)
        targetServer.ssh.set_client_configuration(timeout = "30 min")
        targetServer.executeCli("cd /tmp/butler_installation_scripts")
        time.sleep(10)
        targetServer.executeCli("sudo chmod +x setup_map_pps_chargers.sh")
        time.sleep(10)
        targetServer.executeCli("printf '%s'|./setup_map_pps_chargers.sh"%targetIP)
        time.sleep(10)
        targetServer.executeCli("printf '%s'|./setup_test_data.sh true"%targetIP)
           
if  __name__ == "__main__":
    #build = deployment().getlatestBuildPath()
   # print build
    deployment().configureAuth("192.168.8.167","192.168.9.25")
    deployment().configureBuild("192.168.9.25")