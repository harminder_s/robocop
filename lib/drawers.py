import sshbasic 
from butlerserver import *
import json
from coreApi import coreApi
from apiHelper import apiHelper
from robot.libraries.BuiltIn import BuiltIn
import os
class drawers:
    def delete_all_racks(self,host,ssh_username="gor",password="apj0702"):
        cmd = "sudo /opt/butler_server/bin/butler_server rpcterms butler_test_functions deleter"
        deleteRack_output=r'[ok]'
        self.obj=butlerserver()
        out= self.obj.executingButlerRemoteCommands(host, cmd,un=ssh_username,pw= password)
        if re.match(deleteRack_output,out):
            return True
        else:
            return False


    def curl_drawer_racks(self,host,racktype_source_file='Users/anjali.j/Downloads/racktype.json',username="admin",password="apj0702"): 
        f = open(racktype_source_file)
        content_json = f.read()
        f.close()
        content_json = content_json.encode()
        if not hasattr(self, "obj"):
            self.obj = coreApi(host,username,password)
        api_url = '/api/components/call_server_endpoint'
        trace.info(content_json, also_console=True)
        (status, response_json) = self.obj.executeApi(api_url, content_json)
        print response_json
        trace.info("Status: %s"%(str(status)), also_console=True)
        trace.info("curl racks: %s"%(str(response_json)), also_console=True)

    def add_single_rack(self,host,rack_id="004", rack_face=002, rack_location="023.021", rack_type="11",ssh_username="gor",password="apj0702"): #make all variable in small case
        targetServer = sshbasic.sshbasic(host,un=ssh_username,pw=password)  
        fileList =  targetServer.ssh.list_directory("/opt/butler_server")
        for eFileName in fileList:
            if eFileName.find("erts-") != -1:
                eFile = eFileName
        escriptName = "add_new_rack_easy.escript"
#         localEscriptPth = "%s/%s"%(BuiltIn().get_variable_value("${tmp_pth}","../tmp"),escriptName)
        localEscriptPth = "/tmp/%s"%escriptName                             
        remoteEsciptPth = "/tmp/%s"%escriptName
        if not targetServer.fileExist(remoteEsciptPth):
            trace.info("File doesn't exist, creating and uploading",also_console=True)
            escript = '''#!/opt/butler_server/%s/bin/escript
%%%%! -sname butler -setcookie butler_server
     
     main(Args) ->
        Node = 'butler_server@localhost',
        Mod = 'butler_rack_functions',
        Fun = 'add_new_rack_easy',
        [Rack_id, Rack_face, Rack_location, Rack_type] = Args,
        RackId = erlang:list_to_binary(Rack_id),
        RackFace = erlang:list_to_integer(Rack_face),
        R = rpc:call(Node, Mod, Fun, [RackId, RackFace, Rack_location, Rack_type]),
       io:format("butler_rack_functions ~p~n",[R]).'''%eFile
            fp = open(localEscriptPth,"w")
            fp.write(escript)
            fp.close()
            targetServer.copyToRemote(localEscriptPth,remoteEsciptPth,"0755")
        targetServer.executeCli('/tmp/add_new_rack_easy.escript "%s" %d "%s" "%s"' %(rack_id, int(rack_face), rack_location, rack_type ))
                                
    def add_multiple_racks(self,host,rack_faces=2, num_of_racks=10, rack_type='421', starting_rack_id=3,ssh_username="gor",password="apj0702"):
        targetServer = sshbasic.sshbasic(host,ssh_username,password)  
        fileList =  targetServer.ssh.list_directory("/opt/butler_server")
        for eFileName in fileList:
            if eFileName.find("erts-") != -1:
                eFile = eFileName
        escriptName = "fill_racks_in_empty_storable_position.escript" 
        localEscriptPth = "%s/%s"%(BuiltIn().get_variable_value("${tmp_pth}","../tmp"),escriptName)                             
        remoteEsciptPth = "/tmp/%s"%escriptName       
        if not targetServer.fileExist("/tmp/fill_racks_in_empty_storable_position.escript"):
            print "File Not exist, creating and uploading"
            escript = '''#!/opt/butler_server/%s/bin/escript
%%%%! -sname butler -setcookie butler_server
     
     main(Args) ->
        Node = 'butler_server@localhost',
        Mod = 'butler_test_functions',
        Fun = 'fill_racks_in_empty_storable_position',
        [Rack_faces, Num_of_racks, Rack_type, Starting_rack_id] = Args,
        RackFace = erlang:list_to_integer(Rack_faces),
        RackNumbers = erlang:list_to_integer(Num_of_racks),
        RackIdStart = erlang:list_to_integer(Starting_rack_id), 
        R = rpc:call(Node, Mod, Fun, [RackFace, RackNumbers, Rack_type,RackIdStart]),
       io:format("butler_test_functions ~p~n",[R]).'''%eFile
            fp = open(localEscriptPth,"w")
            fp.write(escript)
            fp.close()
            targetServer.copyToRemote(localEscriptPth,remoteEsciptPth,"0755")
        targetServer.executeCli('/tmp/fill_racks_in_empty_storable_position.escript %d %d "%s" %d' %(int(rack_faces), int(num_of_racks), rack_type, int(starting_rack_id)))
        

     
    def create_storage_strategy(self,host,storageStrategy_source_path="/Users/anjali.j/Downloads/test.csv",storageStrategy_destination_path="/home/gor/drawers/test.csv",username="admin",ssh_username="gor",password="apj0702"): 
        ssh_obj = sshbasic.sshbasic(host,un=ssh_username, pw=password)
        if not os.path.exists(storageStrategy_source_path):
            trace.debug( "source file %s not found"%(storageStrategy_source_path))
            BuiltIn().fail("source file %s not found"%(storageStrategy_source_path))
            
        if not os.path.exists(storageStrategy_destination_path):
            trace.info( "Destination directory not found,creating directory",also_console=True)
            try: 
                os.makedirs(storageStrategy_destination_path)
            except OSError:
                pass
        trace.info(host, also_console=True)
        ssh_obj.copyToRemote(storageStrategy_source_path,storageStrategy_destination_path,mode="0744")
        self.obj = coreApi(host,username,password)
        cmd = '''curl -i -k -X POST -H "Content-Type: multipart/form-data" -F "file=@%s" -H "Authentication-Token:%s" https://%s/api/storage_strategy/sheet'''%(storageStrategy_destination_path,self.obj.authToken,host)
        ssh_obj.executeCli(cmd)
        
        
        
    def set_Stacking(self,host,ssh_username="gor",password="apj0702",slot_type="drawers",length="true",breadth="true",height="false"):
        targetServer = sshbasic.sshbasic(host,un=ssh_username,pw=password)  
        fileList =  targetServer.ssh.list_directory("/opt/butler_server")
        for eFileName in fileList:
            if eFileName.find("erts-") != -1:
                eFile = eFileName
        escriptName = "set_stacking_enabled_vector_for_slot_type.escript"
        localEscriptPth = "%s/%s"%(BuiltIn().get_variable_value("${tmp_pth}","../tmp"),escriptName)                             
        remoteEsciptPth = "/tmp/%s"%escriptName
        if not targetServer.fileExist(remoteEsciptPth):
            trace.info("File doesn't exist, creating and uploading",also_console=True)
            escript = '''#!/opt/butler_server/%s/bin/escript
%%%%! -sname butler -setcookie butler_server
     
     main(Args) ->
        Node = 'butler_server@localhost',
        Mod = 'put_functions',
        Fun = 'set_stacking_enabled_vector_for_slot_type',
        [Slot_type,Length,Breadth,Height] = Args,
        Vectors = {erlang:list_to_atom(Length), erlang:list_to_atom(Breadth), erlang:list_to_atom(Height)},
        Slottype = erlang:list_to_binary(Slot_type),
         R = rpc:call(Node, Mod, Fun, [Slottype,Vectors]),
       io:format("butler_rack_functions ~p~n",[R]).'''%eFile
            fp = open(localEscriptPth,"w")
            fp.write(escript)
            fp.close()
            targetServer.copyToRemote(localEscriptPth,remoteEsciptPth,"0755")
        targetServer.executeCli('/tmp/set_stacking_enabled_vector_for_slot_type.escript "%s" "%s" "%s" "%s"' %(slot_type,length,breadth,height))
                                
    def get_Stacking(self,host,ssh_username,password,slot_type):
        targetServer = sshbasic.sshbasic(host,un=ssh_username,pw=password)  
        fileList =  targetServer.ssh.list_directory("/opt/butler_server")
        for eFileName in fileList:
            if eFileName.find("erts-") != -1:
                eFile = eFileName
        escriptName = "get_stacking_enabled_vector_for_slot_type.escript"
        localEscriptPth = "%s/%s"%(BuiltIn().get_variable_value("${tmp_pth}","../tmp"),escriptName)  
        remoteEsciptPth = "/tmp/%s"%escriptName
        if not targetServer.fileExist(remoteEsciptPth):
            trace.info("File doesn't exist, creating and uploading",also_console=True)
            escript = '''#!/opt/butler_server/%s/bin/escript
%%%%! -sname butler -setcookie butler_server
     
     main(Args) ->
        Node = 'butler_server@localhost',
        Mod = 'put_functions',
        Fun = 'get_stacking_enabled_vector_for_slot_type',
        [Slot_type] = Args,
        Slottype = erlang:list_to_binary(Slot_type),
        R = rpc:call(Node, Mod, Fun, Slottype),
       io:format("butler_rack_functions ~p~n",[R]).'''%eFile
            fp = open(localEscriptPth,"w")
            fp.write(escript)
            fp.close()
            targetServer.copyToRemote(localEscriptPth,remoteEsciptPth,"0755")
        targetServer.executeCli('/tmp/set_stacking_enabled_vector_for_slot_type.escript "%s" ' %(slot_type))

        
        
        
        
            
if  __name__ == "__main__":
#     drawers().curl_drawer_racks(host="192.168.8.212",racktype_source_file='/Users/anjali.j/Downloads/racks.json',username="admin",password="apj0702")
#     drawers().delete_all_racks("192.168.8.206")
#      drawers().set_Stacking("192.168.8.63",ssh_username="gor",password="apj0702",slot_type="drawer",length="true",breadth="true",height="false")
#     drawers().get_Stacking("192.168.8.63",ssh_username="gor",password="apj0702",slot_type="drawer")
    drawers().add_single_rack("192.168.8.206")
#     drawers().create_storage_strategy("192.168.8.212")