import sys, subprocess, re

class githelper:
    
    def executeCmds(self,cmd):
        out = subprocess.check_output(cmd,stderr=subprocess.STDOUT,shell=True)
        return out
    
    def executeCmdstatus(self,cmd):
        out = subprocess.check_call(cmd,stderr=subprocess.STDOUT,shell=True)
        return out
    
    def getVersions(self,packageTar,apps):
        out = self.executeCmds("tar -tvf %s|awk '{print $6}'"%packageTar)
        appDict = {}
        for line in out.splitlines():
            if line.find(".deb") != -1:
                for app in apps:
                    if line.find(app) != -1:
                        debName = line.strip("./")
                        tmp = re.findall(".*\W{1}(?P<commitId>\w{7,8})[\._-].*",debName)
                        if len(tmp)>0:
                            gitVersion =tmp[0].lstrip('g')
                        else: 
                            gitVersion = False
                        appDict[app] = {"debian" :debName,"gitVersion" :gitVersion}
        return appDict
    
    def gitOperations(self,packagetar,apps,branch,myTag):
        appList = apps.keys()
        verDict = self.getVersions(packagetar,appList)
        for key in apps.keys():
            if self.executeCmdstatus("cd %s;git pull"%apps[key]) == 0:
                if self.executeCmdstatus("cd %s;git checkout -b %s %s"%(apps[key],branch,verDict[key]['gitVersion'])) == 0:
                    if self.executeCmdstatus("cd %s;git push --set-upstream origin %s"%(apps[key],branch)) == 0:
                        if self.executeCmdstatus("cd %s;git tag %s"%(apps[key],myTag)) == 0:
                            if self.executeCmdstatus("cd %s;git push --tag"%(apps[key])) == 0:
                                print "Branch %s : Tag %s Created for %s"%(branch,tag,key)

    '''
    git pull
    git checkout -b rc/sprint-29 <<commit_id>>
git push --set-upstream origin rc/sprint-29
git tag 3.2-s29 <<commit_id>>
git push --tag
for each repo these 4 steps need to be done'''
        
if __name__ == "__main__":
    packagetar ="/home/gor/robocop/tmp/installPackage_1_5_0_353.tar.gz"
    branch = "rc/sprint-30"
    tag = "3.3-s30"
    apps={"ButlerServer":"/home/gor/butler_server",
          "butler_md_alpha":"/home/gor/gor.butler.ui.managerdashboard",
          "butler-interface":"/home/gor/butler_interface",
          "butler_ui":"/home/gor/butlerui",
          "ButlerRemote":"/home/gor/butler_remote"}
    githelper().gitOperations(packagetar,apps,branch,tag)
    #githelper().getVersions("/home/gor/robocop/tmp/installPackage_1_5_0_353.tar.gz",apps.keys())