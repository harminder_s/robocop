from coreApi import coreApi
import butlerserver
from pyExcelerator import Workbook
from pyExcelerator.Style import XFStyle
import re

invdict = {}
def add_to_dict(res_dict,key,value):
        
    if res_dict.has_key(key):
        v = res_dict[key]
        v.append(value)
        res_dict[key] = v
    else:
        res_dict[key] =[value]

idict ={}

def getProductbyID(obj,ids):
    api_url = "/api/products/%s"%ids
    (status,response_json) = obj.executeApi(api_url)
    #print status
    res = response_json["product"]
    try:
        storage_strategy = res['extra_fields']['vertical']
    except:
        storage_strategy =False
    try:
        storage_tag_list = res['extra_fields']['storage_tag_list']
    except:
        storage_tag_list = False
    productsku = res["pdfa_values"]["product_sku"]
   
    
    return productsku,storage_strategy,storage_tag_list
    
def inventory(host):
    obj = coreApi(host)
    pageCount =1
    while  True:
        #print pageCount
        api_url = "/api/inventories?page=%d&PAGE_SIZE=500"%pageCount
        print api_url
        (status,response_json) = obj.executeApi(api_url)
        if len(response_json["inventories"]) > 0:
            print "%d found"%pageCount
            #print response_json
            
            for i in response_json["inventories"]:
                if i["uid_type"]=="item":
                    pattern = "A00000"
                    if(not re.search(pattern, str(i["uid"]))):
                        prod = getProductbyID(obj,i["uid"])
                        add_to_dict(idict,i["location"],{"item":prod[0],"count":i["actual_quantity"],"strategy":prod[1],"tag":prod[2]})
        else:
            print "%d not found,exiting"%pageCount
            break
        pageCount += 1
            
def main(host):
    inventory(host)
    for ikey in idict:
        rack = ikey.split(".")[0]
        add_to_dict(invdict,rack,{ikey:idict[ikey]})
    #print invdict
    return invdict


def inventoryBySku(d):
    skudict = {}
    for rackKey in d:
        for slots in d[rackKey]:
            for itemKey in slots.values()[0]:
                add_to_dict(skudict,itemKey.keys()[0],{slots.keys()[0]:itemKey.values()[0]})
    #print skudict
    return skudict
             

def createReport(ip,maindict,indict):
    rackDict = butlerserver.butlerserver().getStorageNode(ip)
    wb = Workbook()
    ws1 = wb.add_sheet("Racks")
    style1 = XFStyle()
    style1.font.name = 'Arial'            
    style1.font.bold = True
    style1.font.colour_index = 0
    
    style2 = XFStyle()
    style2.font.name = 'Arial'            
    style2.font.bold = True
    style2.font.colour_index = 2
    
    style3 = XFStyle()
    style3.font.name = 'Arial'            
    style3.font.bold = True
    style3.font.colour_index = 1
    
    count = 1
    ws1.write(0,0,"S.NO.",style1)
    ws1.write(0,1,"Rack",style1)
    ws1.write(0,2,"Slot",style1)
    ws1.write(0,3,"Allocation",style1)
    ws1.write(0,4,"Rack_slot_tag",style1)
    ws1.write(0,5,"Rack_slot_type",style1)
    ws1.write(0,6,"Item",style1)
    ws1.write(0,7,"Count",style1)
    ws1.write(0,8,"storage_strategy",style1)
    #ws1.write(0,9,"storage_tag_list",style1)
    #storage_strategy,storage_tag_list
    slotDict = butlerserver.butlerserver().getSlotOccupancy([x for x in idict.keys()], ip)
    
    row = 1
    for rackKey in maindict:
        ws1.write(row,0,count,style1)
        count += 1
        ws1.write(row,1,rackKey,style1)
        for slots in maindict[rackKey]:
            #try:
                try:
                    myslot = rackDict[slots.keys()[0]]#.lstrip("0")]
                    slotTag = myslot["slotTag"].lstrip("<<\"").strip("\">>")
                    slotType = myslot["slotType"].lstrip("<<\"").strip("\">>")
                except:
                    slotTag = "Error: %s"%slots.keys()[0]
                    slotType = "Error: %s"%slots.keys()[0]
                ws1.write(row,4,str(slotTag),style1)
                ws1.write(row,5,str(slotType),style1)
                if len(slots.values()[0]) >1:
                    ws1.write(row,2,slots.keys()[0],style2)
                else:
                    ws1.write(row,2,slots.keys()[0],style1)
                try:
                    ws1.write(row,3,slotDict[slots.keys()[0]],style1)
                except:
                #    ws1.write(row,3,slotDict[slots.keys()[0].lstrip('0')],style1)
                    ws1.write(row,3,"Error: %s"%slots.keys()[0],style1)
                #print slots, type(slots)
                iList =[]
                for itemKey in slots.values()[0]:
                    ws1.write(row,6,itemKey["item"],style1)
                    ws1.write(row,7,itemKey["count"],style1)
                    ws1.write(row,8,str(itemKey["strategy"]),style1)
                    
                    iList.append(itemKey.values()[0])
                    row += 1
                iList = list(set(iList))
            #except:
                # print "%s NotFound"%slots.keys()[0]
            #if len(iList) > 1:
            #  ws1.write(row,2,slots.keys()[0],style2)
                 
    ws2 = wb.add_sheet("Items")
    count = 1
    ws2.write(0,0,"S.NO.",style1)
    ws2.write(0,1,"Item",style1)
    ws2.write(0,2,"Slot",style1)
    ws2.write(0,3,"Count",style1)
    row = 1
    for sKey in indict:
        ws2.write(row,0,count,style1)
        count += 1
        ws2.write(row,1,sKey,style1)
        for slots in indict[sKey]:
            ws2.write(row,2,slots.keys()[0],style1)
            ws2.write(row,3,slots.values()[0],style1)
            row += 1
     
    wb.save("%s_test.xls"%ip)
    print "File generated"
    ## for log sheet
    

if __name__ == "__main__":
    host = "192.168.8.212"
    #obj = coreApi(host)
    #print getStorageByID(obj)
    # print getProductbyID(obj,"08282d12-c851-442e-b4e1-fb6a626e09e7")
    #storage_node:get_by_id("030.0.B.04-05-06.02").
    d = main(host)
    #print d
    # slotList = idict.keys()
    #print slotList
    #print d
    i = inventoryBySku(d)
    createReport(host,d,i)
    
    