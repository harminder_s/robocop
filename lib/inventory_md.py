from dashboard import dashboard
from apiHelper import apiHelper


class inventory_md:
    actual_put_stock = {}
    actual_pick_stock = {}

    def __init__(self, serverIP, username="admin", password="apj0702"):
        self.username = username
        self.password = password
        self.api = apiHelper()

    def inventoryTabEvents(self, serverIP, username, password, execution):
        self.dash_obj = dashboard(serverIP, username="admin", password="apj0702", execution="remote")
        self.dash_obj.openBrowser(serverIP, username, password, execution)
        self.dash_obj.loginDashboard()
        self.dash_obj.ui.clickButton("tabs.inventorytab")

    def closeInventoryEvent(self):
        self.dash_obj.closeBrowser()

    def putBackFrontOperations(self, serverIP, username, password, barcodes, qty, pps_put_id):
        self.api.put_back_operations(serverIP, username, password, barcodes, qty, pps_put_id)
        self.api.put_front_operations(serverIP, username, password, pps_put_id)

    def pickFrontBackOperations(self, serverIP, username, password, pdfaValues, pps_pick_id):
        self.api.add_single_order(serverIP, username, password, pdfaValues)
        self.api.pick_front_process(serverIP, username, password, pps_pick_id)

    def fetchActualStock(self, serverIP, username, password, execution):
        self.dash_obj = dashboard(serverIP, username="admin", password="apj0702", execution="remote")
        self.dash_obj.openBrowser(serverIP, username, password, execution)
        self.dash_obj.loginDashboard()
        self.dash_obj.ui.clickButton("tabs.inventorytab")
        self.dash_obj.ui.forceWait(2)
        ##################### Fetch Actual Put Stock #####################
        actualPutInventoryStock = self.dash_obj.ui.getMultipleElementsText("header.putinventorystock")
        actual_put_dict = {actualPutInventoryStock[0]: actualPutInventoryStock[1]}
        self.actual_put_stock = actual_put_dict
        ##################### Fetch Actual Pick Stock #####################
        actualPickInventoryStock = self.dash_obj.ui.getMultipleElementsText("header.pickinventorystock")
        actual_pick_dict = {actualPickInventoryStock[0]: actualPickInventoryStock[1]}
        self.actual_pick_stock = actual_pick_dict
        self.dash_obj.closeBrowser()

    def verifyInventoryPutStock(self, serverIP, username, password, execution, barcodes, qty, pps_put_id):
        self.putBackFrontOperations(serverIP, username, password, barcodes, qty, pps_put_id)
        self.inventoryTabEvents(serverIP, username, password, execution)
        self.dash_obj.ui.forceWait(2)
        expectedPutInventoryStock = self.dash_obj.ui.getMultipleElementsText("header.putinventorystock")
        expected_put_dict = {expectedPutInventoryStock[0]: expectedPutInventoryStock[1]}
        actual_put_dict = self.actual_put_stock
        assert actual_put_dict != expected_put_dict, "Today's Snapshot Put didn't get updated. Actual Put Quantity:" + str(actual_put_dict) + ".Expected Put Quantity:" + str(expected_put_dict)

    def verifyInventoryPickStock(self, serverIP, username, password, execution, pps_pick_id, pdfaValues):
        self.pickFrontBackOperations(serverIP, username, password, pdfaValues, pps_pick_id)
        self.inventoryTabEvents(serverIP, username, password, execution)
        self.dash_obj.ui.forceWait(2)
        exepectedPickInventoryStock = self.dash_obj.ui.getMultipleElementsText("header.pickinventorystock")
        expected_pick_dict = {exepectedPickInventoryStock[0]: exepectedPickInventoryStock[1]}
        actual_pick_dict = self.actual_pick_stock
        assert actual_pick_dict != expected_pick_dict, "Today's Snapshot Pick didn't get updated. Actual Pick Quantity:" + str(actual_pick_dict) + ".Expected Pick Quantity:" + str(expected_pick_dict)
