import os, time, sys, requests
from urlparse import urlparse
from jenkinsapi.jenkins import Jenkins
from robot.api import logger as trace
from robot.libraries.BuiltIn import BuiltIn


class jenkinoperations:
    
    def connectJenkin(self,jenkinIP= "jenkins.greyorange.com", jenkinUN="abhishek@greyorange.sg", jenkinPW="2b8bf1d737ff60b7a84309d5e23c348d"):
        self.jenkinObj = Jenkins("http://%s"%jenkinIP, jenkinUN, jenkinPW)
        self.jenkinUN = jenkinUN
        self.jenkinPW = jenkinPW

    def fireOthers(self,branch = "dev"):
        if branch == "dev":
            joblist = {"ButlerInterface-develop-build":{"initbuild": None,"status" :None,"expectedBuild":None, "buildSuccess": False},
                "ButlerRemote-develop-build":{"initbuild": None,"status" :None,"expectedBuild":None, "buildSuccess": False},
                "ButlerServer-develop-build":{"initbuild": None,"status" :None,"expectedBuild":None, "buildSuccess": False},
                "ButlerUI-develop-build":{"initbuild": None,"status" :None,"expectedBuild":None, "buildSuccess": False},
                "ButlerMD-alpha-build":{"initbuild": None,"status" :None,"expectedBuild":None, "buildSuccess": False}}
        elif branch == "1.5":
            joblist = {"ButlerInterface-1.5-build":{"initbuild": None,"status" :None,"expectedBuild":None, "buildSuccess": False},
                "ButlerRemote-1.5-build":{"initbuild": None,"status" :None,"expectedBuild":None, "buildSuccess": False},
                "ButlerServer-1.5-build":{"initbuild": None,"status" :None,"expectedBuild":None, "buildSuccess": False},
                "ButlerUI-develop-build":{"initbuild": None,"status" :None,"expectedBuild":None, "buildSuccess": False},
                "ButlerMD-alpha-build":{"initbuild": None,"status" :None,"expectedBuild":None, "buildSuccess": False}}
        return self.fireBuilds(joblist)
        
    def firePackaging(self,branch = "dev"):
        if branch == "dev":
            joblist = {"PackagingJob":{"initbuild": None,"status" :None,"expectedBuild":None, "buildSuccess": False}}
        elif branch == "1.5":
            joblist = {"PackagingJob-1.5":{"initbuild": None,"status" :None,"expectedBuild":None, "buildSuccess": False}}
        return self.fireBuilds(joblist)
        
    def fireBuilds(self,jobList):
        J = self.jenkinObj
        allBuild = True
        for jobname in jobList.keys():
            job = J[jobname]
            init = job.get_last_good_buildnumber()
            jobList[jobname]["initbuild"] = init
            trace.info("Current version for '%s' : %d"%(jobname,init), also_console=True)
            futureVer = job.get_next_build_number()
            jobList[jobname]["expectedBuild"] = futureVer
            trace.info("Building '%s'  next version should be : %d"%(jobname,futureVer), also_console=True)
            J.build_job(jobname)
            if job.is_queued():
                jobList[jobname]["status"] = "queued"
            elif job.is_running():
                jobList[jobname]["status"] = "running"
        timeOut = 1800      
        newJobList = jobList.keys()  
        trace.info(str(jobList),also_console=True)
        while timeOut >0:
            timeOut -= 30   
            time.sleep(30)
            
            for jobname in newJobList:
                job = J[jobname]
                if not jobList[jobname]["buildSuccess"]:
                    if job.is_queued():
                        jobList[jobname]["status"] = "queued"
                    elif job.is_running():
                        jobList[jobname]["status"] = "running"
                    else:
                        if job.get_last_good_buildnumber() == jobList[jobname]["expectedBuild"]:
                            lastBuild = job.get_last_build()
                            if lastBuild.get_status() in ["SUCCESS","FAILURE"]:
                                jobList[jobname]["buildSuccess"] = lastBuild.get_status()
                                jobList[jobname]["status"] = "completed"
                                newJobList.remove(jobname)
                                
            for jobname in jobList.keys():
                if jobList[jobname]["buildSuccess"] == "SUCCESS": 
                    allBuild = True
                else:
                    allBuild = False
                    break
                        
            trace.info(str(jobList),also_console=True)
            if allBuild:
                trace.info("All jobs  build successfully : %s"%str(jobList),also_console=True)
                return True       
            trace.info("All jobs are not build successfully , status check after 30 seconds: %s"%str(jobList),also_console=True)
    
        errorMsg = "Error: Build Failure not proceeding further. \n Details:\n %s" % str(jobList)
        try:
            BuiltIn().fail(errorMsg)
            return False
        except:
            print(errorMsg)
            return False
            sys.exit()
            
    def getlatestOtherBuild(self,branch = "dev"):
        if branch == "dev":
            joblist = {"butler_core" : "ButlerServer-develop-Build-commit-id",
                       "butler_interface" : "ButlerInterface-develop-Build-commit-id",
                       "butler_remote" : "ButlerRemote-develop-Build-commit-id",
                       "butler_ui" : "ButlerUI-develop-Build-commit-id",
                       "butler_md_alpha" : "ButlerMD-alpha-Build-commit-id"}
        elif branch == "1.5":
            joblist = {"butler_core" : "ButlerServer-1.5-Build-commit-id",
                       "butler_interface" : "ButlerInterface-1.5-Build-commit-id",
                       "butler_remote" : "ButlerRemote-1.5-Build-commit-id",
                       "butler_ui" : "ButlerUI-develop-Build-commit-id",
                       "butler_md_alpha" : "ButlerMD-alpha-Build-commit-id"}
        jobDict = {}
        J = self.jenkinObj
        for eachJob in joblist.keys():
            job = J[joblist[eachJob]]
            lgb = job.get_last_good_build()
            jobDict[eachJob] = lgb.get_artifact_dict().keys()[0]
        return jobDict
        
    def getlatestPackagesBuild(self,freshBuild = False,branch = "dev" ):
        J = self.jenkinObj
        if branch == "dev":
            job = J["PackagingJob"]
        elif branch == "1.5":
            job = J["PackagingJob-1.5"]
        if freshBuild:
            if self.fireOthers(branch):
                if self.firePackaging(branch):
                    time.sleep(30)
                    lgb = job.get_last_good_build()
        else:
            lgb = job.get_last_good_build()
        buildUrl = lgb.baseurl+"/artifact/"+ lgb.get_artifact_dict().keys()[0]
        
        trace.info("Trying to download latest build %s, please wait ..."%buildUrl,also_console=True)
        try:
            tmp_path = BuiltIn().get_variable_value("${tmp_pth}")
        except:
            tmp_path = "."
        fileName = "%s/%s"%(tmp_path,os.path.basename(urlparse(buildUrl).path))
        extra_pth =BuiltIn().get_variable_value("${extra_pth}")
        outfileName = "%s/packagename"%extra_pth
        cHtmlFileName = self.generateComponentHtml(branch)
        f = open(outfileName,'w')
        f.write("%s,%s"%(fileName,cHtmlFileName))
        f.close()
        if os.path.exists(fileName):
            trace.info("Latest Build found on local, using %s"%fileName,also_console=True)
            return fileName
        r = requests.get(buildUrl, auth=(self.jenkinUN, self.jenkinPW))
        if r.status_code == 200:
            trace.info("Saving file %s"%fileName,also_console=True)
            with open(fileName, 'wb') as out:
                for bits in r.iter_content():
                    out.write(bits)
            return fileName
        else:
            trace.info("Error in downloading latest file, Error code : "+r.status_code,also_console=True)
        return False
    
    def generateComponentHtml(self,branch = "dev"):
        
        htmlStr = '''<!DOCTYPE html>
                    <html>
                    <head>
                    <style>
                    table, th, td {
                        border: 1px solid black;
                        border-collapse: collapse;
                    }
                    th, td {
                        padding: 5px;
                    }
                    </style>
                    </head>
                    <body>
                    <table style="width:100%">
                    <tr><th>Components</th><th>Debian File</th><th>Go to Component Repository</th><th>Download Link</th></tr>
                     '''
        print htmlStr
        cDict = self.getlatestOtherBuild(branch)
        for cKey in cDict.keys():
            tmpStr = '''
                    <tr>
                    <td>
                       <p>%s</p>
                    </td>
                
                    <td>
                        <p>%s</p>
                    </td>
                     
                    <td>
                       <a href="http://butler-artifacts.greyorange.com/butler/components/%s/">%s</a>
                     </td>
                     
                     <td>
                      <a href="http://butler-artifacts.greyorange.com/butler/components/%s/%s">Download</a>
                    </td>
                    
                  </tr> '''%(cKey,cDict[cKey],cKey,cKey,cKey,cDict[cKey])
            htmlStr += tmpStr
        endStr = '''</table>
                    </body>
                    </html>'''
        htmlStr += endStr
        extra_pth =BuiltIn().get_variable_value("${extra_pth}")
        outfileName = "%s/components.html"%extra_pth
        f = open(outfileName,'w')
        f.write(htmlStr)
        f.close()
        return outfileName
if __name__=="__main__":
    j = jenkinoperations()
    j.connectJenkin()
    print "connected"
    print j.getlatestOtherBuild()