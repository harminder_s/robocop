from configparser import configparser
from sendreport import sendreport
from robot.libraries.BuiltIn import BuiltIn
import os

robo = BuiltIn()

class loadconfig:

    ROBOT_LISTENER_API_VERSION = 2

    def __init__(self, confpath,filename,lib_pth,extra_pth,result_pth,tmp_pth,mail,build,upload):
        self.globalconfigDict = configparser(confpath, filename).parseConfig()
        self.lib_pth = lib_pth
        self.conf_pth = confpath
        self.extra_pth = extra_pth
        self.result_pth = result_pth
        self.tmp_pth = tmp_pth
        if build == "yes":
            self.createbuild = True
        else:
            self.createbuild = False
        if mail == "yes":
            self.send_mail = True
        else:
            self.send_mail = False
        
        if upload == "yes":
            self.upload = True
        else:
            self.upload = False
        

    def start_suite(self, name, attrs):
        self.__setvariable()

    def start_test(self, name, attrs):
        #print self.globalconfigDict
        self.__setvariable()
        
    def close(self):
        repObj = sendreport()
        if self.upload:
            repObj.uploadData(self.outPutDir, self.extra_pth)
        if self.send_mail:
            tarFile = repObj.tarOutput(self.outPutDir)
            pngFile = repObj.capturePng(self.reportFile)
            sub = "VM : %s , Build : %s"%(self.vmIP,self.buildName)
            repObj.sendMail(self.mailTo,sub,pngFile,tarFile)
            
    def __setvariable(self):
        for key in self.globalconfigDict:
            robo.set_global_variable("${%s}"%key,self.globalconfigDict[key])
        robo.set_global_variable("${lib_pth}",self.lib_pth)
        robo.set_global_variable("${conf_pth}",self.conf_pth)
        robo.set_global_variable("${extra_pth}",self.extra_pth)
        robo.set_global_variable("${result_pth}",self.result_pth)
        robo.set_global_variable("${tmp_pth}",self.tmp_pth)
        robo.set_global_variable("${createBuild}",self.createbuild)
        self.vmIP = BuiltIn().get_variable_value("${vm_ip}","${smokeapi.host}")
        self.buildName = os.path.basename(BuiltIn().get_variable_value("${buildName}" , "${smokeapi.buildPath}"))
        self.outPutDir= BuiltIn().get_variable_value("${OUTPUT DIR}")
        self.reportFile = BuiltIn().get_variable_value("${REPORT FILE}")
        if self.send_mail:
            self.mailTo = BuiltIn().get_variable_value("${reporting.toaddr}")