from dashboard import dashboard
from apiHelper import apiHelper


class order:
    fetch_order_id = ""

    def __init__(self, serverIP, username="admin", password="apj0702"):
        self.username = username
        self.password = password
        self.api = apiHelper()

    def orderTabEvents(self, serverIP, username, password, execution):
        self.dash_obj = dashboard(serverIP, username="admin", password="apj0702", execution="remote")
        self.dash_obj.openBrowser(serverIP, username, password, execution)
        self.dash_obj.loginDashboard()
        self.dash_obj.ui.clickButton("tabs.orderstab")

    def closeOrderEvent(self):
        self.dash_obj.closeBrowser()

    def verifyOrderListSubTab(self, serverIP, username, password, execution):
        self.orderTabEvents(serverIP, username, password, execution)
        self.dash_obj.ui.clickButton("header.orderlistlink")
        self.dash_obj.ui.forceWait(3)
        assert self.dash_obj.ui.awaitForElementToBeVisible("header.orderrefreshbtn", 30), "Filter Button is not present"
        expectedTableHeaders = ["ORDER ID", "PICK BY", "RECIEVED TIME", "COMPLETED", "ORDER LINE"]
        actualTableHeaders = self.dash_obj.ui.getMultipleElementsText("header.systemsubheaders")
        assert actualTableHeaders == expectedTableHeaders, "Order List Table not formed correctly."

    def putBackFrontOperations(self, serverIP, username, password, barcodes, qty, pps_put_id):
        self.api.put_back_operations(serverIP, username, password, barcodes, qty, pps_put_id)
        self.api.put_front_operations(serverIP, username, password, pps_put_id)

    def pickFrontBackOperations(self, serverIP, username, password, pdfaValues, pps_pick_id):
        a, b = self.api.add_single_order(serverIP, username, password, pdfaValues)
        self.fetch_order_id = a
        self.api.pick_front_process(serverIP, username, password, pps_pick_id)

    def verifyCompletedOrder(self, serverIP, username, password, execution, barcodes, qty, pps_put_id, pps_pick_id,
                             pdfaValues):
        self.putBackFrontOperations(serverIP, username, password, barcodes, qty, pps_put_id)
        self.pickFrontBackOperations(serverIP, username, password, pdfaValues, pps_pick_id)
        self.orderTabEvents(serverIP, username, password, execution)
        self.dash_obj.ui.clickButton("header.orderlistlink")
        self.dash_obj.ui.clickButton("header.orderrefreshbtn")
        self.dash_obj.ui.forceWait(2)
        assert self.dash_obj.ui.awaitForElementToBeVisible("header.orderrefreshbtn", 30), "Filter Button is not present"
        actual_orderId = self.dash_obj.ui.getFirstFromMultipleEleText("header.orderidtext")
        expected_orderId = self.fetch_order_id
        assert actual_orderId == expected_orderId, "Order Id not matching. Wrong Order processed"
        actual_orderDetails = self.dash_obj.ui.getMultipleElementsText("header.orderidtext")
        actual_orderStatus = actual_orderDetails[1]
        expectedOrderStatus = "Completed"
        print actual_orderStatus, expectedOrderStatus
        assert actual_orderStatus == expectedOrderStatus, "Order Status is not Completed"
