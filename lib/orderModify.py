import sshbasic 
from butlerserver import *
import json
from coreApi import coreApi
from apiHelper import apiHelper
from robot.libraries.BuiltIn import BuiltIn
import os
import random
import re
class orderModify:
    def delete_order_by_id(self,host,username,password,order_id):
        if not hasattr(self, "order_modify_obj"):
            self.order_modify_obj = coreApi(host,username,password)  
        api_url = '/api/orders/delete/%s'%(order_id)
        myjson='''{"type" : "full"}'''
        myjson = myjson.encode()   
        (status, response_json) = self.order_modify_obj.executePutApi(api_url,myjson) 
        trace.debug(status)
        trace.debug(response_json)
        if (int(status) != 200):
            errorMsg = "Unable to place request for order cancellation"
            BuiltIn().fail(errorMsg)
        else:
            trace.info("Order with Order id : %s request for cancelation"%(order_id),also_console=True)
        return response_json
            
    def update_orderline_quantity(self,host,username,password,order_id,orderline_id,qty):
        if not hasattr(self, "order_modify_obj"):
            self.order_modify_obj = coreApi(host,username,password)
        api_url = '/api/orders/%s/%s'%(order_id,orderline_id)    
        myjson = '''{"qty": %s}'''%(qty) 
        (status, response_json) = self.order_modify_obj.executePutApi(api_url,myjson)   
        trace.info("Status: %s"%(str(status)), also_console=True)
        trace.info("order deletion: %s"%(str(response_json)), also_console=True)
        if (int(status) != 200):
            errorMsg = "Unable to place request to update order quantity"
            BuiltIn().fail(errorMsg)
        else:
            trace.info("Order with Order id : %s request for update quantity"%(order_id),also_console=True)
        return response_json
        
    def add_orderline(self,host,username,password,order_id,pdfavalues,qty):
        if not hasattr(self, "order_modify_obj"):
            self.order_modify_obj = coreApi(host,username,password)
        api_url = '/api/orders/add_orderline/%s'%(order_id) 
        filter_parameters = "" 
        pdfa_fields =pdfavalues.split(",")
        for pdfa_field in pdfa_fields:
            pdfa_key, pdfa_value = (pdfa_field.split(":")[0].strip("\""), pdfa_field.split(":")[1].strip().strip("\""))
            filter_parameters = filter_parameters + '''"%s in ['%s']",'''%(pdfa_key, pdfa_value)   
        filter_parameters = filter_parameters.strip(",") 
        random_value = random.randint(0,1000000) 
        orderline_id = "new_Orderline-%d"%(random_value)
        myjson = '''{
                        "orderline": {
                            "orderline_id": "%s",
                            "qty": %d,
                            "filter_parameters": [%s],
                            "preference_params": [        
                                {
                                     "param_name": "order by",
                                     "param_value": "product_weight",
                                     "extra_key": "ASC"
                                }
                                    ]
                            }
                    }'''%(orderline_id,qty,filter_parameters) 
        myjson = myjson.encode()
        (status, response_json) = self.order_modify_obj.executePutApi(api_url,myjson)   
        trace.info("Status: %s"%(str(status)), also_console=True)
        trace.info("order deletion: %s"%(str(response_json)), also_console=True)   
        if (int(status) != 200):
            errorMsg = "Unable to place request for orderline addition"
            BuiltIn().fail(errorMsg)
        else:
            trace.info("Order with Order id : %s request for orderline addition"%(order_id),also_console=True) 
        return response_json
    
    def set_config_parameter(self,host,ssh_username,password,config_paramter,config_parameter_value):
        targetServer = sshbasic.sshbasic(host,un=ssh_username,pw=password)  
        fileList =  targetServer.ssh.list_directory("/opt/butler_server")
        for eFileName in fileList:
            if eFileName.find("erts-") != -1:
                eFile = eFileName
        escriptName = "setting_config_parameter.escript"
        localEscriptPth = "%s/%s"%(BuiltIn().get_variable_value("${tmp_pth}","../tmp"),escriptName)                             
        remoteEsciptPth = "/tmp/%s"%escriptName
        if not targetServer.fileExist(remoteEsciptPth):
            trace.info("File doesn't exist, creating and uploading",also_console=True)
            escript = '''#!/opt/butler_server/%s/bin/escript   
%%%%! -sname butler -setcookie butler_server

    main(Args) ->
        Node = 'butler_server@localhost',
        Mod = 'application',
        Fun = set_env,
        [A, B, C] = Args,
        io:format("----> ~p, ~p, ~p", [A, B, C]),
        R = rpc:call(Node, Mod, Fun, [butler_server,list_to_atom(B),erlang:list_to_atom(C)]),
        io:format("application ~p~n",[R]).'''%eFile
            fp = open(localEscriptPth,"w")
            fp.write(escript)
            fp.close()
            targetServer.copyToRemote(localEscriptPth,remoteEsciptPth,"0755")
        targetServer.executeCli('/tmp/setting_config_parameter.escript butler_server %s %s' %(config_paramter,config_parameter_value))
     
    def get_config_parameter(self,host,ssh_username,password,config_parameter):
        targetServer = sshbasic.sshbasic(host,un=ssh_username,pw=password)  
        fileList =  targetServer.ssh.list_directory("/opt/butler_server")
        for eFileName in fileList:
            if eFileName.find("erts-") != -1:
                eFile = eFileName
        escriptName = "getting_config_parameter_value.escript"
        localEscriptPth = "%s/%s"%(BuiltIn().get_variable_value("${tmp_pth}","../tmp"),escriptName)                             
        remoteEsciptPth = "/tmp/%s"%escriptName
        if not targetServer.fileExist(remoteEsciptPth):
            trace.info("File doesn't exist, creating and uploading",also_console=True)
            escript = '''#!/opt/butler_server/%s/bin/escript   
%%%%! -sname butler -setcookie butler_server

    main(Args) ->
        Node = 'butler_server@localhost',
        Mod = 'application',
        Fun = get_env,
        [A,B] = Args,
        R = rpc:call(Node, Mod, Fun, [butler_server,list_to_atom(B)]),
        io:format("application ~p~n",[R]).'''%eFile
            fp = open(localEscriptPth,"w")
            fp.write(escript)
            fp.close()
            targetServer.copyToRemote(localEscriptPth,remoteEsciptPth,"0755")
        targetServer.executeCli('/tmp/getting_config_parameter_value.escript butler_server %s' %(config_parameter))
         
                        
            
    def fetch_request_id(self,host,ssh_username,password,response):
        requestID=response.get('request_id', None)
        trace.info("request id : %s"%requestID, also_console=True)
        return requestID
        
    def get_rack_barcode(self,host,ssh_username,password,rack_id):
        targetServer = sshbasic.sshbasic(host,un=ssh_username,pw=password)  
        fileList =  targetServer.ssh.list_directory("/opt/butler_server")
        for eFileName in fileList:
            if eFileName.find("erts-") != -1:
                eFile = eFileName
        escriptName = "get_rack_barcode.escript"
#         localEscriptPth = "%s/%s"%(BuiltIn().get_variable_value("${tmp_pth}","../tmp"),escriptName)                             
        localEscriptPth = "/tmp/%s"%escriptName
        remoteEsciptPth = "/tmp/%s"%escriptName
        if not targetServer.fileExist(remoteEsciptPth):
            trace.info("File doesn't exist, creating and uploading",also_console=True)
            escript = '''#!/opt/butler_server/%s/bin/escript   
%%%%! -sname butler -setcookie butler_server

    main(Args) ->
        Node = 'butler_server@localhost',
        Mod = 'rackinfo',
        Fun = get_column_by_id,
        [A,B] = Args,
        Rackid = erlang:list_to_binary(A),
        R = rpc:call(Node, Mod, Fun, [Rackid,position]),
       io:format("rack_info ~p~n",[R]).'''%eFile
            fp = open(localEscriptPth,"w")
            fp.write(escript)
            fp.close()
            targetServer.copyToRemote(localEscriptPth,remoteEsciptPth,"0755")
        targetServer.executeCli('/tmp/get_rack_barcode.escript %s position' %(rack_id))
#    
    def get_pps_barcode(self,host,ssh_username,password,pps_id):
        targetServer = sshbasic.sshbasic(host,un=ssh_username,pw=password)  
        fileList =  targetServer.ssh.list_directory("/opt/butler_server")
        for eFileName in fileList:
            if eFileName.find("erts-") != -1:
                eFile = eFileName
        escriptName = "get_pps_barcode.escript"
#         localEscriptPth = "%s/%s"%(BuiltIn().get_variable_value("${tmp_pth}","../tmp"),escriptName) 
        localEscriptPth = "/tmp/%s"%escriptName                            
        remoteEsciptPth = "/tmp/%s"%escriptName
        if not targetServer.fileExist(remoteEsciptPth):
            trace.info("File doesn't exist, creating and uploading",also_console=True)
            escript = '''#!/opt/butler_server/%s/bin/escript   
%%%%! -sname butler -setcookie butler_server

    main(Args) ->
        Node = 'butler_server@localhost',
        Mod = 'ppsinfo',
        Fun = get_column_by_id,
        [A,B] = Args,
        R = rpc:call(Node, Mod, Fun, [list_to_atom(pps_id),location]),
       io:format("pps_info ~p~n",[R]).'''%eFile
            fp = open(localEscriptPth,"w")
            fp.write(escript)
            fp.close()
            targetServer.copyToRemote(localEscriptPth,remoteEsciptPth,"0755")
        targetServer.executeCli('/tmp/get_pps_barcode.escript %s position' %(pps_id))
        
    def get_PpsOrders(self,host,ssh_username,password,pps_id):
        targetServer = sshbasic.sshbasic(host,un=ssh_username,pw=password)  
        fileList =  targetServer.ssh.list_directory("/opt/butler_server")
        for eFileName in fileList:
            if eFileName.find("erts-") != -1:
                eFile = eFileName
        escriptName = "get_PpsOrders.escript"
        ##localEscriptPth = "%s/%s"%(BuiltIn().get_variable_value("${tmp_pth}","../tmp"),escriptName)                             
        localEscriptPth = "/tmp/%s"%escriptName
        remoteEsciptPth = "/tmp/%s"%escriptName
        print "Connected to Node"
        if not targetServer.fileExist(remoteEsciptPth):
            trace.info("File doesn't exist, creating and uploading",also_console=True)
            escript = '''#!/opt/butler_server/%s/bin/escript   
%%%%! -sname butler -setcookie butler_server
    main(Args) ->
        Node = 'butler_server@localhost',
        [PpsId] = Args,
        OrdersList =
          case rpc:call(Node, pps_manager, get_current_taskid, [erlang:list_to_integer(PpsId)]) of
              {picktask, TaskId} ->
                  element(2, rpc:call(Node, ppstaskrec, get_all_orders_attached_to_ppstask, [TaskId]));
              _OtherTask ->
                  []          
          end,
       io:format("orders_list ~p~n",[OrdersList]).'''%eFile
            fp = open(localEscriptPth,"w")
            fp.write(escript)
            fp.close()
            targetServer.copyToRemote(localEscriptPth,remoteEsciptPth,"0755")
        order_list=targetServer.executeCli('/tmp/get_PpsOrders.escript %s' %(pps_id))
        return order_list
        
          
    def PPS_hasOrder(self,host,ssh_username,password,pps_id,order_id):
        order_list=self.get_PpsOrders(host, ssh_username, password, pps_id)
        if order_id in order_list:
            return "True"
        else:
            return "False"    
            
       

    
#         
if  __name__ == "__main__":
    orderModify().add_orderline(host='192.168.8.206', username='admin', password='apj0702', order_id='Orderid-961657', pdfavalues='product_sku:2001', qty=2)