from apiHelper import apiHelper
from time import sleep
from robot.api import logger as trace
from robot.libraries.BuiltIn import BuiltIn
from config import getdata

class orderlines:
    def testOrderlines(self, host, username, password, filename, testdata_spreadsheet, product_spreadsheet, pick, put):
        obj = apiHelper()
        config_dict_testdata = getdata(filename)[testdata_spreadsheet]
        num_test_cases = len(config_dict_testdata.keys())
        pick_ppsid = pick.split(",")[0]
        
        trace.info("Starting test execution on ppsid: %s"%(pick_ppsid), also_console=True)
        for test_case in range(1, num_test_cases+1):
#         for test_case in range(1, 2):
            trace.info("Test Case %d: Starting"%(test_case),also_console=True)
            str_list_tags = str(config_dict_testdata[str(test_case)]['list_tags'])
            str_list_quantity = str(config_dict_testdata[str(test_case)]['list_quantity'])
            list_tags = str_list_tags.split(",")
            list_quantity = str_list_quantity.split(",")
            obj.add_order_multiple_orderline(host, username, password, filename, product_spreadsheet, list_tags, list_quantity)
            obj.pick_front_process(host, username, password, pick_ppsid)
            sleep(5)
            obj.pick_back_process(host, username, password, pick_ppsid)
            trace.info("Test Case %d: Completed"%(test_case),also_console=True)
    
    def postMultipleOrders(self, host, username, password, filename, testdata_spreadsheet, product_spreadsheet, item_tags=all):
        obj = apiHelper()
        config_dict_testdata = getdata(filename)[testdata_spreadsheet]
        num_test_cases = len(config_dict_testdata.keys())
        
        for test_case in range(1, num_test_cases+1):
#         for test_case in range(1, 2):
            trace.info("Test Case %d: Starting"%(test_case),also_console=True)
            str_list_tags = str(config_dict_testdata[str(test_case)]['list_tags'])
            str_list_quantity = str(config_dict_testdata[str(test_case)]['list_quantity'])
            list_tags = str_list_tags.split(",")
            list_quantity = str_list_quantity.split(",")
            obj.add_order_multiple_orderline(host, username, password, filename, product_spreadsheet, list_tags, list_quantity)
            
if __name__ == "__main__":
#     orderlines().testOrderlines(host="192.168.8.108", username="admin", password="apj0702", filename="../conf/config_orderlines.xls", testdata_spreadsheet="testData_orderline", product_spreadsheet="product_orderline", pick='3', put='2,4')
    orderlines().postMultipleOrders(host="192.168.8.70", username="admin", password="apj0702", filename="../conf/config_orderlines.xls", testdata_spreadsheet="test", product_spreadsheet="test_products", item_tags=all)
        
        
        