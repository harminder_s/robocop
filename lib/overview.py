from dashboard import dashboard
from apiHelper import apiHelper


class overview:
    actual_put_stock = {}
    actual_audit_stock = {}

    def __init__(self, serverIP, username="admin", password="apj0702"):
        self.username = username
        self.password = password
        self.api = apiHelper()

    def overviewTabEvents(self, serverIP, username, password, execution):
        self.dash_obj = dashboard(serverIP, username="admin", password="apj0702", execution="remote")
        self.dash_obj.openBrowser(serverIP, username, password, execution)
        self.dash_obj.loginDashboard()
        self.dash_obj.ui.clickButton("tabs.overviewtab")

    def closeOverviewEvent(self):
        self.dash_obj.closeBrowser()

    def putBackFrontOperations(self, serverIP, username, password, barcodes, qty, pps_put_id):
        self.api.put_back_operations(serverIP, username, password, barcodes, qty, pps_put_id)
        self.api.put_front_operations(serverIP, username, password, pps_put_id)

    def pickFrontBackOperations(self, serverIP, username, password, pdfaValues, pps_pick_id):
        self.api.add_single_order(serverIP, username, password, pdfaValues)
        self.api.pick_front_process(serverIP, username, password, pps_pick_id)

    def createStartAuditOperation(self, serverIP, username, password, audit_type, audit_value):
        self.api.start_audit(serverIP, audit_id=self.api.create_audit(serverIP, username, password, audit_type, audit_value))

    def verifyAuditOperation(self, serverIP, pps_side, pps_id, username, password, barcode):
        self.api.verify_sku_audit(serverIP, pps_side, pps_id, username, password, barcode)

    def fetchActualStock(self, serverIP, username, password, execution):
        self.dash_obj = dashboard(serverIP, username="admin", password="apj0702", execution="remote")
        self.dash_obj.openBrowser(serverIP, username, password, execution)
        self.dash_obj.loginDashboard()
        self.dash_obj.ui.clickButton("tabs.overviewtab")
        self.dash_obj.ui.forceWait(2)
        actual_put_audit_overview = self.dash_obj.ui.getMultipleElementsText("header.itemstockedauditedvalues")
        assert self.dash_obj.ui.awaitForElementToBeVisible("header.horizontalgraph", 15), "Performance Graph not present"

        ##################### Fetch Actual Put Overview Stock #####################
        if "None" in actual_put_audit_overview[0]:
            actual_put_dict = {"Put Stock": "0"}
        else:
            actual_put_dict = {"Put Stock": actual_put_audit_overview[0]}
        self.actual_put_stock = actual_put_dict
        self.dash_obj.ui.clickButton("header.horizontalgraph")
        self.dash_obj.ui.selectFromDropDown("Put")
        self.dash_obj.ui.clickButton("header.verticalgraph")
        self.dash_obj.ui.selectFromDropDown("Put")

        ##################### Fetch Actual Audit Overview Stock #####################
        if "None" in actual_put_audit_overview[1]:
            actual_audit_dict = {"Audit Stock": "0"}
        else:
            actual_audit_dict = {"Audit Stock": actual_put_audit_overview[1]}
        self.actual_audit_stock = actual_audit_dict
        self.dash_obj.ui.clickButton("header.horizontalgraph")
        self.dash_obj.ui.selectFromDropDown("Audit")
        self.dash_obj.ui.clickButton("header.verticalgraph")
        self.dash_obj.ui.selectFromDropDown("Audit")

        ##################### Fetch Actual Pick Overview Stock #####################
        self.dash_obj.ui.clickButton("header.horizontalgraph")
        self.dash_obj.ui.selectFromDropDown("Pick")
        self.dash_obj.ui.clickButton("header.verticalgraph")
        self.dash_obj.ui.selectFromDropDown("Pick")
        self.dash_obj.closeBrowser()

    def verifyOverviewPutStock(self, serverIP, username, password, execution, barcodes, qty, pps_put_id):
        self.putBackFrontOperations(serverIP, username, password, barcodes, qty, pps_put_id)
        self.overviewTabEvents(serverIP, username, password, execution)
        self.dash_obj.ui.forceWait(2)
        expected_put_audit_overview = self.dash_obj.ui.getMultipleElementsText("header.itemstockedauditedvalues")
        expected_put_dict = {"Put Stock": expected_put_audit_overview[0]}
        actual_put_dict = self.actual_put_stock
        assert actual_put_dict != expected_put_dict, "Overview Put Stock didn't get updated. Actual Put Quantity:" + str(actual_put_dict) + ".Expected Put Quantity:" + str(expected_put_dict)

    def verifyOverviewAuditStock(self, serverIP, username, password, execution, audit_type, audit_value, pps_side, pps_id, barcode):
        print serverIP, username, password
        self.createStartAuditOperation(serverIP, username, password, audit_type, audit_value)
        self.verifyAuditOperation(serverIP, pps_side, pps_id, username, password, barcode)
        self.overviewTabEvents(serverIP, username, password, execution)
        self.dash_obj.ui.forceWait(2)
        expected_put_audit_overview = self.dash_obj.ui.getMultipleElementsText("header.itemstockedauditedvalues")
        expected_audit_dict = {"Audit Stock": expected_put_audit_overview[1]}
        actual_audit_dict = self.actual_audit_stock
        assert actual_audit_dict != expected_audit_dict, "Overview Audit Stock didn't get updated. Actual Audit Quantity:" + str(actual_audit_dict) + ".Expected Audit Quantity:" + str(expected_audit_dict)
