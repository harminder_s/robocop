from vmoperations import vmoperations
from vmwareoperations import vmwareops
class ovirtvm:
    def createVM(self,host,host_user,host_pw,VM_NAME,instanceType,templateName,clusterName):
        myvm =  vmoperations(host,host_user,host_pw).createGuest(VM_NAME,instanceType,templateName,clusterName)
        return myvm

    def deleteVM(self,host,host_user,host_pw,VM_NAME):
        return vmoperations(host,host_user,host_pw).removeVM(VM_NAME) 
    
    def createVMWARE(self,host,host_user,host_pw,VM_NAME,templateName):   
        myvm = vmwareops(host,host_user,host_pw).createvm(VM_NAME, templateName)
        return myvm
     
if __name__=="__main__":
    
    host = "192.168.8.10"
    host_user = "admin@internal"
    host_pw = "apj0702"
    
    VM_NAME = "test_09May_1"
    instanceType = "Large"
    templateName = "debian8-vhost-01"
    clusterName = "vhost-01-Local"
    
    #ovirtvm().deleteVM(host,host_user,host_pw,VM_NAME)
    print ovirtvm().createVM(host, host_user, host_pw, VM_NAME, instanceType, templateName, clusterName)
