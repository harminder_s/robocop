
from butlerserver import butlerserver


class pgsqlHelper:

    def get_uid_by_pdfa(self, host, pdfa_value='{\\\"product_sku\\\": \\\"2001\\\"}'):
        if not hasattr(self, "butlerserver_obj"):
            self.butlerserver_obj = butlerserver()
        query_uid = "select product_uid as uid from products where pdfa_values ='%s'" %(pdfa_value)
        print query_uid
        uid = self.butlerserver_obj.getpgsqlOutput(host, query=query_uid)
        return uid[0]['uid']

    def get_product_quantity_by_location(self, host, uid):
        if not hasattr(self, "butlerserver_obj"):
            self.butlerserver_obj = butlerserver()
        uid = self.get_uid_by_pdfa(host)
        query = "select sum(actual_quantity), location from inventories where uid = '%s' and deleted = 'f' " \
                "group by location" %uid
        list_qty_location = self.butlerserver_obj.getpgsqlOutput(host, query=query)
        return list_qty_location

    def getSlotBarcodeByStorageNode(self, host, storage_node="%031.1.A.01-02%"):
        if not hasattr(self, "butlerserver_obj"):
            self.butlerserver_obj = butlerserver()
        query = "select resources_barcode from resources where resources_type = 'rack' and " \
                "cast(resources_data as varchar) like '%s'" %storage_node
        slot_barcodes = self.butlerserver_obj.getpgsqlOutput(host, query=query)
        list_slot_barcodes = []
        for each in slot_barcodes:
            list_slot_barcodes.append(each['resources_barcode'])
        return list_slot_barcodes
    
    def get_Order_Status(self,host,order_id):
        if not hasattr(self, "butlerserver_obj"):
            self.butlerserver_obj = butlerserver()
        query = "select warehouse_status AS Order_status from orders where order_id='%s';" % order_id
        order_status=self.butlerserver_obj.getpgsqlOutput(host, query=query)
        return order_status
    
    def get_Orderline_Quantity(self,host,order_id,orderline_id):
        if not hasattr(self, "butlerserver_obj"):
            self.butlerserver_obj = butlerserver()
        query = "select qty from orderlines where order_id='%s' and orderline_id='%s';" %(order_id,orderline_id)
        quantity=self.butlerserver_obj.getpgsqlOutput(host, query=query)
        return quantity
    
    def get_Orderline_Status(self,host,order_id,orderline_id):
        if not hasattr(self, "butlerserver_obj"):
            self.butlerserver_obj = butlerserver()
        query = "select orderline_status from orderlines where order_id='%s' and orderline_id='%s';"\
                %(order_id,orderline_id)
        orderlineStatus=self.butlerserver_obj.getpgsqlOutput(host, query=query)
        return orderlineStatus
     
    def get_Modification_Request_Status(self,host,request_id):
        if not hasattr(self, "butlerserver_obj"):
            self.butlerserver_obj = butlerserver()
        query = "select status AS modification_request_status from order_modification where request_id='%s';" \
                %(request_id)
        request_status=self.butlerserver_obj.getpgsqlOutput(host, query=query)
        return request_status   

    def get_product_and_qty_by_location(self,host,location):
        if not hasattr(self, "butlerserver_obj"):
            self.butlerserver_obj = butlerserver()
        # uid = self.get_uid_by_pdfa(host)
        query = "select sum(i.actual_quantity) as qty,p.extra_fields->'product_barcodes' as barcode from inventories i," \
                " products p where p.product_uid = i.uid  and i.location like '%s' " \
                "group by i.uid, i.location, p.extra_fields" % (location + '%')
        list_qty_barcode_by_location = self.butlerserver_obj.getpgsqlOutput(host, query=query)
        return list_qty_barcode_by_location

    def get_product_qty_by_sku(self,host,sku='2001'):
        if not hasattr(self, "butlerserver_obj"):
            self.butlerserver_obj = butlerserver()
        query = "select location,sum(actual_quantity) from inventories where uid in (select product_uid from products " \
                "where pdfa_values->>'product_sku' = '%s') group by location" % sku
        list_qty_per_sku_location = self.butlerserver_obj.getpgsqlOutput(host, query=query)
        return list_qty_per_sku_location

    def get_location_barcode_by_sku(self,host,sku='2001'):
        if not hasattr(self, "butlerserver_obj"):
            self.butlerserver_obj = butlerserver()
        query = "select sum(i.actual_quantity),i.location,p.extra_fields->'product_barcodes' as product_barcodes," \
                "p.pdfa_values->>'product_sku' as sku from inventories i, products p where p.product_uid = i.uid " \
                "and p.pdfa_values->>'product_sku'= '%s' group by i.location, p.extra_fields, p.pdfa_values;" % sku
        list_location_barcode_by_sku = self.butlerserver_obj.getpgsqlOutput(host, query=query)
        return list_location_barcode_by_sku

    def get_sku_at_location(self,host,location='031.1.A.01'):
        if not hasattr(self, "butlerserver_obj"):
            self.butlerserver_obj = butlerserver()
        query = "select distinct p.pdfa_values->'product_sku' as sku , i.location from inventories i, products p " \
                "where i.uid=p.product_uid and i.location like'%s'" % (location + '%')
        list_sku_location = self.butlerserver_obj.getpgsqlOutput(host, query=query)
        return list_sku_location

    def get_product_by_sku_location(self,host="192.168.8.206",sku="2001",location="031.1.A.01-02"):
        if not hasattr(self, "butlerserver_obj"):
            self.butlerserver_obj = butlerserver()
        query = "select sum(i.actual_quantity),i.location,p.extra_fields->'product_barcodes' as product_barcodes," \
                "p.pdfa_values->>'product_sku' as sku from inventories i, products p where p.product_uid = i.uid and " \
                "p.pdfa_values->>'product_sku'= '%s' and i.location = '%s' " \
                "group by i.location, p.extra_fields, p.pdfa_values;" % (sku, location)
        list_count_product_on_location = self.butlerserver_obj.getpgsqlOutput(host, query=query)
        return list_count_product_on_location
    
    def get_orderlines_of_order(self,host,order_id):
        if not hasattr(self, "butlerserver_obj"):
            self.butlerserver_obj = butlerserver()
        query = "select orderline_id from orderlines where order_id='%s';" % order_id
        orderline_ids=self.butlerserver_obj.getpgsqlOutput(host, query=query)
        return orderline_ids

if  __name__ == "__main__":
    # print pgsqlHelper().get_product_and_qty_by_location("192.168.8.206","%027.1.A.01-02%")
    # print pgsqlHelper().getSlotBarcodeByStorageNode("192.168.8.206",storage_node="%027.1.A.01-02%")[0]
    # print pgsqlHelper().get_location_barcode_by_sku(host="192.168.8.206",sku='product1')
    # print pgsqlHelper().get_sku_at_location("192.168.8.206")
#     print pgsqlHelper().get_product_by_sku_location(host="192.168.8.206", sku="product1", location="031.1.A.01-02")
    print pgsqlHelper().get_orderlines_of_order("192.168.8.212", order_id='OD4-934884')


    # print pgsqlHelper().get_product_and_qty_by_location("192.168.8.206","031.1.A.01")
#     # pgsqlHelper().get_uid_by_pdfa("192.168.8.157")
#     pgsqlHelper().get_product_quantity_by_location("192.168.8.206", "2f90b276-3862-4bfc-95d1-13e18ce08f96")
#     print pgsqlHelper().get_Order_Status(host="192.168.8.63",order_id='ODDD_01')



