
import requests
import json
from xlrd import open_workbook
import os
from coreApi import coreApi


class postReceiveKeys:

    def gen_auth_token(self, request_port='443', host='192.168.8.129', username='admin', password='apj0702'):
        url = 'https://%s:%s/api/auth/token' % (host, request_port)
        payload = {'username':username, 'password': password}
        headers = {'Accept': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded'}
        a = requests.post(url, data=payload, headers=headers, verify=False)
        auth_token = a.json()['auth_token']

        return auth_token

    def post_rk(self, request_port='443', interface_ip='192.168.8.129', receive_qty=2):
        url = 'https://%s:%s/api/receive_keys' % (interface_ip, request_port)

        filename = os.path.abspath(os.path.join(os.getcwd(), '..')) + '/conf/product_sku.xlsx'

        wb = open_workbook(filename)
        values = wb.sheet_by_index(0).col_values(0, 1)

        for rk in range(51, 52):
            f = ('{"receive_keys": [{"receive_key": "Perf%s","status": 3,"header_fields": [{"receive_batch": '
                 '{"Format": "String","Mandatory": "Y","Validation": "none"}}],"rk_lines": []}]}') % rk
            data = json.loads(f)
            values_index = 0
            for i in range(0, 2):
                values_index += 1
                if values_index > len(values) - 1:
                    values_index = 0
                i = str(i)
                rk_line = json.loads(
                    '{"line_seq":"line_0%s","sku":"%s","receive_qty":%d}' % ((str(i)), values[values_index], receive_qty))
                data["receive_keys"][0]["rk_lines"].append(rk_line)

            data = json.dumps(data)
            payload = data
            headers = {'Content-Type': 'application/json', 'Accept': 'application/json', 'Authentication-Token': self.gen_auth_token()}
            a = requests.post(url, data=payload, headers=headers, verify=False)
            print a.content, a.status_code
            return a.content, a.status_code


if __name__ == "__main__":
    # print postReceiveKeys().gen_auth_token()
    postReceiveKeys().post_rk()

