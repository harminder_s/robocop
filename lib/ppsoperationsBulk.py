import time, sys
from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from apiHelper import apiHelper


class ppsoperations:
    
    def ppsputback(self,barcode, ip,pps ,user,passwd,execType):
        putBackObj = launchpps(ip,execType)
        putBackObj.enterCredentials(pps, user, passwd,"back")
        out = putBackObj.putBack(barcode)
        putBackObj.driver.close() 
        return out
        
    def ppsputfront(self,ip,pps,adminUser,adminPasswd,execType):
        ppsUsername = apiHelper().createUsers(ip, adminUser, adminPasswd, pps, "front")
        putFrontObj = launchpps(ip,execType)
        #putFrontObj = launchpps(ip)
        ppsPasswd = adminPasswd
        putFrontObj.enterCredentials(pps, adminUser,adminPasswd,"front", ppsUsername, ppsPasswd)
        out = putFrontObj.putFront()
        putFrontObj.driver.close() 
        return out
              
    def ppspickfront(self,barcode, ip,pps ,user,passwd):
        pickFrontObj = launchpps(ip)
        pickFrontObj.enterCredentials(pps, user, passwd,"front")
        out = pickFrontObj.pickFront(barcode)
        pickFrontObj.driver.close()
        return out
        
    def ppspickback(self, ip,pps ,user,passwd):
        pickBackObj = launchpps(ip)
        pickBackObj.enterCredentials(pps, user, passwd,"back")
        out=    pickBackObj.pickBack()
        pickBackObj.driver.close() 
        return out
        
    def ppspickfrontlogin(self, ip,pps ,user,passwd):
        self.pickFrontObj = launchpps(ip)
        self.pickFrontObj.enterCredentials(pps, user, passwd,"front")
        return self.pickFrontObj
        
    def ppspickfrontlogout(self,obj):
        obj.driver.close()
    
class launchpps:
    def __init__(self,serverIP,execution = "remote"):  
        if execution == "local":
            self.driver = webdriver.Chrome()
        else:
            try:
                self.driver = webdriver.Remote("http://192.168.8.80:4444/wd/hub", webdriver.DesiredCapabilities.CHROME.copy())
            except:
                print "Remote hub has some problem launching local browser"
                self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(30)
        self.base_url = "https://%s/"%serverIP
        self.verificationErrors = []
        self.accept_next_alert = True
        self.host = serverIP
        self.apiHelperObj = apiHelper()
        self.timeout = 10
     
    def kbenter(self,text):
        driver = self.driver
        tBox = driver.find_element_by_class_name("ui-keyboard-preview")
        time.sleep(0.2)
        tBox.send_keys(text)
        time.sleep(0.1)
        driver.find_element_by_class_name("ui-keyboard-accept").click()
        
    
    def enterCredentials(self,pps, adminUser,adminPasswd,side, ppsUsername=False, ppsPasswd=False):
        if side == "back":
            ppsname = "PPS back %s"%pps
        if side == "front":
            ppsname = "PPS front %s"%pps
        self.ppsid = pps
        self.side =side
        self.username = adminUser
        self.password = adminPasswd
        if ppsUsername:
            self.ppsUser = ppsUsername
        else:
            self.ppsUser = adminUser
        if ppsPasswd:
            self.ppsPasswd = ppsPasswd
        else:
            self.ppsPasswd = adminPasswd
        driver = self.driver
        driver.set_window_size(1200, 600)
        driver.get(self.base_url + "/ui/")
        Select(driver.find_element_by_css_selector("select.selectPPS")).select_by_visible_text(ppsname)
        time.sleep(0.2)
        driver.find_element_by_id("username").click()
        self.kbenter(self.ppsUser)
        time.sleep(0.2)
        driver.find_element_by_id("password").click()
        self.kbenter(self.ppsPasswd)
        time.sleep(0.2)
        driver.find_element_by_id("loginBtn").click()     
        
    def addToDict(self,d,k,v):
        if d.has_key(k):
            val = d[k]
            val.append(v)
            d[k]= val
        else:
            d[k]=[v]
     
    def dict_diff(self, dict_a, dict_b):
        return dict([
            (key, dict_b.get(key, dict_a.get(key)))
            for key in set(dict_a.keys()+dict_b.keys())
            if (
                (key in dict_a and (not key in dict_b or dict_a[key] != dict_b[key])) or
                (key in dict_b and (not key in dict_a or dict_a[key] != dict_b[key])))])   
    
    def waitForElementLoad(self,element, find = "id"):
        try:
            if find == "id":
                element_present = EC.presence_of_element_located((By.ID, element))
                WebDriverWait(self.driver, self.timeout).until(element_present)
                return self.driver.find_element_by_id(element)
            elif find == "class":
                element_present = EC.presence_of_element_located((By.CLASS_NAME, element))
                WebDriverWait(self.driver, self.timeout).until(element_present)
                return self.driver.find_element_by_class_name(element)
            elif find == "css":
                element_present = EC.presence_of_element_located((By.CSS_SELECTOR, element))
                WebDriverWait(self.driver, self.timeout).until(element_present)
                return self.driver.find_element_by_css_selector(element)
            elif find == "xpath":
                element_present = EC.presence_of_element_located((By.XPATH, element))
                WebDriverWait(self.driver, self.timeout).until(element_present)
                return self.driver.find_element_by_xpath(element)
            elif find == "link":
                element_present = EC.presence_of_element_located((By.LINK_TEXT, element))
                WebDriverWait(self.driver, self.timeout).until(element_present)
                return self.driver.find_element_by_link_text(element)
            else:
                print "Unknown Elements"
        except TimeoutException:
            print "Timed out waiting for Element to load"
            return False 
        
    def putBack(self,barcodeList): 
        totalTime = 0
        totalItem = 0  
        for barcode,count in barcodeList:
            while len(self.apiHelperObj.getEmptyPpsBin(self.host, self.username, self.password, self.ppsid, self.side)) == 0:
                print "No empty bin, retrying after 10 seconds"
                time.sleep(10)
                if len(self.apiHelperObj.getEmptyPpsBin(self.host, self.username, self.password, self.ppsid, self.side)) > 0:
                    break
            ii = time.time()
            if self.waitForElementLoad("action","class").text == "Stage Bin or Scan Entity":
                time.sleep(0.1)
                kb = self.waitForElementLoad("div.keyboard-actions > img", "css")
                
                kb.click()
                time.sleep(0.1)
                try:
                    self.kbenter(barcode) 
                except:
                    self.apiHelperObj.processBarcode(self.host, self.username, self.password, barcode, self.ppsid, self.side)
                while 1:
                    try:
                        obj = self.waitForElementLoad("action","class")
                        if obj.text =="Place Entity in Bin and Press PPTL":
                                break
                    except:
                        pass
                
                newAddition = self.apiHelperObj.getSelectedPpsBin(self.host, self.username, self.password, self.ppsid, self.side)
                pptl =newAddition[0]
                print "%s is Placed at bin location: %s"%(barcode,pptl)
                time.sleep(1)
                
                if count > 1:
                    kq = self.waitForElementLoad("keyboard","id")
                    kq.click()
                    print barcode,count
                    self.waitForElementLoad("ui-keyboard-preview","class").send_keys(count)
                    self.waitForElementLoad("ui-keyboard-accept","class").click()
                pptlObj = self.waitForElementLoad("div.pptl.selected","css") 
                pptlObj.click()
                time.sleep(2)
                pptlObj = self.waitForElementLoad("div.bin.use > div.pptl","css") 
                pptlObj.click()
                time.sleep(1)
                self.waitForElementLoad("Stage","link").click()
                time.sleep(2)
                errorDict = self.apiHelperObj.getErrorPpsBin(self.host, self.username, self.password, self.ppsid, self.side)
                print errorDict
                if len(errorDict.keys()) >0:
                    print "Items can not be put %s"%str(errorDict.values()[0])
                    self.apiHelperObj.clearErrorPpsBin(self.host, self.username, self.password, self.ppsid, self.side, errorDict.keys()[0])
            
                else:
                    print "%s staged"%barcode
            timeTaken = time.time()-ii
            totalTime += timeTaken
            totalItem += 1 
            print barcode,timeTaken,totalTime,totalItem
    
    def putFront(self):
        itemDict = {}
        
        while True:
            timeOut = 600
            while timeOut >0:
                initSelected = self.apiHelperObj.getSelectedPpsBin(self.host, self.username, self.password, self.ppsid, self.side)
                if len(initSelected) > 0:
                    break
                else:
                    print initSelected
                    time.sleep(1)
                    timeOut -= 1
            if len(initSelected) == 0:
                print "No Items in Bin, exiting"
                return False
            while True:
                if self.waitForElementLoad("action","class"):
                    break
            timeOut =300
            while timeOut >0:
                action = self.waitForElementLoad("action","class").text
                print "Current status is : %s"%action
                if action == "Wait for MSU":
                    print "Sleeping for 1 seconds"
                    timeOut-=1
                    time.sleep(1)
                else:
                    break
                   
            kqSkip = True
            print "Item to be picked, placed at bin location: %s"%initSelected[0]
            barcode = self.apiHelperObj.getProductBarcodeToScan(self.host, self.username, self.password, self.ppsid, self.side)
            self.waitForElementLoad("div.keyboard-actions > img","css").click()
            self.kbenter(barcode) 
            while 1:
                try:
                    action = self.waitForElementLoad("action","class").text
                    print "Current status is : %s"%action
                    if action == "Place Entity in Slot and Scan More":
                        break
                    elif action =="Scan Slot to Confirm":
                        kqSkip = False
                        break
                except:
                    pass
            kqCount = 1
            if kqSkip:
                kqCount= self.apiHelperObj.getKQ(self.host, self.username, self.password, self.ppsid, self.side)
                if kqCount > 1:
                    self.waitForElementLoad("keyboard","id").click()
                    kq= self.waitForElementLoad("ui-keyboard-preview","class")
                    kq.send_keys(kqCount)
                    self.waitForElementLoad("ui-keyboard-accept","class").click()
                    while 1:
                        action = self.waitForElementLoad("action","class").text
                        print "Current status is : %s"%action
                        if action =="Scan Slot to Confirm":
                            break
            slotList = self.apiHelperObj.getRackSlotBarcode(self.host, self.username, self.password, self.ppsid, self.side)
            self.waitForElementLoad("div.keyboard-actions > img","css").click()
            rackID = slotList[0]
            self.kbenter(rackID) 
            time.sleep(1)
            self.addToDict(itemDict, barcode,(kqCount,rackID))
            time.sleep(1)
            
        return itemDict
        
    def pickFront(self,barcode):
        driver =self.driver
        driver.set_window_size(1200, 600)
        timeOut =300
        while timeOut >0:
            action = driver.find_element_by_class_name("action").text
            print "Current status is : %s"%action
            if action == "Wait for MSU":
                print "Sleeping for 10 seconds"
                timeOut-=10
                time.sleep(10)
            else:
                break
        
        time.sleep(0.1)
        driver.find_element_by_css_selector("div.keyboard-actions > img").click()
        self.kbenter(barcode) 
        time.sleep(0.1)
        selected = self.getbinInfo()[5]
        
        pptl =selected.keys()[0]
        print "Item is Placed at bin location: %s"%pptl
        pptlObj = selected.get(pptl)
        pptlObj.click()
        if len(self.getbinInfo()[5].keys()) == 0:
            print "PPS pick front operation completed"
        return  pptl
            
    def pickBack(self):
        driver =self.driver
        driver.set_window_size(1200, 600)
      
        selected = self.getbinInfo()[5]
        time.sleep(0.2)
        for sBins in selected.keys():
            pptl =sBins
            print "Item is Placed at bin location: %s"%pptl
            pptlObj = selected.get(pptl)
            pptlObj.click()
            time.sleep(0.2)
            try:
                pptlObj.click()
            except: 
                pass
            if len(self.getbinInfo()[5].keys()) == 0:
                print "PPS pick back operation completed"
        return  str(selected.keys())
   
if __name__ == "__main__":
    execType = "remote"
    import config
    skuList =[]
    confDict = config.datasheet("../conf/config_binola.xls")[0]['products_binola']
    for keys in  confDict:
        skuList.append(confDict[keys]['product_barcodes'])
    print skuList
    print len(skuList)
    ip = "192.168.8.62"
    ppspick = ["1"]
    ppsput = ["5"]#"PPS front 1"
    user = "admin"
    passwd = "apj0702"
    #barcode = [("VS51401",1)]
    barcode = [(x,1) for x in skuList[-1::-1]]
#     barcode = [("B0_MOB_8",100),
#                ("B3_HEAD_2",9),
#                ("B0_MOB_1",8),
#                ("B0_MOB_2",70),
#                ("B0_MOB_3",6),
#                ("B0_MOB_4",5),
#                ("B0_MOB_5",4),
#                ("B0_MOB_6",30),
#                ("B0_MOB_7",2),
#                ("B0_MOB_8",1),
#                ("B0_MOB_9",90),
#                ("B0_MOB_10",9),
#                ("B3_HEAD_1",9),]
#  
    #barcode = "barcode_sku_1001"
    #018.0.A.02
    #slotList = self.apiHelperObj.getRackSlotBarcode(ip, user, passwd, "5", "front")
    #print self.apiHelperObj.getPpsIdMode(ip,user, passwd)  
     
    ppsobj = ppsoperations()
    init = time.time()
    if sys.argv[1] == "putback":
        ppsobj.ppsputback(barcode,ip,"2", user, passwd,execType)
    else:
        ppsobj.ppsputfront(ip,"2", user, passwd,execType)
    final = time.time()
    print final-init
    
    #self.apiHelperObj.add_order(ip, user, passwd, "/home/demo/workspace/test/goal/conf/config_sanity.xls", "orders_demo", "orderlines_demo")
    #ppsobj.ppspickfront(ip,"1" , user, passwd)
    #ppsobj.ppspickback(ip,"1" , user, passwd)
     
