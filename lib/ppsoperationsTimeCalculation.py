ppsKB = {'$': ('ui-keyboard-36', False), '(': ('ui-keyboard-40', False), ',': ('ui-keyboard-44', False), 
         '0': ('ui-keyboard-0', False), '4': ('ui-keyboard-4', False), '8': ('ui-keyboard-8', False), 
         '<': ('ui-keyboard-60', False), '@': ('ui-keyboard-64', False), 'D': ('ui-keyboard-D', True), 
         'H': ('ui-keyboard-H', True), 'L': ('ui-keyboard-L', True), 'P': ('ui-keyboard-P', True), 
         'T': ('ui-keyboard-T', True), 'X': ('ui-keyboard-X', True), '`': ('ui-keyboard-96', False), 
         'd': ('ui-keyboard-d', False), 'h': ('ui-keyboard-h', False), 'l': ('ui-keyboard-l', False), 
         'p': ('ui-keyboard-p', False), 't': ('ui-keyboard-t', False), 'x': ('ui-keyboard-x', False), 
         '|': ('ui-keyboard-124', False), '#': ('ui-keyboard-35', False), "'": ('ui-keyboard-39', False), 
         '+': ('ui-keyboard-43', False), '/': ('ui-keyboard-47', False), '3': ('ui-keyboard-3', False), 
         '7': ('ui-keyboard-7', False), ';': ('ui-keyboard-59', False), '?': ('ui-keyboard-63', False), 
         'C': ('ui-keyboard-C', True), 'G': ('ui-keyboard-G', True), 'K': ('ui-keyboard-K', True), 
         'O': ('ui-keyboard-O', True), 'S': ('ui-keyboard-S', True), 'W': ('ui-keyboard-W', True), 
         '[': ('ui-keyboard-91', False), '_': ('ui-keyboard-95', False), 'c': ('ui-keyboard-c', False), 
         'g': ('ui-keyboard-g', False), 'k': ('ui-keyboard-k', False), 'o': ('ui-keyboard-o', False), 
         's': ('ui-keyboard-s', False), 'w': ('ui-keyboard-w', False), '{': ('ui-keyboard-123', False), 
         '"': ('ui-keyboard-34', False), '&': ('ui-keyboard-38', False), '*': ('ui-keyboard-42', False), 
         '.': ('ui-keyboard-46', False), '2': ('ui-keyboard-2', False), '6': ('ui-keyboard-6', False), 
         ':': ('ui-keyboard-58', False), '>': ('ui-keyboard-62', False), 'B': ('ui-keyboard-B', True), 
         'F': ('ui-keyboard-F', True), 'J': ('ui-keyboard-J', True), 'N': ('ui-keyboard-N', True), 
         'R': ('ui-keyboard-R', True), 'V': ('ui-keyboard-V', True), 'Z': ('ui-keyboard-Z', True), 
         '^': ('ui-keyboard-94', False), 'b': ('ui-keyboard-b', False), 'f': ('ui-keyboard-f', False), 
         'j': ('ui-keyboard-j', False), 'n': ('ui-keyboard-n', False), 'r': ('ui-keyboard-r', False), 
         'v': ('ui-keyboard-v', False), 'z': ('ui-keyboard-z', False), '~': ('ui-keyboard-126', False), 
         '!': ('ui-keyboard-33', False), '%': ('ui-keyboard-37', False), ')': ('ui-keyboard-41', False), 
         '-': ('ui-keyboard-45', False), '1': ('ui-keyboard-1', False), '5': ('ui-keyboard-5', False), 
         '9': ('ui-keyboard-9', False), '=': ('ui-keyboard-61', False), 'A': ('ui-keyboard-A', True), 
         'E': ('ui-keyboard-E', True), 'I': ('ui-keyboard-I', True), 'M': ('ui-keyboard-M', True), 
         'Q': ('ui-keyboard-Q', True), 'U': ('ui-keyboard-U', True), 'Y': ('ui-keyboard-Y', True), 
         ']': ('ui-keyboard-93', False), 'a': ('ui-keyboard-a', False), 'e': ('ui-keyboard-e', False), 
         'i': ('ui-keyboard-i', False), 'm': ('ui-keyboard-m', False), 'q': ('ui-keyboard-q', False), 
         'u': ('ui-keyboard-u', False), 'y': ('ui-keyboard-y', False), '}': ('ui-keyboard-125', False)}


import time

from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By


from apiHelper import apiHelper


class ppsoperations:
    def __init__(self,fn):
        self.fp = open(fn,"w+")
    def ppsputback(self,barcode, ip,pps ,user,passwd):
        putBackObj = launchpps(ip,self.fp)
        putBackObj.enterCredentials(pps, user, passwd,"back")
        out = putBackObj.putBack(barcode)
        putBackObj.driver.close() 
        return out
        
    def ppsputfront(self,barcode, ip,pps ,user,passwd):
        putFrontObj = launchpps(ip,self.fp)
        putFrontObj.enterCredentials(pps, user, passwd,"front")
        out = putFrontObj.putFront(barcode)
        putFrontObj.driver.close() 
        return out
              
    def ppspickfront(self,barcode, ip,pps ,user,passwd):
        pickFrontObj = launchpps(ip,self.fp)
        pickFrontObj.enterCredentials(pps, user, passwd,"front")
        out = pickFrontObj.pickFront(barcode)
        pickFrontObj.driver.close()
        return out
        
    def ppspickback(self, ip,pps ,user,passwd):
        pickBackObj = launchpps(ip,self.fp)
        pickBackObj.enterCredentials(pps, user, passwd,"back")
        out=    pickBackObj.pickBack()
        pickBackObj.driver.close() 
        return out
        
    def ppspickfrontlogin(self, ip,pps ,user,passwd):
        self.pickFrontObj = launchpps(ip)
        self.pickFrontObj.enterCredentials(pps, user, passwd,"front")
        return self.pickFrontObj
        
    def ppspickfrontlogout(self,obj):
        obj.driver.close()
    
    def __del__(self):
        self.fp.close()
class launchpps:
    def __init__(self,serverIP,fp):
#         try:
#             self.driver = webdriver.Firefox()
#         except:
        self.driver = webdriver.Chrome()
        #self.driver = webdriver.Remote("http://192.168.8.80:4444/wd/hub", webdriver.DesiredCapabilities.FIREFOX.copy())
        self.driver.implicitly_wait(30)
        #self.base_url = "https://192.168.1.13/"
        self.timeout = 10
        self.base_url = "https://%s/"%serverIP
        self.verificationErrors = []
        self.accept_next_alert = True
        self.host = serverIP
        self.fp = fp

            
    def kbenter(self,text):
        driver = self.driver
        for ch in text:
            val,caps = ppsKB[ch]
            time.sleep(0.5)
            if caps:
                #driver.find_element_by_class_name("ui-keyboard-shift").click()
                driver.find_element_by_xpath("(//button[@type='button'])[41]").click()
                time.sleep(0.2)
                driver.find_element_by_class_name(val).click()
                time.sleep(0.2)
                driver.find_element_by_xpath("(//button[@type='button'])[94]").click()
                #driver.find_element_by_class_name("ui-keyboard-shift").click()
            else:
                driver.find_element_by_class_name(val).click() 
        time.sleep(0.1)
        driver.find_element_by_class_name("ui-keyboard-accept").click()
        
    
    def enterCredentials(self,pps, username,password,side):
        if side == "back":
            ppsname = "PPS back %s"%pps
        if side == "front":
            ppsname = "PPS front %s"%pps
        self.ppsid = pps
        self.side =side
        self.username = username
        self.password = password
        driver = self.driver
        driver.set_window_size(1200, 600)
        init = time.time()
        driver.get(self.base_url + "/ui/")
        self.waitForPageLoad("loginBtn")
        final = time.time()
        print "Login Page Load time",final-init
        self.fp.write("Login Page Load time,%0.2f \n"%(final-init))
        # driver.
        #Select(driver.find_element_by_css_selector("select.selectPPS")).select_by_visible_text("PPS front 2")
        Select(driver.find_element_by_css_selector("select.selectPPS")).select_by_visible_text(ppsname)
        time.sleep(0.2)
        driver.find_element_by_id("username").click()
        self.kbenter(username)
        time.sleep(0.2)
        driver.find_element_by_id("password").click()
        self.kbenter(password)
        time.sleep(0.2)
        driver.find_element_by_id("loginBtn").click()     
        
    def getbinInfo(self):
        stagedbins = {}
        usedbins = {}
        emptybins = {}
        useselectedbins ={}
        selectedbins = {}
        driver = self.driver
        
        driver.implicitly_wait(5)
        mybins = driver.find_elements_by_xpath("//div[@class='bin staged']")
        for mybin in mybins:
            stagedbins[mybin.find_element_by_class_name("pptl").text] = \
            mybin.find_element_by_class_name("item-count").text
        
        mybins = driver.find_elements_by_xpath("//div[@class='bin use']")
        for mybin in mybins:
            usedbins[mybin.find_element_by_class_name("pptl").text] = \
            mybin.find_element_by_class_name("item-count").text
        
        mybins = driver.find_elements_by_xpath("//div[@class='bin empty']")                    
        for mybin in mybins:
            emptybins[mybin.find_element_by_class_name("pptl").text] = \
            mybin.find_element_by_class_name("item-count").text
            
        mybins = driver.find_elements_by_xpath("//div[@class='bin selected']")                    
        for mybin in mybins:
            selectedbins[mybin.find_element_by_class_name("pptl").text] = \
            mybin.find_element_by_class_name("pptl")
            
        mybins = driver.find_elements_by_xpath("//div[@class='bin use selected-staging']")                    
        for mybin in mybins:
            useselectedbins[mybin.find_element_by_class_name("pptl").text] = \
            mybin.find_element_by_class_name("item-count").text
        
        totalBins = len(stagedbins)+len(usedbins)+len(emptybins)+\
        len(selectedbins) + len(useselectedbins)
        
        print "Staged Bins :", stagedbins
        print "used Bins : ", usedbins
        print "empty Bins : ", emptybins
        print "selected Bins : ",selectedbins
        print "use selected-staging Bins", useselectedbins
        return totalBins, stagedbins, usedbins, emptybins, useselectedbins, selectedbins
     
    def dict_diff(self, dict_a, dict_b):
        return dict([
            (key, dict_b.get(key, dict_a.get(key)))
            for key in set(dict_a.keys()+dict_b.keys())
            if (
                (key in dict_a and (not key in dict_b or dict_a[key] != dict_b[key])) or
                (key in dict_b and (not key in dict_a or dict_a[key] != dict_b[key])))])   
    
    def waitForPageLoad(self,element, find = "id"):
        try:
            if find == "id":
                element_present = EC.presence_of_element_located((By.ID, element))
            elif find == "class":
                element_present = EC.presence_of_element_located((By.CLASS_NAME, element))
            WebDriverWait(self.driver, self.timeout).until(element_present)
            return True
        except TimeoutException:
            print "Timed out waiting for page to load"
            return False 
        
    def getNotifyText(self):
        return self.driver.find_element_by_css_selector("div.notify > span").text
     
    def textTime(self,mystr):
        tt = 0
        for i in mystr:
            caps = ppsKB[i][1]
            if caps:
                tt += 1
            else:
                tt += 0.6
        return tt
                
    def putBack(self,barcode):   
        driver = self.driver
        init = time.time()
        self.waitForPageLoad("action","class")
        while True:
            if self.waitForPageLoad("action","class"):
                break
            
        if driver.find_element_by_class_name("action").text == "Stage Bin or Scan Entity":
            final = time.time()
            print "Put Back Page Load time",final-init
            self.fp.write("Put Back Page Load time,%0.2f \n"%(final-init))
        initSelected = self.getbinInfo()[5]
        ## Entering Barcode and verification
        time.sleep(0.5)
        init = time.time()
        driver.find_element_by_css_selector("div.keyboard-actions > img").click()
        self.kbenter(barcode) 
        while True:
            if self.getNotifyText() == "Entity scan successful.":
                break
        final = time.time()
        print "Barcode Scan time",final-init-self.textTime(barcode)
        self.fp.write("Barcode Scan time,%0.2f \n"%(final-init-self.textTime(barcode)))
        self.getbinInfo()
        finalSelected = self.getbinInfo()[5]
        
        newAddition = self.dict_diff(finalSelected,initSelected)
        pptl = False
        if len(newAddition.keys()) == 1:
            pptl =newAddition.keys()[0]
            print "Item is Placed at bin location: %s"%pptl
            pptlObj = newAddition.get(pptl)
            init = time.time()
            pptlObj.click()
            while True:
                if self.getNotifyText() == "PPTL press successful":
                    break
            final = time.time()
            print "PPTL Press time",final-init
            self.fp.write("PPTL Press time,%0.2f \n"%(final-init))
            
            time.sleep(1)
            ###Pressing PPTL 
            init = time.time()
            pptlObj.click()
            while True:
                if self.getNotifyText() == "Bin %s selected"%pptl:
                    break
            driver.find_element_by_link_text("Stage").click()
            final = time.time()
            print "Stage time",final-init
            self.fp.write("Stage time,%0.2f \n"%(final-init))
            
            
        else:
            print "Error Scenario Exiting"
            driver.close()
        return  pptl
    
    def putFront(self,barcode,putAll = True):
        driver =self.driver
        init = time.time()
        self.waitForPageLoad("action","class")
        while True:
            if self.waitForPageLoad("action","class"):
                break
        final = time.time()
        print "Put Front Page Load time",final-init
        self.fp.write("Put Front Page Load time,%0.2f \n"%(final-init))
        timeOut =300
        init = time.time()
        while timeOut >0:
            action = driver.find_element_by_class_name("action").text
            print "Current status is : %s"%action
            if action == "Wait for MSU":
                #print "Sleeping for 10 seconds"
                timeOut-=1
                time.sleep(1)
            else:
                break
        final = time.time()
        print "MSU Reach time",final-init
        self.fp.write("MSU Reach time,%0.2f \n"%(final-init))
               
        initSelected = self.getbinInfo()[5]
        print "Item to be picked, placed at bin location: %s"%initSelected.keys()[0]
        time.sleep(0.1)
        init = time.time()
        driver.find_element_by_css_selector("div.keyboard-actions > img").click()
        self.kbenter(barcode) 
        while True:
            if self.getNotifyText() == "Entity scan successful":
                break
        final = time.time()
        print "Barcode Scan time",final-init-self.textTime(barcode)
        self.fp.write("Barcode Scan time,%0.2f \n"%(final-init-self.textTime(barcode)))
        time.sleep(0.1)
        slotList = apiHelper().getRackSlotBarcode(self.host, self.username, self.password, self.ppsid, self.side)
        time.sleep(0.1)
        init = time.time()
        driver.find_element_by_css_selector("div.keyboard-actions > img").click()
        rackID = slotList[0]
        self.kbenter(rackID) 
        while True:
            if ("System is Idle" == driver.find_element_by_css_selector("div.idleScreen").text):
                break
        final = time.time()
        print "Slot Scan time",final-init-self.textTime(rackID)
        self.fp.write("Slot Scan time,%0.2f \n"%(final-init-self.textTime(rackID)))
        time.sleep(0.1)
        return rackID 
        
    def pickFront(self,barcode):
        driver =self.driver
        init = time.time()
        self.waitForPageLoad("action","class")
        while True:
            if self.waitForPageLoad("action","class"):
                break
        final = time.time()
        print "Pick Front Page Load time",final-init
        self.fp.write("Pick Front Page Load time,%0.2f \n"%(final-init))
        driver.set_window_size(1200, 600)
        timeOut =300
        init = time.time()
        while timeOut >0:
            action = driver.find_element_by_class_name("action").text
            print "Current status is : %s"%action
            if action == "Wait for MSU":
                #print "Sleeping for 10 seconds"
                timeOut-=1
                time.sleep(1)
            else:
                break
        final = time.time()
        print "MSU Reach time",final-init
        self.fp.write("MSU Reach time,%0.2f \n"%(final-init))
        time.sleep(0.1)
        init = time.time()
        driver.find_element_by_css_selector("div.keyboard-actions > img").click()
        self.kbenter(barcode) 
        while True:
            if self.getNotifyText() == "Item scan successful":
                break
        final = time.time()
        print "Item Scan time",final-init-self.textTime(barcode)
        self.fp.write("Item Scan time,%0.2f \n"%(final-init-self.textTime(barcode)))
        time.sleep(0.1)
        selected = self.getbinInfo()[5]
        
        pptl =selected.keys()[0]
        print "Item is Placed at bin location: %s"%pptl
        pptlObj = selected.get(pptl)
        init = time.time()
        pptlObj.click()
        while True:
            if ("System is Idle" == driver.find_element_by_css_selector("div.idleScreen").text):
                break
        final = time.time()
        print "PPTL Press time",final-init
        self.fp.write("PPTL Press time,%0.2f \n"%(final-init))
        if len(self.getbinInfo()[5].keys()) == 0:
            print "PPS pick front operation completed"
        return  pptl
            
    def pickBack(self):
        driver =self.driver
        driver.set_window_size(1200, 600)
      
        selected = self.getbinInfo()[5]
        time.sleep(0.2)
        for sBins in selected.keys():
            pptl =sBins
            print "Item is Placed at bin location: %s"%pptl
            pptlObj = selected.get(pptl)
            init = time.time()
            pptlObj.click()
            while True:
                if ("System is Idle" == driver.find_element_by_css_selector("div.idleScreen").text):
                    break
            final = time.time()
            print "PPTL Press time",final-init
            self.fp.write("PPTL Press time,%0.2f \n"%(final-init))
            time.sleep(0.2)
            try:
                pptlObj.click()
            except: 
                pass
            if len(self.getbinInfo()[5].keys()) == 0:
                print "PPS pick back operation completed"
        return  str(selected.keys())
    
    
        
if __name__ == "__main__":
    ip = "192.168.8.87"
    #driver = webdriver.Chrome()
    barcode = "2001"
    user = "admin"
    passwd = "apj0702"
    for i in range(10):
        ppsobj = ppsoperations("timingMD_%d.csv"%i)
        ppsobj.ppsputback(barcode,ip,"5","admin","apj0702")
        ppsobj.ppsputfront(barcode,ip,"5", user, passwd)
    #    
        apiHelper().add_order(ip, user, passwd, "/Users/abhishek/Documents/workspace/test/robocop/conf/config_sanity.xls", "orders_demo", "orderlines_demo")
        ppsobj.ppspickfront(barcode,ip,"1" , user, passwd)
        ppsobj.ppspickback(ip,"1" , user, passwd)
    
