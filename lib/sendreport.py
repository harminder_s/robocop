import cgi
from email import encoders
from email.MIMEBase import MIMEBase
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.mime.image import MIMEImage
import tarfile, os, smtplib, time
import uuid
import sshbasic

from robot.api import logger as trace
from robot.libraries.Screenshot import Screenshot
from selenium import webdriver
from robot.api import ExecutionResult

class sendreport:
    def tarOutput(self,outPath):
        outTar = "%s.tar.gz"%outPath
        tar = tarfile.open(outTar, "w:gz")
        tar.add(outPath, arcname=os.path.basename(outPath))
        tar.close()
        trace.info("Tar '%s'created Successfully"%outTar, also_console=True)
        return outTar
    
    def capturePng(self, reportPath):
        try:
            driver = webdriver.Firefox()
        except:
            driver = webdriver.Chrome()
        pth = "file:///%s#totals?all"%reportPath
        driver.get(pth)
        screenShot = os.path.splitext(reportPath)[0]+".png"
        driver.save_screenshot(screenShot)
        driver.close()
        trace.info("screenshot '%s'created Successfully"%screenShot, also_console=True)
        return screenShot
    
    def uploadData(self,outPutDir, extra_pth, artifactoryServer="139.162.23.74",un = "gor", pw ="Nu0iewaj"):
        result = ExecutionResult('%s/output.xml'%outPutDir)
        stats = result.statistics
        if stats.total.critical.failed == 0:
            sshObj = sshbasic.sshbasic(artifactoryServer,un,pw,"other")
            fp = open("%s/packagename"%extra_pth,"r")
            fName,debHtml = fp.read().strip().split(",")#.strip()
            fp.close()
            resDir = "/var/www/html/butler/releases/dev/%s"%(time.strftime("%Y%m%d_%H%M%S",time.gmtime(time.time())))
            sshObj.ssh.execute_command("mkdir %s"%resDir)
            sshObj.copyToRemote(fName,resDir)
            sshObj.copyToRemote(debHtml,resDir)
            sshObj.copyToRemote("%s/report.html"%outPutDir,resDir)
            sshObj.copyToRemote("%s/log.html"%outPutDir,resDir)
            print "Build and results has been uploaded to artifactory server"
        else:
            print "No Build and results has been uploaded to artifactory server due to failures"
        
    def sendMail(self,toaddr,sub = "",screenShot = False,outTar =False):
        
        fromaddr = "abhishek@greyorange.sg"
        msg = MIMEMultipart('related')
        msg['From'] = "robocop<abhishek@greyorange.sg>"
        msg['To'] = toaddr
        msg['Subject'] = "Smoke Test Report - "+sub
         
         
        msg_alternative = MIMEMultipart('alternative')
        msg.attach(msg_alternative)
        if screenShot :
            img = dict(title="SmokeTest...", path='%s'%screenShot, cid=str(uuid.uuid4()))
            msg_text = MIMEText('[image: {title}]'.format(**img), 'plain', 'utf-8')
            msg_alternative.attach(msg_text)
             
            msg_html = MIMEText('<div dir="ltr">'
                                 '<img src="cid:{cid}" alt="{alt}"><br></div>'
                                .format(alt=cgi.escape(img['title'], quote=True), **img),
                                'html', 'utf-8')
            msg_alternative.attach(msg_html)
         
            with open(img['path'], 'rb') as ifile:
                msg_image = MIMEImage(ifile.read(), name=os.path.basename(img['path']))
                msg.attach(msg_image)
            msg_image.add_header('Content-ID', '<{}>'.format(img['cid']))
        
        if outTar:
            part = MIMEBase('application', 'octet-stream')
            outfp = open(outTar,'rb')
            part.set_payload((outfp).read())
            filename = os.path.basename(outTar)
            encoders.encode_base64(part)
            part.add_header('Content-Disposition', "attachment; filename= %s" % filename)
            msg.attach(part)
            outfp.close()
           
        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.starttls()
        server.login(fromaddr, "zulicvzxsbugydrg")
        text = msg.as_string()
        toaddrList = toaddr.split(",")
        server.sendmail(fromaddr, toaddrList, text)
        trace.info("Mail send to %s"%toaddr, also_console=True)
        server.quit()
if __name__ == "__main__":
    #pngfile = sendreport().capturePng("/home/gor/goal/results/SmokeTest_20160617_053232/report.html")
    #tarfile = sendreport().tarOutput("/home/gor/goal/results/SmokeTest_20160617_053232")
    #sendreport().sendMail("abhishek@greyorange.sg")
    sendreport().uploadData("/Users/abhishek/Documents/workspace/test/robocop/results/test.robot_20160802_033041","/Users/abhishek/Documents/workspace/test/robocop/extra")