import os, re, traceback, commands

from SSHLibrary import SSHLibrary 
from robot.api import logger as trace
from robot.libraries.BuiltIn import BuiltIn


robo = BuiltIn()

class sshbasic:
    #servers ={}
    
    def __init__(self,ip,un= "gor",pw="apj0702",serverType = "gor"):
            self.ssh = SSHLibrary()
            self.gorPrompt = "%"
            self.suPrompt = "#"
            self.ip = ip
            self.un = un
            self.pw = pw
            self.type = serverType
            self.sudo = False
            
            if self.__isValidIP():
                if self.__pingIP():
                    if self.type == "gor":
                        self.__loginTogorServer()
                    else:
                        self.__loginToServer()
                else:
                    errorMsg = "Error:  Not able to Ping '%s'" % self.ip
                    BuiltIn().fail(errorMsg)
                    return False
            else:
                    errorMsg = "Error:  Invalid IP '%s'" % self.ip
                    BuiltIn().fail(errorMsg)
                    return False
            
    def closeConnection(self):
        self.ssh.close_connection()            
    
    def __del__(self):
        self.ssh.close_connection()
        
    def __isValidIP(self): 
        ip_str = self.ip
        try:
            if len(ip_str.split()) == 1: 
                ipList = ip_str.split('.') 
                if len(ipList) == 4: 
                    for i, item in enumerate(ipList):                       
                        try: 
                            ipList[i] = int(item) 
                        except: 
                            return False                    
                    if max(ipList) > 255: 
                        return False 
                else:
                    return False    
                return True 
        except Exception:
            errorMsg = "Error:  %s" % traceback.format_exc()
            BuiltIn().fail(errorMsg)
            return False

    def __pingIP(self):
        #=======================================================================
        # Function ping
        # Description:  to ping an IP 
        # Parameters:
        # host  : ip of system to be pinged       
        # Return: True if ping successful otherwise false
        #=======================================================================   
        try:
            if (os.name == "posix"):
                cmd = "ping -c 2 %s"%self.ip
            else:
                cmd = "ping %s"%self.ip
            if  commands.getstatusoutput(cmd)[0] == 0:
                return True
            return False
        except Exception:
            errorMsg = "Error:  %s" % traceback.format_exc()
            BuiltIn().fail(errorMsg)
            return False
        
    def __loginTogorServer(self):
        trace.info("Login to %s"%self.ip)
        self.ssh.open_connection(self.ip,timeout="30 min",term_type="pty:0")
        self.ssh.login(self.un, self.pw)
        self.ssh.write(" ",loglevel="DEBUG")
        line = self.ssh.read(loglevel="DEBUG",delay="1s") 
        self.gorPrompt =    self.__getgorPrompt(line)
        self.ssh.write("sudo su -",loglevel="DEBUG")
        line = self.ssh.read(loglevel="DEBUG",delay="1s")
        sudoPasswdStr = "[sudo] password for %s:"%self.un
        if line.find(sudoPasswdStr) != -1:
            self.ssh.write(self.pw)
            line = self.ssh.read(loglevel="DEBUG",delay="1s")
        self.sudoPrompt=    self.__getsudoPrompt(line)
        self.ssh.set_client_configuration(prompt=self.gorPrompt)
        self.ssh.write("exit",loglevel="DEBUG")
        line = self.ssh.read_until_prompt(loglevel="DEBUG")

            
    def __loginToServer(self):
        trace.info("Login to %s"%self.ip)
        print "Login to %s"%self.ip
        self.ssh.open_connection(self.ip,timeout="1 hour",term_type="pty:0")
        self.ssh.login(self.un, self.pw)
        
    
    def executeCli(self,cmd,sudo=False):
        output = ""
        try:
            trace.info("Executing command : %s"%cmd,also_console=True)
            cmdList = re.split("\s+",cmd)
            if cmdList[0].strip() == "sudo":
                cmd = "sudo -k "+" ".join(cmdList[1:])
                self.sudo = True
            if sudo:
                cmd = "sudo "+cmd
                self.sudo = True
            self.ssh.write(cmd,loglevel="DEBUG")
            var = self.gorPrompt
            cmdList = cmd.split(";")
            for cdcmd in cmdList:                
                m = re.search("\W*cd\s+(?P<target>\S+)\W*",cdcmd)
                if m:
                    target = m.group("target")
                    m1 = re.search("^\S+@\S+\s+(?P<dir>\S+)\s*%\s*",var)
                    if m1:
                        mydir = m1.group("dir")
                        homeDir = "/home/%s"%self.un
                        if target.find("/var") != -1:
                            toReplace = "~"+os.path.basename(target.strip("/"))
                        elif target.find(homeDir) != -1:
                            target = re.sub(homeDir,"~",target)
                            toReplace = target
                        else:
                            toReplace = target
                        var = re.sub(mydir,toReplace,var)
                        self.gorPrompt = var
            self.ssh.set_client_configuration(prompt=var)
            pat = re.compile(r"%s"%re.escape(var))
            if self.sudo:
                sudoPasswdStr = "password for %s:"%self.un
                cmdOut = self.ssh.read_until(sudoPasswdStr,loglevel="DEBUG")
                if cmdOut.find(sudoPasswdStr) != -1:
                    self.ssh.write(self.pw)
                self.sudo = False
            ansi_escape = re.compile(r'\x1b[^m]*m')
            
            cmdOut = self.ssh.read_until_prompt(loglevel="DEBUG")
            cleanOut = ansi_escape.sub('', cmdOut)
            output =  re.sub(pat,"",cleanOut).strip().strip("%").strip()
            trace.info("Output :\n%s"%output,also_console=True) 
            return output
        except RuntimeError:
            return traceback.format_exc()
        except Exception:
            errorMsg = "Error:  %s" % traceback.format_exc()
            BuiltIn().fail(errorMsg)
            return False
        
    def fileExist(self,filewithFullPath):
        mydir = os.path.dirname(filewithFullPath)
        myfile = os.path.basename(filewithFullPath)
        
        allfiles = self.ssh.list_files_in_directory(mydir)
        if myfile in allfiles:
            return True
        return False
    
    def copyToRemote(self,localFilePath,remotePath,mode="0744"):
        try:
            trace.info("Copying %s to %s:%s"%(localFilePath,self.ip,remotePath),also_console=True)
            self.ssh.put_file(localFilePath,remotePath,mode)
            return True
        except Exception:
            errorMsg = "Error:  %s" % traceback.format_exc()
            BuiltIn().fail(errorMsg)
            return False
        
    def copyFromRemote(self,remotePath,localPath):
        try:
            trace.info("Copying %s from %s to local:%s"%(remotePath,self.ip,localPath),also_console=True)
           
            self.ssh.get_file(remotePath,localPath)
            return True
        except Exception:
            errorMsg = "Error:  %s" % traceback.format_exc()
            BuiltIn().fail(errorMsg)
            return False
  
    def __getgorPrompt(self,line):
        try:
            m1 = re.search("\s*(?P<gorPrompt>\S+\s+\S+\s+%)\s*\S*",line.strip(),re.MULTILINE)
            if m1:
                gorPrompt = m1.group("gorPrompt")
                return gorPrompt.strip()
            else:
                return False
        except Exception:
            errorMsg = "Error:  %s" % traceback.format_exc()
            BuiltIn().fail(errorMsg)
            return False
        
    def __getsudoPrompt(self,line):
        try:
            m1 = re.search("\s*(?P<sudoPrompt>\S+\s*#)\s*",line.strip(),re.MULTILINE)
            if m1:
                sudoPrompt = m1.group("sudoPrompt")
                return sudoPrompt
            else:
                return False
        except Exception:
            errorMsg = "Error:  %s" % traceback.format_exc()
            BuiltIn().fail(errorMsg)
            return False

if  __name__ == "__main__":
    a = sshbasic("139.162.23.74","gor", "Nu0iewaj","other")
    #a.ssh.set_client_configuration(timeout = "3 sec")
    # print a.executeOnButlerRemote("ls -lrt")
    