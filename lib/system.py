from dashboard import dashboard
import time

class system:
    def __init__(self, serverIP, username="admin", password="apj0702", execution="remote"):
        self.username = username
        self.password = password

    def systemTabEvents(self, serverIP, username, password, execution):
        self.dash_obj = dashboard(serverIP, username="admin", password="apj0702", execution="remote")
        self.dash_obj.openBrowser(serverIP, username, password, execution)
        self.dash_obj.loginDashboard()
        self.dash_obj.ui.clickButton("tabs.systemtab")

    def closeSystemEvent(self):
        self.dash_obj.closeBrowser()

    def veriyButlerBotsSubTab(self, serverIP, username, password, execution):
        self.systemTabEvents(serverIP, username, password, execution)
        self.dash_obj.ui.clickButton("header.systembutletbotslink")
        assert self.dash_obj.ui.awaitForElementPresence("header.filterbtndata", 30), "Filter Button is not present"
        expectedTableHeaders = ["BOTS", "STATUS", "CURRENT TASK", "MSU MOUNTED", "LOCATION", "VOLTAGE"]
        actualTableHeaders = self.dash_obj.ui.getMultipleElementsText("header.systemsubheaders")
        assert actualTableHeaders == expectedTableHeaders, "Butler Bots Table not formed correctly."

    def verifyPickPutStationsSubTab(self, serverIP, username, password, execution):
        self.systemTabEvents(serverIP, username, password, execution)
        self.dash_obj.ui.clickButton("header.systemppslink")
        assert self.dash_obj.ui.awaitForElementToBeVisible("header.filterbtndata", 30), "Filter Button is not present"
        expectedTableHeaders = ["PPS", "STATUS", "OPERATING MODE", "PERFORMANCE", "OPERATOR ASSIGNED"]
        actualTableHeaders = self.dash_obj.ui.getMultipleElementsText("header.systemsubheaders")
        assert actualTableHeaders == expectedTableHeaders, "Pick Put Station Table not formed correctly."
        expectedPpsCount = 4
        actualPpsCount = self.dash_obj.ui.getCountOfElements("header.ppsrows")
        assert actualPpsCount == expectedPpsCount, "All Pick Put Stations details are not present on MD. Expected PPS:" \
                                                   + str(expectedPpsCount) + ".Actual PPS:" + str(actualPpsCount)

        self.dash_obj.ui.clickPpsBox("audit")
        self.dash_obj.ui.clickPpsBox("put")
        self.dash_obj.ui.clickPpsBox("pick")

    def verifyChargingStationSubTab(self, serverIP, username, password, execution):
        self.systemTabEvents(serverIP, username, password, execution)
        self.dash_obj.ui.clickButton("header.systemcslink")
        self.dash_obj.ui.forceWait(3)
        assert self.dash_obj.ui.awaitForElementToBeVisible("header.filterbtndata", 30), "Filter Button is not present"
        expectedTableHeaders = ["STATION ID", "STATUS", "OPERATING MODE", "BOTS CONNECTED"]
        actualTableHeaders = self.dash_obj.ui.getMultipleElementsText("header.systemsubheaders")
        assert actualTableHeaders == expectedTableHeaders, "Charging Station Table not formed correctly."
        expectedChargeStationCount = 4
        actualChargeStationCount = self.dash_obj.ui.getCountOfElements("header.ppsrows")
        assert actualChargeStationCount == expectedChargeStationCount, "All Charging Stations details are not present on MD. Expected CS:" \
                                                                       + str(expectedChargeStationCount) + ".Actual CS:" + str(actualChargeStationCount)

    def verifyPpsModeChange(self, serverIP, username, password, execution):
        self.systemTabEvents(serverIP, username, password, execution)
        self.dash_obj.ui.clickButton("header.systemppslink")
        self.dash_obj.ui.forceWait(3)
        assert self.dash_obj.ui.awaitForElementToBeVisible("header.filterbtndata", 30), "Filter Button is not present"
        self.dash_obj.ui.changePpsMode("put", "pick")
        assert self.dash_obj.ui.awaitForElementPresence("header.ppschangemodesuccess", 10), "Mode change isn't successful"