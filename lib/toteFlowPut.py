'''
Created on 20-Apr-2016

@author: nishantmahawar
'''

from apiHelper import apiHelper
from time import sleep
from robot.api import logger as trace
from robot.libraries.BuiltIn import BuiltIn
from config import getdata
from multiprocessing import Process, Queue, Lock
from robot.libraries.BuiltIn import BuiltIn
import re

class toteFlowPut:

       
    def tote_put(self, host, username, password, filename, product_spreadsheet, tote_spreadsheet, pick, put):
        ppsput = put.split(",")
        tote_config_dict = getdata(filename)[tote_spreadsheet]
        product_config_dict = getdata(filename)[product_spreadsheet]
        num_tote = len(tote_config_dict.keys())
        num_pps_put = len(ppsput)
        first_item = 1
        last_item = int(num_tote/num_pps_put)
        self.process = []
        self.queue = []
        self.error_sku = []
       # self.obj = apiHelper()
        
        for ppsid in ppsput:
            processname = "putBack-%s"%(ppsid)
            queue_obj = Queue()
            process_obj = Process(target=self.putBackToteOperations, args=(queue_obj, processname, host,username,password,ppsid,"back",product_config_dict, tote_config_dict,first_item,last_item))
            self.process.append(process_obj)
            self.queue.append(queue_obj)
            first_item = last_item + 1
            last_item = last_item + int(num_tote/num_pps_put)               
            process_obj.start()
            sleep(2)
             
            processname = "putFront-%s"%(ppsid)
            queue_obj = Queue()
            process_obj = Process(target=self.putFrontToteOperations, args=(queue_obj, processname, host,username,password,ppsid,"front"))
            self.process.append(process_obj)
            self.queue.append(queue_obj)
            process_obj.start()
            sleep(2)
            
        for p_obj in self.process:
            p_obj.join()    
        
        for q_obj in self.queue:
            trace.info("%s"%str(q_obj.get()), also_console=True)
        return self.error_sku

       
        
    def putBackToteOperations(self, queue, processname,host,username,password,ppsid,pps_side,product_config_dict,tote_config_dict, first_item,last_item):
#         self.obj = apiHelper()
        tag = first_item
        obj = apiHelper()
        obj.loginApi(host, username, password, ppsid=ppsid, pps_side="back")
#         self.obj.loginApi(host, username, password, ppsid=ppsid, pps_side="back")
        sleep(2)
#        count = 1
        while tag <= last_item:
#            count = count + 1
            item = {}
            tote_id = tote_config_dict[str(tag)]['tote_id']
            expectation_id = tote_config_dict[str(tag)]['expectation_id']
            str_tote_items = tote_config_dict[str(tag)]["items"].strip()
            list_tote_items = str_tote_items.split('\n')
            
            trace.info("%s: Tote ID: %s, Expectation ID: %s"%(processname, tote_id, expectation_id), also_console=True)
            
            lock_get_error_pps = Lock()
            lock_get_error_pps.acquire()
            obj.processBarcode(host, username, password, tote_id, ppsid, pps_side="back")
            trace.info("%s: Opening tote: %s"%(processname, tote_id), also_console=True)
            lock_get_error_pps.release()
            sleep(2)
            
            for entry in list_tote_items:
                pattern = '\s*product_tag\s*:\s*(\w),\s*quantity\s*:\s*(\d)\s*'
                product_tag = ""
                quantity = 1
                product_barcode = ""
                
                m_obj = re.match(pattern,entry)
                if(m_obj):
                    product_tag = m_obj.group(1)
                    quantity = int(m_obj.group(2))
                    product_barcode = product_config_dict[str(product_tag)]['product_barcodes']
                    item[product_barcode] = quantity
                else:
                    BuiltIn().fail("Data in spreadsheet having tag: %s is incorrect. Please correct it and run it again"%(tag))
                
            
            lock_get_error_pps = Lock()
            lock_get_error_pps.acquire()
            pps_bin_error = obj.getErrorPpsBin(host, username, password, ppsid, pps_side="back")
            lock_get_error_pps.release()
            
            for pps_bin_id, product_sku in pps_bin_error.iteritems():
                self.error_sku.append(product_sku)
                trace.info("%s: Error Sku - %s"%(processname, product_sku), also_console=True)
                lock_clear_error_pps = Lock()
                lock_clear_error_pps.acquire()
                pps_id = obj.clearErrorPpsBin(host, username, password, ppsid, "back", pps_bin_id)
                trace.info("%s: Cleared PPS Bin ID: %s having SKU: %s"%(processname, pps_id, product_sku), also_console=True)
                lock_clear_error_pps.release()
                
            for barcode, quantity in item.iteritems():
                time = 0
                lock_get_empty_bin = Lock()
                lock_get_empty_bin.acquire()   
                flag = obj.isScanAllowed(host, username, password, ppsid, pps_side="back") 
                lock_get_empty_bin.release()    
            
                while (not flag and time <= 300):
                    sleep(30)
                    time = time + 30
                    lock_get_empty_bin = Lock()
                    lock_get_empty_bin.acquire()   
                    flag = obj.isScanAllowed(host, username, password, ppsid, pps_side="back") 
                    lock_get_empty_bin.release()
                
                
                lock_process_barcode = Lock()
                lock_process_barcode.acquire()
                trace.info("%s: Putting Item against tote: %s having barcode: %s"%(processname, tote_id, barcode), also_console=True)
                obj.processBarcode(host, username, password, barcode, ppsid, pps_side="back")
                lock_process_barcode.release()
                        
                sleep(2)
                        
                if int(quantity) > 1:
                    lock_update_kq = Lock()
                    lock_update_kq.acquire()
                    obj.updateKQ(host, username, password, ppsid, pps_side="back",qty=int(quantity))
                    lock_update_kq.release()        
                    sleep(2)   
                
            
                lock_process_bin_event = Lock()
                lock_process_bin_event.acquire()
                pps_bin_id = obj.processPpsbinEvent(host, username, password, ppsbin_state="empty", ppsbin_event="secondary_button_press", ppsid=ppsid, pps_side="back")
                trace.info("%s: Placed item: %s on PPS Bin: %s, Quantity: %d"%(processname, barcode, str(pps_bin_id), int(quantity)), also_console=True)
                lock_process_bin_event.release()
                sleep(2)
                
                lock_stage_all = Lock()
                lock_stage_all.acquire()
                obj.stageAllEvent(ppsid=ppsid, pps_side="back")
                trace.info("%s: Staged all Items"%(processname), also_console=True)
                lock_stage_all.release()
                sleep(2)
                
                
            lock_get_error_pps = Lock()
            lock_get_error_pps.acquire()
            pps_bin_error = obj.processBarcode(host, username, password, tote_id, ppsid, pps_side="back")
            trace.info("%s: Closing tote: %s"%(processname, tote_id), also_console=True)
            lock_get_error_pps.release()
            sleep(2)   
    
            tag = tag + 1
                
        trace.info("%s"%(str(self.error_sku)),also_console=True)
        queue.put("%s: Successfully Completed PutBack Operations"%(processname))       
                
                
                            
                 
    def putFrontToteOperations(self, queue, processname,host,username,password,ppsid,pps_side):
        time = 0
        obj = apiHelper()
        obj.loginApi(host, username, password, ppsid=ppsid, pps_side="front")        
        while time < 300:
            
            lock_bin_list = Lock()
            lock_bin_list.acquire()
            pps_bin_id_list = obj.getInUsePpsBin(host, username, password, ppsid, pps_side="front")
            lock_bin_list.release()
            
            if(pps_bin_id_list):
                time = 0
                for pps_bin in pps_bin_id_list:
                    lock_wait_msu = Lock()
                    lock_wait_msu.acquire()
                    obj.waitForMSU(ppsid, "front")
                    lock_wait_msu.release()
                    
                    sleep(2)
                    
                    lock_get_barcode = Lock()
                    lock_get_barcode.acquire()
                    barcode = obj.getProductBarcodeToScan(host,username,password,ppsid, pps_side="front")
                    obj.processBarcode(host, username, password, barcode, ppsid, pps_side="front")
                    lock_get_barcode.release()
                    
                    sleep(2)
                    
                    lock_get_kq = Lock()
                    lock_get_kq.acquire()
                    qty = obj.getKQ(host, username, password, ppsid, pps_side="front")
                    lock_get_kq.release()
                    
                    if qty > 1:
                        lock_update_kq = Lock()
                        lock_update_kq.acquire()
                        obj.updateKQ(host, username, password, ppsid, pps_side="front", qty=qty)
                        lock_update_kq.release()
                        
                        sleep(2)
                    
                    lock_get_rack_slot = Lock()
                    lock_get_rack_slot.acquire()        
                    rack_slots = obj.getRackSlotBarcode(host,username,password,ppsid, pps_side="front")
                    obj.processBarcode(host, username, password, rack_slots[0], ppsid, pps_side="front")
                    lock_get_rack_slot.release()
                    
                    sleep(2)
                    
                    lock_trace1 = Lock()
                    lock_trace1.acquire()
                    trace.info("%s: Placed item: %s, Rack Slot: %s, Quantity: %d"%(processname, barcode, rack_slots, qty), also_console=True)
                    lock_trace1.release()
                    
            else:
                lock_trace2 = Lock()
                lock_trace2.acquire()
                trace.info("%s: No Item available in Put Front, Sleeping for 10 seconds. Idle Time: %d seconds"%(processname, time), also_console=True)
                lock_trace2.release()
                sleep(10)
                time = time + 10
        queue.put("%s: Successfully Completed PutFront Operations"%(processname))
            
            
            
            
            
if __name__ == "__main__":
    toteFlowPut().tote_put(host="192.168.8.121", username="admin", password="apj0702", filename="../conf/config_binola.xls", product_spreadsheet="products_binola", tote_spreadsheet="tote_flow",pick='1,4', put='2')
            
        