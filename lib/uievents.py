from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
import configparser
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
import selenium.webdriver.support.expected_conditions as EC
import selenium.webdriver.support.ui as ui
import time


class uiEvents:
    def __init__(self, serverIP, username="admin", password="apj0702", execution="remote"):

        if execution == "local":
            self.driver = webdriver.Chrome()
        else:
            try:
                self.driver = webdriver.Remote("http://192.168.8.80:4444/wd/hub",
                                               webdriver.DesiredCapabilities.CHROME.copy())
            except:
                print "Remote hub has some problem launching local browser"
                self.driver = webdriver.Chrome()
                self.launchBrowser(serverIP)
                self.fetchPOMDict()

    def launchBrowser(self, serverIP):
        self.driver.implicitly_wait(30)
        self.base_url = "https://%s/" % serverIP
        self.verificationErrors = []
        self.accept_next_alert = True
        self.host = serverIP
        self.driver.set_window_size(1800, 1200)
        self.driver.get(self.base_url + "/md_alpha")

    def fetchPOMDict(self):
        self.eleDict = self.parsePOM()

    def parsePOM(self, conf="dashboardPOM.cfg"):
        definitions = configparser.configparser("lib", conf).parseConfig()
        return definitions

    def inputText(self, ele, text):
        eleObj = self.eleDict[ele]
        self.driver.find_element_by_css_selector(eleObj).clear()
        self.driver.find_element_by_css_selector(eleObj).send_keys(text)

    def clickButton(self, ele):
        eleObj = self.driver.find_element_by_css_selector(self.eleDict[ele])
        eleObj.click()

    def hoverTo(self, ele):
        eleObj = self.driver.find_element_by_css_selector(self.eleDict[ele])
        hover = ActionChains(self.driver).move_to_element(eleObj)
        hover.perform()
        self.clickButton(ele)

    def submitButton(self, ele):
        eleObj = self.driver.find_element_by_css_selector(self.eleDict[ele])
        eleObj.submit()

    def acceptAlert(self, ele):
        eleObj = self.driver.find_element_by_css_selector(self.eleDict[ele])
        eleObj.click()

    def getAllTabsText(self, identifier):
        myDict = {}
        for item in self.eleDict.keys():
            if item.split(".")[0] == identifier:
                myDict[item] = self.driver.find_element_by_css_selector(self.eleDict[item]).text
        return myDict

    def getMultipleElementsText(self, identifier):
        myList = []
        findElements = self.driver.find_elements_by_css_selector(self.eleDict[identifier])
        for ele in findElements:
            myList.append(ele.text)
        return myList

    def getFirstFromMultipleEleText(self, identifier):
        myList = []
        findElements = self.driver.find_elements_by_css_selector(self.eleDict[identifier])
        for ele in findElements:
            myList.append(ele.text)
        return myList[0]

    def getCountOfElements(self, identifier):
        findElements = self.driver.find_elements_by_css_selector(self.eleDict[identifier])
        return len(findElements)

    def selectFromDropDown(self, identifier):
        counter = 1
        for operations in self.driver.find_elements_by_css_selector(".Dropdown-option span"):
            operations = operations.text
            if identifier in operations:
                print "##############"
                print "true"
                print "##############"
                self.driver.find_element_by_css_selector(".Dropdown-option:nth-child(" + str(counter) + ")").click()
                break
            else:
                counter = counter + 1

    def clickPpsBox(self, ppsOperation):
        counter = 1
        breakFlag = False
        for rows in self.driver.find_elements_by_css_selector(".public_fixedDataTable_main .fixedDataTableRowLayout_rowWrapper .public_fixedDataTable_bodyRow"):
            eleObj = self.driver.find_elements_by_css_selector(
                ".public_fixedDataTable_main .fixedDataTableRowLayout_rowWrapper:nth-child(" + str(
                    counter) + ") .public_fixedDataTableCell_cellContent")
            for pps in eleObj:
                pps = pps.text
                if pps == ppsOperation:
                    checkBox = self.driver.find_element_by_css_selector(".public_fixedDataTable_main .fixedDataTableRowLayout_rowWrapper:nth-child(" + str(counter)
                     + ") .public_fixedDataTableCell_cellContent [type = 'checkbox']")
                    self.driver.execute_script("arguments[0].click();", checkBox)
                    breakFlag = True
                    break
            if breakFlag:
                break
            else:
                counter = counter + 1

    def changePpsMode(self, ppsOperation, ppsOperationTo):
        counter = 1
        operationCounter = 1
        breakFlag = False
        for rows in self.driver.find_elements_by_css_selector(".public_fixedDataTable_main .fixedDataTableRowLayout_rowWrapper .public_fixedDataTable_bodyRow"):
            eleObj = self.driver.find_elements_by_css_selector(
                ".public_fixedDataTable_main .fixedDataTableRowLayout_rowWrapper:nth-child(" + str(
                    counter) + ") .public_fixedDataTableCell_cellContent")
            for pps in eleObj:
                pps = pps.text
                if pps == ppsOperation:
                    checkBox = self.driver.find_element_by_css_selector(".public_fixedDataTable_main .fixedDataTableRowLayout_rowWrapper:nth-child(" + str(counter)
                     + ") .public_fixedDataTableCell_cellContent [type = 'checkbox']")
                    self.driver.execute_script("arguments[0].click();", checkBox)
                    self.driver.find_element_by_css_selector(".Dropdown-arrow").click()
                    for operations in self.driver.find_elements_by_css_selector(".Dropdown-option span"):
                        operations = operations.text
                        operations = operations.lower()
                        if operations in ppsOperationTo:
                            self.driver.find_element_by_css_selector(".Dropdown-option:nth-child("+str(operationCounter)+")").click()
                            break
                        else:
                            operationCounter = operationCounter + 1
                    breakFlag = True
                    break
            if breakFlag:
                break
            else:
                counter = counter + 1

    def awaitForElementPresence(self, locator, timeout):
        try:
            ui.WebDriverWait(self.driver, timeout).until(
                EC.presence_of_element_located((By.CSS_SELECTOR, self.eleDict[locator])))
            return True
        except TimeoutException:
            return False

    def awaitForElementNotPresent(self, locator, timeout):
        try:
            ui.WebDriverWait(self.driver, timeout).until_not(
                EC.presence_of_element_located((By.CSS_SELECTOR, self.eleDict[locator])))
            return True
        except TimeoutException:
            return False

    def awaitForElementToBeVisible(self, locator, timeout):
        try:
            ui.WebDriverWait(self.driver, timeout).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, self.eleDict[locator])))
            return True
        except TimeoutException:
            return False

    def awaitForElementNotToBeVisible(self, locator, timeout):
        try:
            ui.WebDriverWait(self.driver, timeout).until_not(
                EC.visibility_of_element_located((By.CSS_SELECTOR, self.eleDict[locator])))
            return True
        except TimeoutException:
            return False

    def forceWait(self, timeout):
        time.sleep(timeout)