#! /usr/bin/python

# based on http://github.com/jensdepuydt

#this script requires ovirt-engine-sdk-python
import datetime, sys
from ovirtsdk.api import API
from ovirtsdk.xml import params
from time import sleep
from robot.api import logger as trace

class vmoperations:
    def __init__(self,host,host_user,host_pw):
        apiurl="https://"+host+"/api"
        self.api = API(url=apiurl,username=host_user,password=host_pw,insecure=True)
 
    def createGuest(self,VM_NAME,instanceType,templateName,clusterName):
     
        try:
            vm_list=self.api.vms.list()
            if VM_NAME == "auto":
                VM_NAME = "VM_%s"%('{:%d%b%Y_%H%M%S}'.format(datetime.datetime.now()))
            elif VM_NAME in [i.name for i in vm_list]:
                vm_ip = self.api.vms.get(VM_NAME).get_guest_info().get_ips().ip[0].get_address()
                trace.error('VM already exists, stopping the installation')
                sys.exit()
            self.api.vms.add(params.VM(name=VM_NAME, cluster=self.api.clusters.get(clusterName), 
                                  template=self.api.templates.get(templateName),
                                  disks=params.Disks(clone=True)))
            #self.api.vms.add(params.VM(name=VM_NAME, cluster=self.api.clusters.get(clusterName), 
            #                   template=self.api.templates.get(templateName)))
            
            print 'VM was created from Template successfully'
            trace.info('VM was created from Template successfully',also_console=True) 
            print 'Waiting for VM to reach Down status'
            trace.info('Waiting for VM to reach Down status',also_console=True) 
            while self.api.vms.get(VM_NAME).status.state != 'down':
                sleep(1)
            myVM = self.api.vms.get(VM_NAME)
            # admin_vm_manager_perm = params.Permission(role=self.api.roles.get('UserVmManager'),
            #                                         user=self.api.users.get('admin'))
            #myVM.permissions.add(admin_vm_manager_perm)
            
            #myVM.set_instance_type(params.InstanceType(name=instanceType))
            #myVM.set_console(params.GraphicsConsole(protocol="VNC"))
            myVM.set_delete_protected(False)
            myVM.update()
        except Exception as e:
            print 'Failed to create VM from Template:\n%s' % str(e)
            trace.error('Failed to create VM from Template:\n%s' % str(e)) 
        disk_name="%s_Disk1"%"debian-8-basic"
        print "Waiting for "+disk_name+" to reach ok status"
        trace.info('Waiting for %s to reach ok status' % disk_name,also_console=True) 
        while myVM.disks.get(name=disk_name).status.state != 'ok':
            sleep(1)
        while not self.powerOnGuest(VM_NAME):
            sleep(1)
        sleep(30)
        timeOut = 300
        while timeOut>0:
            try:
                vm_ip = self.api.vms.get(VM_NAME).get_guest_info().get_ips().ip[0].get_address()
                break
            except:
                print "Waiting for VM to come up sleeping for 30 seconds"
                sleep(30)
                timeOut -= 30
        print  "Succesfully created guest:%s with IP:%s "%(VM_NAME,vm_ip)
        trace.info("Succesfully created guest:%s with IP:%s "%(VM_NAME,vm_ip),also_console=True) 
        return vm_ip,VM_NAME

    def powerOnGuest(self,guest_name):
        try:
            if self.api.vms.get(guest_name).status.state != 'up':
                print 'Starting VM'
                trace.info('Starting VM',also_console=True) 
                self.api.vms.get(guest_name).start()
                print 'Waiting for VM to reach Up status'
                trace.info('Waiting for VM to reach Up status',also_console=True) 
                while self.api.vms.get(guest_name).status.state != 'up':
                    sleep(1)
            else:
                print 'VM already up'
                trace.info('VM already up',also_console=True) 
            return True
               
        except Exception as e:
            print 'Failed to Start VM:\n%s' % str(e)
            
            trace.error('Failed to start VM :\n%s' % str(e))
            return False
            
    def powerOffGuest(self,guest_name):
        try:
            if self.api.vms.get(guest_name).status.state != 'down':
                print 'Stoping VM'
                #trace.info('Stoping VM',also_console=True) 
                self.api.vms.get(guest_name).stop()
                print 'Waiting for VM to reach Up status'
                #trace.info('Waiting for VM to reach Up status',also_console=True) 
                while self.api.vms.get(guest_name).status.state != 'down':
                    sleep(1)
            else:
                print 'VM already down'
                #trace.info('VM already down',also_console=True) 
                
        except Exception as e:
            print 'Failed to Stop VM:\n%s' % str(e)
            #trace.error('Failed to stop VM :\n%s' % str(e))
    
    def removeVM(self,VM_NAME):
        self.powerOffGuest(VM_NAME)
        try:
            myVM = self.api.vms.get(VM_NAME)
            myVM.set_delete_protected(False)
            myVM.update()
            myVM.delete()
            print '%s VM was removed successfully'%VM_NAME
            #trace.info('%s VM was removed successfully'%VM_NAME,also_console=True) 
            print 'Waiting for VM "%s" to be deleted'%VM_NAME
            ##trace.info('Waiting for VM "%s" to be deleted'%VM_NAME,also_console=True) 
            while VM_NAME in [vm.name for vm in self.api.vms.list()]:
                sleep(1)
            return True
        except Exception as e:
            print 'Failed to remove VM:\n%s' % str(e)
            #trace.error('Failed to remove VM :\n%s' % str(e))
            return False
        return False
        
