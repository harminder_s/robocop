#!/usr/bin/python
import sys, re, subprocess, datetime
from time import sleep
from pysphere import MORTypes, VIServer, VITask, VIProperty, VIMor, VIException
from pysphere.vi_virtual_machine import VIVirtualMachine

class vmwareops:
    def __init__(self,server= "192.168.8.227",username="administrator@vsphere.local",password= "Grey@123"):
        
        self.verbose=  True
        
        self.resource_pool     = "/Resources"
        self.verbose        = True
        self.maxwait     = 120
        
        self.print_verbose('Connecting to server %s with username %s' % (server,username))
        self.con = VIServer()
        self.con.connect(server,username,password)
        self.print_verbose('Connected to server %s' % server)
        self.print_verbose('Server type: %s' % self.con.get_server_type())
        self.print_verbose('API version: %s' % self.con.get_api_version())
        
    def print_verbose(self,message):
        if self.verbose:
            print message
    
    def find_vm(self,name):
        try:
            vm = self.con.get_vm_by_name(name)
            return vm
        except VIException:
            return None
    
    def find_resource_pool(self,name):
        rps = self.con.get_resource_pools()
        for mor, path in rps.iteritems():
            self.print_verbose('Parsing RP %s' % path)
            if re.match('.*%s' % name,path):
                return mor
        return None

    def find_ip(self,vm):
        net_info = None
        waitcount = 0
        while net_info is None:
            if waitcount > self.maxwait:
                break
            net_info = vm.get_property('net',False)
            self.print_verbose('Waiting 5 seconds ...')
            waitcount += 5
            sleep(5)
        if net_info:
            for ip in net_info[0]['ip_addresses']:
                if re.match('\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}',ip) and ip != '127.0.0.1':
                    self.print_verbose('IPv4 address found: %s' % ip)
                    return ip
        self.print_verbose('Timeout expired: No IP address found')
        return None

    def createvm(self,vm_name="auto",template = "vmtemplate"):
        if vm_name == "auto":
            vm_name = "VM_%s"%('{:%d%b%Y_%H%M%S}'.format(datetime.datetime.now()))
        elif self.find_vm(vm_name):
            print 'ERROR: %s already exists' % vm_name
            sys.exit()
        self.print_verbose('Finding template %s' % template)
        template_vm = self.find_vm(template)
        if template_vm is None:
            print 'ERROR: %s not found' % template
            sys.exit(1)
        self.print_verbose('Template %s found' % template)
    
        # Verify the target Resource Pool exists
        self.print_verbose('Finding resource pool %s' % self.resource_pool)
        resource_pool_mor = self.find_resource_pool(self.resource_pool)
        if resource_pool_mor is None:
            print 'ERROR: %s not found' % self.resource_pool
            sys.exit(1)
        self.print_verbose('Resource pool %s found' % self.resource_pool)
        self.print_verbose('Trying to clone %s to VM %s' % (template,vm_name))
        clone = template_vm.clone(vm_name, True, None, resource_pool_mor, None, None, False)
        self.print_verbose('VM %s created' % vm_name)
            
        self.print_verbose('Booting VM %s' % vm_name)
        clone.power_on()
         
        vm = self.find_vm(vm_name)
        ip = self.find_ip(vm)
        return ip,vm_name

    def __del__(self):
        self.con.disconnect()
        
if __name__ == "__main__":
    vmobj = vmwareops("192.168.8.227","administrator@vsphere.local","Grey@123")
    vmobj.createvm()
        