import os, sys


try:
    import xml.etree.ElementTree as etree
except:
    import elementtree.ElementTree as etree

class parseXML:
    def __init__(self,xmlPath):
        tree = etree.parse(xmlPath)
        root = tree.getroot()
        xml_namespace = "{http://www.guavus.com/GMS/}"
        self.dict = self.xml_to_dict({},root,xml_namespace)
        
    def xml_to_dict(self,d, el, xml_namespace):
        children = el.getchildren()
        el.tag = el.tag.replace(xml_namespace,"")
        if children:
            for child in children:
                child.tag = child.tag.replace(xml_namespace,"")
                if not d.has_key(child.tag):
                    d[child.tag] = []
                if (child.getchildren()):
                    d1 = {}
                    self.xml_to_dict(d1, child, xml_namespace)
                    d[child.tag].append(d1)
                else:
                    if (child.text):
                        text = child.text.strip(" \t\n\r")
                        if len(text) != 0:
                            d[child.tag].append(child.text)
        return d
    
    
    def add_to_dict(self,res_dict,key,value):    
           if res_dict.has_key(key):
               v = res_dict[key]
               v = v+","+value
               res_dict[key] = v
           else:
               res_dict[key] =value
               
    def clusterToAppMapping(self,):
        d = self.dict
        appDict ={}
        for app in range(len(d["Applications"][0]['Application'])):
            appType = d["Applications"][0]['Application'][app]["Application_Name"][0]
            
            for appClu in d["Applications"][0]['Application'][app]["Clusters"][0]['Cluster']:
                self.add_to_dict(appDict,appClu['Cluster_Name'][0],appType)
        return appDict
    
    def clusterVIP(self):   
        d = self.dict 
        vipDict = {}
        for clu in range(len(d["Clusters"][0]['Cluster'])):
            cluType = d["Clusters"][0]['Cluster'][clu]["Cluster_Name"][0]
            if d["Clusters"][0]['Cluster'][clu]["Application_Type"][0].find("HA") != -1:
                for cluVIP in d["Clusters"][0]['Cluster'][clu]['Cluster_VIPs']:
                    self.add_to_dict(vipDict,cluType,cluVIP['Cluster_VIP'][0])
            else:
                self.add_to_dict(vipDict,cluType,"None")        
        return vipDict
    
    def nodeName(self):
        d = self.dict
        nodeDict = {}
        for nod in range(len(d["Nodes"][0]['Node'])):
            hostName = d["Nodes"][0]['Node'][nod]["Node_Hostname"][0]
            chassisName = d["Nodes"][0]['Node'][nod]["Chassis_LogicalName"][0]
            slotNumber = d["Nodes"][0]['Node'][nod]["Slot_Number"][0]
            self.add_to_dict(nodeDict,chassisName+"#"+slotNumber,hostName)
           
        return nodeDict
    
    def isMGMTInterface(self,network):
        d = self.dict
        for clu in range(len(d["Networks"][0]['Network'])):
            if d["Networks"][0]['Network'][clu]['Network_Name'][0] == network:
                if (d["Networks"][0]['Network'][clu]["IsManagement"][0] == 'Mgmt') or (d["Networks"][0]['Network'][clu]["IsManagement"][0] == 'Mgmt/Hadoop'):
                    return True
        return False
           
    def nodeMGMTNetwork(self):
        d = self.dict
        nodeDict = {}
        for nod in range(len(d["Nodes"][0]['Node'])):
            hostName = d["Nodes"][0]['Node'][nod]["Node_Hostname"][0]
            for nodeMem in d["Nodes"][0]['Node'][nod]['Node_IPdetails']:
                if self.isMGMTInterface(nodeMem["Network_Name"][0]):
                    self.add_to_dict(nodeDict,hostName,nodeMem["Network_IP"][0]+" /"+nodeMem["Network_Subnet"][0])              
        return nodeDict
    
    def nodeNetwork(self):
        d = self.dict
        nodeDict = {}
        for nod in range(len(d["Nodes"][0]['Node'])):
            hostName = d["Nodes"][0]['Node'][nod]["Node_Hostname"][0]
            for nodeMem in d["Nodes"][0]['Node'][nod]['Node_IPdetails']:
                self.add_to_dict(nodeDict,hostName,nodeMem["Network_IP"][0]+" /"+nodeMem["Network_Subnet"][0])              
        return nodeDict
    
    def nodeMAC(self):
        d = self.dict
        nodeDict = {}
        for nod in range(len(d["Server_Details"][0]['Chassis'])):
            for nodeMem in range(len(d["Server_Details"][0]['Chassis'][nod]["Slot"])):
                hostname =  d["Server_Details"][0]['Chassis'][nod]["Slot"][nodeMem]['Host_Name'][0]
                for ifc in d["Server_Details"][0]['Chassis'][nod]["Slot"][nodeMem]['Ifc_Member']:
                    self.add_to_dict(nodeDict,hostname,ifc['Mac_Address'][0])     
        return nodeDict
    
    def clusterMembers(self):    
        d = self.dict
        membersDict = {}
        nodeDict = self.nodeName()
        for clu in range(len(d["Clusters"][0]['Cluster'])):
            cluType = d["Clusters"][0]['Cluster'][clu]["Cluster_Name"][0]    
            for cluMem in d["Clusters"][0]['Cluster'][clu]['MemberNodes']:
                self.add_to_dict(membersDict,cluType,nodeDict[cluMem['Chassis_LogicalName'][0]+"#"+cluMem['Slot_Number'][0]])       
        return membersDict
        
    def xmlDict(self):
        d = self.dict
        cluAppDict = self.clusterToAppMapping()
        cluVIPDict = self.clusterVIP()
        cluMembersDict = self.clusterMembers()
        nodeMACDict = self.nodeMAC()
        nodeMGMTNet = self.nodeMGMTNetwork()
        nodeNetDict = self.nodeNetwork()
        if type(d) is dict:
            cluster_details = {}
            for items in d.keys():
                if items == "Clusters":                
                    cluster_list = d[items][0]["Cluster"]  
                    for i in cluster_list: 
                        clusterName = i["Cluster_Name"][0]
                        netDict = {}
                        mgmtnetDict = {}
                        macDict ={}
                        nodeDict ={}
                        for node in cluMembersDict[clusterName].split(","):
                            nodeDict.setdefault(node, {})["net"] = nodeNetDict[node].split(",")
                            nodeDict.setdefault(node, {})["mgmt"] = nodeMGMTNet[node].split(",")
                            nodeDict.setdefault(node, {})["mac"] = nodeMACDict[node].split(",")
                        #print nodeDict
                        cluster_details.setdefault(clusterName, {})["app"]=cluAppDict[clusterName].split(",")
                        cluster_details.setdefault(clusterName, {})["vip"]=cluVIPDict[clusterName]
                        cluster_details.setdefault(clusterName, {})["members"]=nodeDict
            return cluster_details
   

if __name__ == "__main__":    
    outDict = parseXML("/Users/abhishek.sharma/Documents/workspace1/test/GAuto_v2/conf/tmo-thirty-Aug19.xml").xmlDict()
    print outDict
    for key in outDict.keys():
        print key, "->",outDict[key]["app"], "-> ", outDict[key]["members"].keys()


        
        