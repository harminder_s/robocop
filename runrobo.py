import sys, os, time, re
import robot
from robot import run
#from robot.libraries.BuiltIn import BuiltIn

def getAllPaths():    
    for i in range(20):
        pth =  os.getcwd()
        if os.path.basename(pth).find("robocop") != -1:
            conf_pth = "%s/conf"%pth        
            sys.path.append(conf_pth)
            lib_pth = "%s/lib"%pth       
            sys.path.append(lib_pth)
            extra_pth = "%s/extra"%pth        
            sys.path.append(extra_pth)
            result_pth = "%s/results"%pth        
            sys.path.append(result_pth)
            tmp_pth = "%s/tmp"%pth        
            sys.path.append(tmp_pth)
            return conf_pth,lib_pth,extra_pth,result_pth,tmp_pth
        elif i==9:
            print ("Not able to get conf dir in goal, exiting....")
            sys.exit()
        else:
            print ("Retrying: %d times left"%(10-i))
            os.chdir("..")
    return False
debug = False
conf_pth,lib_pth,extra_pth,result_pth,tmp_pth = getAllPaths()
if len(sys.argv) > 1:
    configFile = False
    argList = sys.argv[1:]
    for arg in argList:
        m = re.search("^\s*config\s*=\s*(?P<filename>\S*)\s*$",arg)
        if m:
            configFile =  m.group("filename")
            argList.remove(arg)
    
    if "debug" in argList:
        debug = True
        argList.remove("debug")
    
    mail = "no"
    if "mail" in argList:
        mail = "yes"
        
    build = "no"
    if "build" in argList:
        build = "yes"
        
    upload = "no"
    if "upload" in argList:
        upload = "yes"
            
    if not configFile:
        print "No config file provided, using '%s/main.cfg'"%(conf_pth)
        configFile = "main.cfg"
    if len(argList) > 0:    
    
        testNamewithPath = argList[0]
        testName = os.path.basename(testNamewithPath)
        if len(testName.strip()) == 0:
            testName = os.path.basename(testNamewithPath.strip("/"))
            
        outputDir = "%s/%s_%s"%(result_pth,testName,time.strftime("%Y%m%d_%H%M%S",time.gmtime(time.time())))    

    else:
        print "Please provide test suite/case name with path"
        sys.exit()
        
    if debug:
        run(testNamewithPath,loglevel="DEBUG:INFO",debugfile= "debug.txt",outputdir= outputDir,listener ="loadconfig:%s:%s:%s:%s:%s:%s:%s:%s:%s"%(conf_pth,configFile,lib_pth,extra_pth,result_pth,tmp_pth,mail,build,upload))
    else: 
        run(testNamewithPath,loglevel="INFO:WARN",outputdir= outputDir,listener ="loadconfig:%s:%s:%s:%s:%s:%s:%s:%s:%s"%(conf_pth,configFile,lib_pth,extra_pth,result_pth,tmp_pth,mail,build,upload))#, include=['tag1', 'tag2'])
else:
    print "Please provide test suite/case name with path"
