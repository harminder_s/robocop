import schedule
import time, os

def job():
    os.system("python runrobo.py suites/SmokeTest config=smoke.cfg debug mail build upload>logs")


schedule.every().day.at("00:00").do(job)


while True:
    schedule.run_pending()
    time.sleep(1)
