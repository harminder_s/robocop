***Settings ***
Library	drawers.py

*** Test Cases ***
Add Single Rack
	[Documentation]    Add single Rack at a specific Location
	
	add_single_rack	${drawers.host}	${singleRack.rack_id}	${singleRack.rack_face}	${singleRack.rack_location}	${singleRack.rack_type}	ssh_username=${drawers.ssh_username}	password=${drawers.password}	