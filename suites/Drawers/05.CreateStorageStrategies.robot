***Settings ***
Library	drawers.py

*** Test Cases ***
Create Storage Strategies
	[Documentation]    Creating Storage Strategies and Storage tags by reading from csv file
	
	create_storage_strategy		${drawers.host}	${storageStrategy.storageStrategy_source_path}	${storageStrategy.storageStrategy_destination_path}		username=${drawers.user}	ssh_username=${drawers.ssh_username}	password=${drawers.password}	