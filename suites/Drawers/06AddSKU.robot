***Settings ***
Library	apiHelper.py
Library	drawers.py
Library	Collections	

*** Test Cases ***
Bulk Add Sku
	[Documentation]    Add Bulk Sku by reading from the spreadsheet
	
	add_bulk_sku	${drawers.host}	username=${drawers.user}	password=${drawers.password}	filename=${AddSKU.configfile}	spreadsheet=${AddSKU.sku_spreadsheet}	item_tags=${AddSKU.sku_tags}
	