***Settings ***
Library	apiHelper.py
Library	Collections	

*** Test Cases ***
Bulk Add Products
	[Documentation]    Add Bulk Products by reading from the spreadsheet
	
	add_bulk_product	${drawers.host}	username=${drawers.user}	password=${drawers.password}	filename=${AddProducts.configfile}	spreadsheet=${AddProducts.product_spreadsheet}	item_tags=${AddProducts.product_tags}
