***Settings ***
Library	apiHelper.py
Library	drawers.py
Library	Collections

*** Test Cases ***	

Change PPS Mode
	[Documentation]	Changes the PPS Mode.
    ...                The keyword changes the PPS mode as specified in the config file.
    ${host}=	Get Variable Value	${vm_ip}	${drawers.host}
    alignPpsMode	${host}	${drawers.user}	${drawers.password}	${drawers.put}	${drawers.pick}	${drawers.audit}
	${outMsg}=	Catenate	Modes requested as, Put:  ${drawers.put}    Pick:    ${drawers.pick}    Audit:   ${drawers.audit} 
    Pass Execution	${outMsg} 
	
	
	
Get PPS ID and Mode
	[Documentation]	Get all PPS ID and Mode.
    ...                The keyword returns a dictionary with "put","pick" and "audit" as keys and their values as list.
    ${host}=	Get Variable Value	${vm_ip}	${drawers.host}
	${PpsDict}=	getPpsIdMode	${host}	${drawers.user}	${drawers.password}
	@{put}=	Get From Dictionary	${PpsDict}	put
	@{pick}=	Get From Dictionary	${PpsDict}	pick
	
	${ppsput}=	Get From List	${put}	0
	${ppspick}=	Get From List	${pick}	0
	
	Set Suite Variable	${ppsput}
	Set Suite Variable	${ppspick}
	${outMsg}=	Catenate	Modes, Put	${ppsput}	Pick:	${ppspick} 
    Pass Execution	${outMsg}
    

Put Product one by one
	[Documentation]    Put Product one by one
	delete_all_racks	${drawers.host}	ssh_username=${drawers.ssh_username}	password=${drawers.password}
	add_single_rack	${drawers.host}	${singleRack.rack_id}	${singleRack.rack_face}	${singleRack.rack_location}	${singleRack.rack_type}	ssh_username=${drawers.ssh_username}	password=${drawers.password}
	set_Stacking	${drawers.host}	ssh_username=${drawers.ssh_username}	password=${drawers.password}	slot_type=drawer	length=true	breadth=true	height=false 
	put_back_operations	${drawers.host}	${drawers.user}	${drawers.password}	barcodes=Gravity1,Gravity2,Gravity3,Gravity4,Gravity5,Gravity6,Gravity7,Gravity8	qty=${1}	ppsid=${ppsput}
	put_front_Operations	${drawers.host}	${drawers.user}	${drawers.password}	ppsid=${ppsput}


Pick two products 
	[Documentation]    Pick two products
	add_single_order	${drawers.host}	${drawers.user}	${drawers.password}	pdfavalues=product_sku:Gravity1	qty=${1}
	add_single_order	${drawers.host}	${drawers.user}	${drawers.password}	pdfavalues=product_sku:Gravity4	qty=${1}
	pick_front_process	${drawers.host}	${drawers.user}	${drawers.password}	${ppspick}	
	pick_back_process	${drawers.host}	${drawers.user}	${drawers.password}	${ppspick}

	
Put product to check the Gravity	
	[Documentation]    put product to check Gravity
	put_back_operations	${drawers.host}	${drawers.user}	${drawers.password}	barcodes=Gravity21	qty=${1}	ppsid=${ppsput}
	put_front_operations	${drawers.host}	${drawers.user}	${drawers.password}	ppsid=${ppsput}
	

Check negative case for gravity
	[Documentation]    put product to check negative case for Gravity
	put_back_operations	${drawers.host}	${drawers.user}	${drawers.password}	barcodes=Gravity2	qty=${1}	ppsid=${ppsput}
	Sleep	5
	${output}=	getErrorPpsBin	${drawers.host}	${drawers.user}	${drawers.password}	ppsid=2
	${output_dictionary}=	Get Dictionary Values	${output}
	${output_val}=	Get From List	${output_dictionary}	0
	${value}=	Create Dictionary	Gravity2=${1}	
	Log	${value}
    Log	 ${output_val}
    Dictionaries Should Be Equal    ${output_val}    ${value}
	clearAllErrorPpsBin	${drawers.host}	${drawers.user}	${drawers.password}	ppsid=${ppsput}

	
	
	
	
	