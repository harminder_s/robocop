***Settings ***
Library	drawers.py
Library	apiHelper.py
Library	bulkPut.py
Library	Collections

*** Test Cases ***

Change PPS Mode
	[Documentation]	Changes the PPS Mode.
    ...                The keyword changes the PPS mode as specified in the config file.
    ${host}=	Get Variable Value	${vm_ip}	${drawers.host}
    alignPpsMode	${host}	${drawers.user}	${drawers.password}	${drawers.put}	${drawers.pick}	${drawers.audit}
	${outMsg}=	Catenate	Modes requested as, Put:  ${drawers.put}    Pick:    ${drawers.pick}    Audit:   ${drawers.audit} 
    Pass Execution	${outMsg} 
	
	
	
Get PPS ID and Mode
	[Documentation]	Get all PPS ID and Mode.
    ...                The keyword returns a dictionary with "put","pick" and "audit" as keys and their values as list.
    ${host}=	Get Variable Value	${vm_ip}	${drawers.host}
	${PpsDict}=	getPpsIdMode	${host}	${drawers.user}	${drawers.password}
	@{put}=	Get From Dictionary	${PpsDict}	put
	@{pick}=	Get From Dictionary	${PpsDict}	pick
	
	${ppsput}=	Get From List	${put}	0
	${ppspick}=	Get From List	${pick}	0
	
	Set Suite Variable	${ppsput}
	Set Suite Variable	${ppspick}
	${outMsg}=	Catenate	Modes, Put	${ppsput}	Pick:	${ppspick} 
    Pass Execution	${outMsg}
    
  

when stacking is TTF against all the directions,Verify Stacking in same putaway
	[Documentation]    Verify stacking with True,True,False
	delete_all_racks	${drawers.host}	ssh_username=${drawers.ssh_username}	password=${drawers.password}
	add_single_rack	${drawers.host}	${singleRack.rack_id}	${singleRack.rack_face}	${singleRack.rack_location}	${singleRack.rack_type}	ssh_username=${drawers.ssh_username}	password=${drawers.password}
	set_Stacking	${drawers.host}	ssh_username=${drawers.ssh_username}	password=${drawers.password}	slot_type=drawer	length=true	breadth=true	height=false 
	put_back_operations	${drawers.host}	${drawers.user}	${drawers.password}	barcodes=Stacking_TTF	qty=${24}	ppsid=${ppsput}
	put_front_operations	${drawers.host}	${drawers.user}	${drawers.password}	ppsid=${ppsput}
	${output}=	getErrorPpsBin	${drawers.host}	${drawers.user}	${drawers.password}	ppsid=2
	${output_dictionary}=	Get Dictionary Values	${output}
	${output_val}=	Get From List	${output_dictionary}	0
	${value}=	Create Dictionary	Stacking_TTF=${8}	
	Log	${value}
    Log	 ${output_val}
    Dictionaries Should Be Equal    ${output_val}    ${value}
	clearAllErrorPpsBin	${drawers.host}	${drawers.user}	${drawers.password}	ppsid=${ppsput}


when stacking is TTT against all the directions,Verify Stacking in same putaway
	[Documentation]    Verify stacking with True,True,True
	delete_all_racks	${drawers.host}	ssh_username=${drawers.ssh_username}	password=${drawers.password}
	add_single_rack	${drawers.host}	${singleRack.rack_id}	${singleRack.rack_face}	${singleRack.rack_location}	${singleRack.rack_type}	ssh_username=${drawers.ssh_username}	password=${drawers.password}
	set_Stacking	${drawers.host}	ssh_username=${drawers.ssh_username}	password=${drawers.password}	slot_type=drawer	length=true	breadth=true	height=true 
	put_back_operations	${drawers.host}	${drawers.user}	${drawers.password}	barcodes=Stacking_TTT	qty=${24}	ppsid=${ppsput}
	put_front_operations	${drawers.host}	${drawers.user}	${drawers.password}	ppsid=${ppsput}
	${output}=	getErrorPpsBin	${drawers.host}	${drawers.user}	${drawers.password}	ppsid=${ppsput}
	${value} =	Set Variable	{}	
    Should be equal as strings    ${output}    ${value}
    Sleep	5
	clearAllErrorPpsBin	${drawers.host}	${drawers.user}	${drawers.password}	ppsid=${ppsput}

when stacking is TTT against all the directions,Verify Stacking in different putaway
	[Documentation]
	delete_all_racks	${drawers.host}	ssh_username=${drawers.ssh_username}	password=${drawers.password}
	add_single_rack	${drawers.host}	${singleRack.rack_id}	${singleRack.rack_face}	${singleRack.rack_location}	${singleRack.rack_type}	ssh_username=${drawers.ssh_username}	password=${drawers.password}	
    bulk_put_operations	${drawers.host}	username=${drawers.user}	password=${drawers.password}	filename=${drawers.configfile}	spreadsheet=Stacking_Drawers	pick=${ppsput}	put=${ppsput}	item_tags=all
	Sleep	5
	${output}=	getErrorPpsBin	${drawers.host}	${drawers.user}	${drawers.password}	ppsid=${ppsput}
	Should Not Be Empty	${output}
	clearAllErrorPpsBin	${drawers.host}	${drawers.user}	${drawers.password}	ppsid=${ppsput}
 

    
    
	
	
	
	
	