***Settings ***
Library	drawers.py
Library	Collections
Library	apiHelper.py

*** Test Cases ***

Change PPS Mode
	[Documentation]	Changes the PPS Mode.
    ...                The keyword changes the PPS mode as specified in the config file.
    ${host}=	Get Variable Value	${vm_ip}	${drawers.host}
    alignPpsMode	${host}	${drawers.user}	${drawers.password}	${drawers.put}	${drawers.pick}	${drawers.audit}
	${outMsg}=	Catenate	Modes requested as, Put:  ${drawers.put}    Pick:    ${drawers.pick}    Audit:   ${drawers.audit} 
    Pass Execution	${outMsg} 
	
	

Get PPS ID and Mode
	[Documentation]	Get all PPS ID and Mode.
    ...                The keyword returns a dictionary with "put","pick" and "audit" as keys and their values as list.
    ${host}=	Get Variable Value	${vm_ip}	${drawers.host}
	${PpsDict}=	getPpsIdMode	${host}	${drawers.user}	${drawers.password}
	@{put}=	Get From Dictionary	${PpsDict}	put
	@{pick}=	Get From Dictionary	${PpsDict}	pick
	
	${ppsput}=	Get From List	${put}	0
	${ppspick}=	Get From List	${pick}	0
	
	Set Suite Variable	${ppsput}
	Set Suite Variable	${ppspick}
	${outMsg}=	Catenate	Modes, Put	${ppsput}	Pick:	${ppspick} 
    Pass Execution	${outMsg}

  
Verify Drawer visibility:negative case
	[Documentation]		Checking Drawer visibility for invalid product
	
	delete_all_racks	${drawers.host}	ssh_username=${drawers.ssh_username}	password=${drawers.password}
	add_single_rack	${drawers.host}	${singleRack.rack_id}	${singleRack.rack_face}	${singleRack.rack_location}	${singleRack.rack_type}	ssh_username=${drawers.ssh_username}	password=${drawers.password}
	set_Stacking	${drawers.host}	ssh_username=${drawers.ssh_username}	password=${drawers.password}	slot_type=drawer	length=true	breadth=true	height=false 
	put_back_operations	${drawers.host}	${drawers.user}	${drawers.password}	barcodes=Visible1	qty=${400}	ppsid=${ppsput}
 	put_front_operations	${drawers.host}	${drawers.user}	${drawers.password}	ppsid=${ppsput}
 	put_back_operations	${drawers.host}	${drawers.user}	${drawers.password}	barcodes=Visible3	qty=${1}	ppsid=${ppsput}
 	sleep	5
 	${output}=	getErrorPpsBin	${drawers.host}	${drawers.user}	${drawers.password}	ppsid=${ppsput}
	${output_dictionary}=	Get Dictionary Values	${output}
	${output_val}=	Get From List	${output_dictionary}	0
	${value}=	Create Dictionary	Visible3=${1}	
	Log	${value}
    Log	 ${output_val}
    Dictionaries Should Be Equal    ${output_val}    ${value}
	clearAllErrorPpsBin	${drawers.host}	${drawers.user}	${drawers.password}	ppsid=${ppsput}
	
	

Verify Drawer visibility:positive case
	[Documentation]		Checking Drawer visibility for invalid product
    ${output}=	put_back_operations	${drawers.host}	${drawers.user}	${drawers.password}	barcodes=Visible2	qty=${1}	ppsid=${ppsput}
	put_front_operations	${drawers.host}	${drawers.user}	${drawers.password}	ppsid=${ppsput}
	Sleep	5
 	Log	${output}
	Run Keyword if	${output}=='False'	Fail	"Item could not be placed in bin"