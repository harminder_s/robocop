***Settings ***
Library	drawers.py
Library	apiHelper.py
Library	Collections


*** Test Cases ***

Change PPS Mode
	[Documentation]	Changes the PPS Mode.
    ...                The keyword changes the PPS mode as specified in the config file.
    ${host}=	Get Variable Value	${vm_ip}	${drawers.host}
    alignPpsMode	${host}	${drawers.user}	${drawers.password}	${drawers.put}	${drawers.pick}	${drawers.audit}
	${outMsg}=	Catenate	Modes requested as, Put:  ${drawers.put}    Pick:    ${drawers.pick}    Audit:   ${drawers.audit} 
    Pass Execution	${outMsg} 
	
	
	
Get PPS ID and Mode
	[Documentation]	Get all PPS ID and Mode.
    ...                The keyword returns a dictionary with "put","pick" and "audit" as keys and their values as list.
    ${host}=	Get Variable Value	${vm_ip}	${drawers.host}
	${PpsDict}=	getPpsIdMode	${host}	${drawers.user}	${drawers.password}
	@{put}=	Get From Dictionary	${PpsDict}	put
	@{pick}=	Get From Dictionary	${PpsDict}	pick
	
	${ppsput}=	Get From List	${put}	0
	${ppspick}=	Get From List	${pick}	0
	
	Set Suite Variable	${ppsput}
	Set Suite Variable	${ppspick}
	${outMsg}=	Catenate	Modes, Put	${ppsput}	Pick:	${ppspick} 
    Pass Execution	${outMsg}
  
 
Setting stacking vector
	[Documentation]    Set the stacking vector for storage node
	delete_all_racks	${drawers.host}	ssh_username=${drawers.ssh_username}	password=${drawers.password}
	add_single_rack	${drawers.host}	${singleRack.rack_id}	${singleRack.rack_face}	${singleRack.rack_location}	${singleRack.rack_type}	ssh_username=${drawers.ssh_username}	password=${drawers.password}
	set_Stacking	${drawers.host}	ssh_username=${drawers.ssh_username}	password=${drawers.password}	slot_type=drawer	length=true	breadth=true	height=false

Put products at drawer corners	
	[Documentation]    Put the products at all the corners as per storage node calculation
	put_back_operations	${drawers.host}	${drawers.user}	${drawers.password}	barcodes=SN_Corners1,SN_Corners2,SN_Corners3,SN_Corners4,SN_Corners5,SN_Corners6,SN_Corners7,SN_Corners8	qty=${1}	ppsid=${ppsput}
	put_front_operations	${drawers.host}	${drawers.user}	${drawers.password}	ppsid=${ppsput}

put product to check Storage node corners	
	[Documentation]	To check negative case for storage node corner
	put_back_operations	${drawers.host}	${drawers.user}	${drawers.password}	barcodes=SN_Corners_Negative	qty=${1}	ppsid=${ppsput}
	Sleep	5
	${output}=	getErrorPpsBin	${drawers.host}	${drawers.user}	${drawers.password}	ppsid=2
	${output_dictionary}=	Get Dictionary Values	${output}
	${output_val}=	Get From List	${output_dictionary}	0
	${value}=	Create Dictionary	SN_Corners_Negative=${1}	
	Log	${value}
    Log	 ${output_val}
    Dictionaries Should Be Equal    ${output_val}    ${value}
	clearAllErrorPpsBin	${drawers.host}	${drawers.user}	${drawers.password}	ppsid=${ppsput}

 	
 	
 	
	
	
	
	
	
	
	