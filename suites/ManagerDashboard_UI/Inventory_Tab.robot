*** Settings ***
Library    inventory_md.py    192.168.9.    admin    apj0702
Library    Collections

*** Test Cases ***
Verify Today's Put stock snapshot on Inventory screen
    [Documentation]    Verify Put stock gets updated on Inventory tab
    fetchActualStock    serverIP=${sanity.host}    username=${sanity.user}    password=${sanity.password}    execution=${sanity.execution}
    verifyInventoryPutStock    serverIP=${sanity.host}    username=${sanity.user}    password=${sanity.password}    execution=${sanity.execution}    barcodes=${sanity.barcode}    qty=${sanity.put_qty}    pps_put_id=${sanity.put}
    closeInventoryEvent

Verify Today's Pick stock snapshot on Inventory screen
    [Documentation]    Verify Pick stock gets updated on Inventory tab
    fetchActualStock    serverIP=${sanity.host}    username=${sanity.user}    password=${sanity.password}    execution=${sanity.execution}
    verifyInventoryPickStock    serverIP=${sanity.host}    username=${sanity.user}    password=${sanity.password}    execution=${sanity.execution}    pps_pick_id=${sanity.pick}    pdfaValues=${sanity.pdfa_value}
    closeInventoryEvent