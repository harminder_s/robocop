*** Settings ***
Library    dashboard.py    192.168.9.    admin    apj0702    remote
Library    Collections


*** Test Cases ***
Login to Manager Dashboard with valid credentials
    [Documentation]    Verify that user is able to login to MD with valid credentials
    openBrowser    serverIP=${sanity.host}    username=${sanity.user}    password=${sanity.password}    execution=${sanity.execution}
    loginDashboard
    closeBrowser

Verify all tabs are clickable
    [Documentation]    Verify that user is able to click all tabs on MD
    openBrowser    serverIP=${sanity.host}    username=${sanity.user}    password=${sanity.password}    execution=${sanity.execution}
    loginDashboard
    verifyAllTabs
    closeBrowser