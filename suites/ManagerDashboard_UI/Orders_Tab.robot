*** Settings ***
Library    order.py    192.168.9.    admin    apj0702
Library    Collections

*** Test Cases ***
Verify elements are visible on Order List screen
    [Documentation]    Verify elements and details on Order List Sub tab
    verifyOrderListSubTab    serverIP=${sanity.host}    username=${sanity.user}    password=${sanity.password}    execution=${sanity.execution}
    closeOrderEvent

Verify put and picking/processing of a Completed order on Order List screen
    [Documentation]    Verify processing of a Completed order on Order List Sub tab
    verifyCompletedOrder    serverIP=${sanity.host}    username=${sanity.user}    password=${sanity.password}    execution=${sanity.execution}    barcodes=${sanity.barcode}    qty=${sanity.put_qty}    pps_put_id=${sanity.put}    pps_pick_id=${sanity.pick}    pdfaValues=${sanity.pdfa_value}
    closeOrderEvent

