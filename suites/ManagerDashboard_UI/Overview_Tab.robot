*** Settings ***
Library    overview.py    192.168.9.    admin    apj0702
Library    Collections

*** Test Cases ***
Verify Put Stock on Overview screen
    [Documentation]    Verify Put stock gets updated on Overview tab
    fetchActualStock    serverIP=${sanity.host}    username=${sanity.user}    password=${sanity.password}    execution=${sanity.execution}
    verifyOverviewPutStock    serverIP=${sanity.host}    username=${sanity.user}    password=${sanity.password}    execution=${sanity.execution}    barcodes=${sanity.barcode}    qty=${sanity.put_qty}    pps_put_id=${sanity.put}
    closeOverviewEvent

Verify Audit Stock on Overview screen
    [Documentation]    Verify Audit stock gets updated on Overview tab
    fetchActualStock    serverIP=${sanity.host}    username=${sanity.user}    password=${sanity.password}    execution=${sanity.execution}
    verifyOverviewAuditStock    serverIP=${sanity.host}    username=${sanity.user}    password=${sanity.password}    execution=${sanity.execution}    audit_type=${sanity.audit_type}    audit_value=${sanity.audit_value}    pps_side=${sanity.pps_side}    pps_id=${sanity.audit}    barcode=${sanity.barcode}
    closeOverviewEvent