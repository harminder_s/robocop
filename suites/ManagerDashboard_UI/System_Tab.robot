*** Settings ***
Library    system.py    192.168.9.    admin    apj0702    remote
Library    Collections

*** Test Cases ***
Verify elements are visible on Butler Bots screen
    [Documentation]    Verify elements and details on Butler Bots Sub tab
    veriyButlerBotsSubTab    serverIP=${sanity.host}    username=${sanity.user}    password=${sanity.password}    execution=${sanity.execution}
    closeSystemEvent

Verify elements are visible and clickable on PPS screen
    [Documentation]    Verify elements and details on PPS Sub tab
    verifyPickPutStationsSubTab    serverIP=${sanity.host}    username=${sanity.user}    password=${sanity.password}    execution=${sanity.execution}
    closeSystemEvent

Verify elements are visible on PPS screen
    [Documentation]    Verify elements and details on Charging Station Sub tab
    verifyChargingStationSubTab    serverIP=${sanity.host}    username=${sanity.user}    password=${sanity.password}    execution=${sanity.execution}
    closeSystemEvent

Verify PPS mode change
    [Documentation]    Verify user is able to change the PPS mode
    verifyPpsModeChange    serverIP=${sanity.host}    username=${sanity.user}    password=${sanity.password}    execution=${sanity.execution}
    closeSystemEvent