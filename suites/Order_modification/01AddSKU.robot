***Settings ***
Library	apiHelper.py
Library	Collections	

*** Test Cases ***
Bulk Add Sku
	[Documentation]    Add Bulk Sku by reading from the spreadsheet
	
	add_bulk_sku	${order_modify.host}	username=${order_modify.user}	password=${order_modify.password}	filename=${order_modify.config_file}	spreadsheet=${order_modify.sku_spreadsheet}	item_tags=${order_modify.sku_tags}
	