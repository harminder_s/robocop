***Settings ***
Library	apiHelper.py
Library	Collections	

*** Test Cases ***
Bulk Add Products
	[Documentation]    Add Bulk Products by reading from the spreadsheet
	
	add_bulk_product	${order_modify.host}	username=${order_modify.user}	password=${order_modify.password}	filename=${order_modify.config_file}	spreadsheet=${order_modify.product_spreadsheet}	item_tags=${order_modify.product_tags}