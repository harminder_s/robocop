***Settings ***
Library	apiHelper.py
Library	bulkPut.py
Library	Collections	

*** Test Cases ***

Change PPS Mode
	[Documentation]    Changes the PPS Mode.
    ...                The keyword changes the PPS mode as specified in the config file.
    alignPpsMode	${order_modify.host}	${order_modify.user}	${order_modify.password}	${order_modify.put}	${order_modify.pick}	${order_modify.audit}
	${outMsg}=    Catenate    Modes requested as, Put:  ${order_modify.put}    Pick:    ${order_modify.pick}    Audit:   ${order_modify.audit} 
    Pass Execution    ${outMsg}

Bulk Put Operations
	[Documentation]    Performs bulk and parallel put operations on all PPS station in PUT mode
    ...                The keyword gets the products list from the excel sheet.
    
    @{error_sku}=	bulk_put_operations	${order_modify.host}	username=${order_modify.user}	password=${order_modify.password}	filename=${order_modify.configfile}	spreadsheet=${order_modify.product_spreadsheet}	pick=${order_modify.pick}	put=${order_modify.put}	item_tags=all
	${outMsg}=    Catenate    Successfully completed Put Back and Front Operations
    Pass Execution    ${outMsg}