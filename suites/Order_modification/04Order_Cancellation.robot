***Settings ***
Library	apiHelper.py
Library	orderModify.py
Library	Collections	
Library	pgsqlHelper.py

*** Test Cases ***

Change PPS Mode
	[Documentation]	Changes the PPS Mode.
    ...                The keyword changes the PPS mode as specified in the config file.
    ${host}=	Get Variable Value	${vm_ip}	${order_modify.host}
    alignPpsMode	${host}	${order_modify.user}	${order_modify.password}	${order_modify.put}	${order_modify.pick}	${order_modify.audit}
	${outMsg}=	Catenate	Modes requested as, Put:  ${order_modify.put}    Pick:    ${order_modify.pick}    Audit:   ${order_modify.audit} 
    Pass Execution	${outMsg} 
	
Get PPS ID and Mode
	[Documentation]	Get all PPS ID and Mode.
    ...                The keyword returns a dictionary with "put","pick" and "audit" as keys and their values as list.
    ${host}=	Get Variable Value	${vm_ip}	${order_modify.host}
	${PpsDict}=	getPpsIdMode	${host}	${order_modify.user}	${order_modify.password}
	@{put}=	Get From Dictionary	${PpsDict}	put
	@{pick}=	Get From Dictionary	${PpsDict}	pick
	
	${ppsput}=	Get From List	${put}	0
	${ppspick}=	Get From List	${pick}	0
	
	Set Suite Variable	${ppsput}
	Set Suite Variable	${ppspick}
	${outMsg}=	Catenate	Modes, Put	${ppsput}	Pick:	${ppspick} 
    Pass Execution	${outMsg}

Testcase 1: cancel single line order when rack has not reached pps 
	[Documentation]		cancel order when rack has not reached pps 
	Sleep	5
	${order_id}	${orderline_id}=	add_single_order	${order_modify.host}	${order_modify.user}	${order_modify.password}	pdfavalues=product_sku:2001	qty=${1}
	${response_json}=	delete_order_by_id	${order_modify.host}	${order_modify.user}	${order_modify.password}	${order_id}
	${request_id}=	fetch_request_id	${order_modify.host}	${order_modify.user}	${order_modify.password}	${response_json}	
	${list}=	getModificationRequestStatus	${order_modify.host}	${request_id}
	${output}=	Get From List	${list}	0
	Log	${output}
	${items}=    Get Dictionary Items    ${output}
	Log List    ${items}
    ${request_result}=	List Should Contain Value    ${items}    success
    Log	${request_result} 
    ${order_status_result}=	get_Order_Status	${order_modify.host}	${order_id}
    ${output}=	Get From List	${order_status_result}	0
    Log	${output}
    ${items}=    Get Dictionary Items    ${output}
    Log List    ${items}
    ${result}=	List Should Contain Value	${items}	cancelled	
    Run Keyword if	'${request_result}'!='None'	or	'${result}'!='None'	Fail	"not cancelled"

   
Testcase 2:cancel single line order when rack has reached pps    
    [Documentation]		cancel order when rack has reached pps 
    Sleep	5
	${order_id}	${orderline_id}=	add_single_order	${order_modify.host}	${order_modify.user}	${order_modify.password}	pdfavalues=product_sku:2001	qty=${1}
    : FOR    ${INDEX}    IN RANGE    1    150
    	\	${Order_output}=	PPS_hasOrder	${order_modify.host}	${order_modify.ssh_username}	${order_modify.password}	${ppspick}	${order_id}
    	\    Exit For Loop If    ${Order_output} == True
    	\    Log    ${INDEX}
    Log    Exited
	Log	${Order_output}
	${response_json}=	delete_order_by_id	${order_modify.host}	${order_modify.user}	${order_modify.password}	${order_id}
	${request_id}=	fetch_request_id	${order_modify.host}	${order_modify.user}	${order_modify.password}	${response_json}	
	${list}=	getModificationRequestStatus	${order_modify.host}	${request_id}
	${output}=	Get From List	${list}	0
	Log	${output}
	${items}=    Get Dictionary Items    ${output}
	Log List    ${items}
    ${request_result}=	List Should Contain Value    ${items}    failure
    Log	${request_result}
    ${order_status_result}=	get_Order_Status	${order_modify.host}	${order_id}
    ${output}=	Get From List	${order_status_result}	0
    Log	${output}
    ${items}=    Get Dictionary Items    ${output}
    Log List    ${items}
    ${result}=	List Should Not Contain Value	${items}	cancelled
    Run Keyword if	'${request_result}'!='None'	or	'${result}'!='None'	Fail	"not cancelled"
    pick_front_process	${order_modify.host}	${order_modify.user}	${order_modify.password}	${ppspick}
    pick_back_process	${order_modify.host}	${order_modify.user}	${order_modify.password}	${ppspick}
  	
Testcase 3: cancel multi line order when rack has not reached pps 
	[Documentation]		cancel order when rack has not reached pps 
	Sleep	5
	@{list_tags}	Create List	1	1	1	1
	@{list_quantity}	Create List	2	1	2	1
    ${order_id}=	add_order_multiple_orderline	host=${order_modify.host}	username=${order_modify.user}	password=${order_modify.password}	filename=${order_modify.configfile}	spreadsheet=${order_modify.product_spreadsheet}	list_tags=${list_tags}	list_quantity=${list_quantity}
	Log	${order_id}
	${response_json}=	delete_order_by_id	${order_modify.host}	${order_modify.user}	${order_modify.password}	${order_id}
	${request_id}=	fetch_request_id	${order_modify.host}	${order_modify.user}	${order_modify.password}	${response_json}	
	${list}=	getModificationRequestStatus	${order_modify.host}	${request_id}
	${output}=	Get From List	${list}	0
	Log	${output}
	${items}=    Get Dictionary Items    ${output}
	Log List    ${items}
    ${request_result}=	List Should Contain Value    ${items}    success
    Log	${request_result} 
    ${order_status_result}=	get_Order_Status	${order_modify.host}	${order_id}
    ${output}=	Get From List	${order_status_result}	0
    Log	${output}
    ${items}=    Get Dictionary Items    ${output}
    Log List    ${items}
    ${result}=	List Should Contain Value	${items}	cancelled	
    Run Keyword if	'${request_result}'!='None'	or	'${result}'!='None'	Fail	"not cancelled"

Testcase 4: cancel multi line order when rack has reached pps 
	[Documentation]		cancel order when rack has reached pps 
	Sleep	5
	@{list_tags}	Create List	1	1	1	1
	@{list_quantity}	Create List	2	1	2	1
    ${order_id}=	add_order_multiple_orderline	host=${order_modify.host}	username=${order_modify.user}	password=${order_modify.password}	filename=${order_modify.configfile}	spreadsheet=${order_modify.product_spreadsheet}	list_tags=${list_tags}	list_quantity=${list_quantity}
	Log	${order_id}
	: FOR    ${INDEX}    IN RANGE    1    150
    	\	${Order_output}=	PPS_hasOrder	${order_modify.host}	${order_modify.ssh_username}	${order_modify.password}	${ppspick}	${order_id}
    	\    Exit For Loop If    ${Order_output} == True
    	\    Log    ${INDEX}
    Log    Exited
	Log	${Order_output}
	${response_json}=	delete_order_by_id	${order_modify.host}	${order_modify.user}	${order_modify.password}	${order_id}
	${request_id}=	fetch_request_id	${order_modify.host}	${order_modify.user}	${order_modify.password}	${response_json}	
	${list}=	getModificationRequestStatus	${order_modify.host}	${request_id}
	${output}=	Get From List	${list}	0
	Log	${output}
	${items}=    Get Dictionary Items    ${output}
	Log List    ${items}
    ${request_result}=	List Should Contain Value    ${items}    failure
    Log	${request_result} 
    pick_front_process	${order_modify.host}	${order_modify.user}	${order_modify.password}	${ppspick}
    pick_back_process	${order_modify.host}	${order_modify.user}	${order_modify.password}	${ppspick}
    ${order_status_result}=	get_Order_Status	${order_modify.host}	${order_id}
    ${output}=	Get From List	${order_status_result}	0
    Log	${output}
    ${items}=    Get Dictionary Items    ${output}
    Log List    ${items}
    ${result}=	List Should Contain Value	${items}	completed	
    Run Keyword if	'${request_result}'!='None'	or	'${result}'!='None'	Fail	"not cancelled"
    
Testcase 5:cancel order after order pick	
	[Documentation]		cancel order after order pick
	Sleep	5
	${order_id}	${orderline_id}=	add_single_order	${order_modify.host}	${order_modify.user}	${order_modify.password}	pdfavalues=product_sku:2001	qty=${1}
	pick_front_process	${order_modify.host}	${order_modify.user}	${order_modify.password}	${ppspick}	
	pick_back_process	${order_modify.host}	${order_modify.user}	${order_modify.password}	${ppspick}
	${response_json}=	delete_order_by_id	${order_modify.host}	${order_modify.user}	${order_modify.password}	${order_id}
	${request_id}=	fetch_request_id	${order_modify.host}	${order_modify.user}	${order_modify.password}	${response_json}	
	Run Keyword if	'${request_id}'!='None'	Fail	"completed order cancelled"
    
    
    
	
	
	
	