***Settings ***
Library	apiHelper.py
Library	orderModify.py
Library	Collections	
Library	pgsqlHelper.py

*** Test Cases ***

Testcase1:Change PPS Mode
	[Documentation]	Changes the PPS Mode.
    ...                The keyword changes the PPS mode as specified in the config file.
    ${host}=	Get Variable Value	${vm_ip}	${order_modify.host}
    alignPpsMode	${host}	${order_modify.user}	${order_modify.password}	${order_modify.put}	${order_modify.pick}	${order_modify.audit}
	${outMsg}=	Catenate	Modes requested as, Put:  ${order_modify.put}    Pick:    ${order_modify.pick}    Audit:   ${order_modify.audit} 
    Pass Execution	${outMsg} 
	
	
	
Testcase2: Get PPS ID and Mode
	[Documentation]	Get all PPS ID and Mode.
    ...                The keyword returns a dictionary with "put","pick" and "audit" as keys and their values as list.
    ${host}=	Get Variable Value	${vm_ip}	${order_modify.host}
	${PpsDict}=	getPpsIdMode	${host}	${order_modify.user}	${order_modify.password}
	@{put}=	Get From Dictionary	${PpsDict}	put
	@{pick}=	Get From Dictionary	${PpsDict}	pick
	
	${ppsput}=	Get From List	${put}	0
	${ppspick}=	Get From List	${pick}	0
	
	Set Suite Variable	${ppsput}
	Set Suite Variable	${ppspick}
	${outMsg}=	Catenate	Modes, Put	${ppsput}	Pick:	${ppspick} 
    Pass Execution	${outMsg}
    
Testcase3:Modify single quantity order to multiple quantity order when rack has not reached pps(single line order)  
	[Documentation]		change quantity when rack has not reached pps 
	${order_id}	${orderline_id}=	add_single_order	${order_modify.host}	${order_modify.user}	${order_modify.password}	pdfavalues=product_sku:2001	qty=${1}
	${response_json}=	update_orderline_quantity	${order_modify.host}	${order_modify.user}	${order_modify.password}	${order_id}	${orderline_id}	qty=${10}
	${request_id}=	fetch_request_id	${order_modify.host}	${order_modify.user}	${order_modify.password}	${response_json}	
	${list}=	getModificationRequestStatus	${order_modify.host}	${request_id}
	${output}=	Get From List	${list}	0
	Log	${output}
	${items}=    Get Dictionary Items    ${output}
	Log List    ${items}
    ${request_result}=	List Should Contain Value    ${items}    success
    Log	${request_result} 
    ${current_qty}=	get_Orderline_Quantity	${order_modify.host}	${order_id}	${orderline_id}	
    ${output}=	Get From List	${current_qty}	0
	Log	${output}
	${items}=    Get Dictionary Items    ${output}
	Log List    ${items}
    ${result}=	List Should Contain Value    ${items}    10
    Log	${request_result} 	
    pick_front_process	${order_modify.host}	${order_modify.user}	${order_modify.password}	${ppspick}
    pick_back_process	${order_modify.host}	${order_modify.user}	${order_modify.password}	${ppspick}
    Run Keyword if	'${request_result}'!='None'	and	'${result}'!='None'	Fail	"not modified"
 

Testcase 4:Modify multiple quantity order to single quantity order when rack has not reached pps(single line order) 
	[Documentation]		change quantity when rack has not reached pps 
	${order_id}	${orderline_id}=	add_single_order	${order_modify.host}	${order_modify.user}	${order_modify.password}	product_sku:2001	qty=${10}
	${response_json}=	update_orderline_quantity	${order_modify.host}	${order_modify.user}	${order_modify.password}	${order_id}	${orderline_id}	qty=${1}
	${request_id}=	fetch_request_id	${order_modify.host}	${order_modify.user}	${order_modify.password}	${response_json}	
	${list}=	getModificationRequestStatus	${order_modify.host}	${request_id}
	${output}=	Get From List	${list}	0
	Log	${output}
	${items}=    Get Dictionary Items    ${output}
	Log List    ${items}
    ${request_result}=	List Should Contain Value    ${items}    success
    Log	${request_result} 
    ${current_qty}=	get_Orderline_Quantity	${order_modify.host}	${order_id}	${orderline_id}	
    ${output}=	Get From List	${current_qty}	0
	Log	${output}
	${items}=    Get Dictionary Items    ${output}
	Log List    ${items}
    ${result}=	List Should Contain Value    ${items}    1
    Log	${request_result} 	
    pick_front_process	${order_modify.host}	${order_modify.user}	${order_modify.password}	${ppspick}
    pick_back_process	${order_modify.host}	${order_modify.user}	${order_modify.password}	${ppspick}
    Run Keyword if	'${request_result}'!='None'	and	'${result}'!='None'	Fail	"not modified" 
 
  
Testcase5: Modify multiple quantity order to zero quantity when rack has not reached pps(single line order) 
	[Documentation]		change quantity when rack has not reached pps 
	${order_id}	${orderline_id}=	add_single_order	${order_modify.host}	${order_modify.user}	${order_modify.password}	pdfavalues=product_sku:2001	qty=${10}
	${response_json}=	update_orderline_quantity	${order_modify.host}	${order_modify.user}	${order_modify.password}	${order_id}	${orderline_id}	qty=${0}
	${request_id}=	fetch_request_id	${order_modify.host}	${order_modify.user}	${order_modify.password}	${response_json}	
	${list}=	getModificationRequestStatus	${order_modify.host}	${request_id}
	${output}=	Get From List	${list}	0
	Log	${output}
	${items}=    Get Dictionary Items    ${output}
	Log List    ${items}
    ${request_result}=	List Should Contain Value    ${items}    success
    Log	${request_result} 
    ${current_qty}=	get_Orderline_Quantity	${order_modify.host}	${order_id}	${orderline_id}	
    ${output}=	Get From List	${current_qty}	0
	Log	${output}
	${items}=    Get Dictionary Items    ${output}
	Log List    ${items}
    ${result}=	List Should Contain Value    ${items}    0
    Log	${request_result} 	
    Run Keyword if	'${request_result}'!='None'	and	'${result}'!='None'	Fail	"not modified"     
     
Testcase6: Modify single quantity to zero when rack has not reached pps(single line order) 
	[Documentation]		change quantity when rack has not reached pps 
	${order_id}	${orderline_id}=	add_single_order	${order_modify.host}	${order_modify.user}	${order_modify.password}	pdfavalues=product_sku:2001	qty=${1}
	${response_json}=	update_orderline_quantity	${order_modify.host}	${order_modify.user}	${order_modify.password}	${order_id}	${orderline_id}	qty=${0}
	${request_id}=	fetch_request_id	${order_modify.host}	${order_modify.user}	${order_modify.password}	${response_json}	
	${list}=	getModificationRequestStatus	${order_modify.host}	${request_id}
	${output}=	Get From List	${list}	0
	Log	${output}
	${items}=    Get Dictionary Items    ${output}
	Log List    ${items}
    ${request_result}=	List Should Contain Value    ${items}    success
    Log	${request_result} 
    ${current_qty}=	get_Orderline_Quantity	${order_modify.host}	${order_id}	${orderline_id}	
    ${output}=	Get From List	${current_qty}	0
	Log	${output}
	${items}=    Get Dictionary Items    ${output}
	Log List    ${items}
    ${result}=	List Should Contain Value    ${items}    0
    Log	${request_result} 	
    Run Keyword if	'${request_result}'!='None'	and	'${result}'!='None'	Fail	"not modified"     

Testcase7: Modify quantity of a multi quantity order when rack has not reached pps(single line order)         
    [Documentation]		change quantity when rack has reached pps 
    ${order_id}	${orderline_id}=	add_single_order	${order_modify.host}	${order_modify.user}	${order_modify.password}	pdfavalues=product_sku:2001	qty=${10}
    ${Order_output}=	PPS_hasOrder	${order_modify.host}	${order_modify.ssh_username}	${order_modify.password}	${ppspick}	${order_id}
	Log	${Order_output}
	${response_json}=	update_orderline_quantity	${order_modify.host}	${order_modify.user}	${order_modify.password}	${order_id}	${orderline_id}	qty=${6}
	${request_id}=	fetch_request_id	${order_modify.host}	${order_modify.user}	${order_modify.password}	${response_json}	
	${list}=	getModificationRequestStatus	${order_modify.host}	${request_id}
	${output}=	Get From List	${list}	0
	Log	${output}
	${items}=    Get Dictionary Items    ${output}
	Log List    ${items}
    ${request_result}=	List Should Contain Value    ${items}    success
    Log	${request_result} 
    ${current_qty}=	get_Orderline_Quantity	${order_modify.host}	${order_id}	${orderline_id}	
    ${output}=	Get From List	${current_qty}	0
	Log	${output}
	${items}=    Get Dictionary Items    ${output}
	Log List    ${items}
    ${result}=	List Should Contain Value    ${items}    6
    Log	${request_result} 
    pick_front_process	${order_modify.host}	${order_modify.user}	${order_modify.password}	${ppspick}
    pick_back_process	${order_modify.host}	${order_modify.user}	${order_modify.password}	${ppspick}	
    Run Keyword if	'${request_result}'!='None'	and	'${result}'!='None'	Fail	"not modified"                                                     
        
Testcase8: Modify single quantity order to zero quantity when rack has reached pps(single line order)         
    [Documentation]		change quantity when rack has reached pps 
    ${order_id}	${orderline_id}=	add_single_order	${order_modify.host}	${order_modify.user}	${order_modify.password}	pdfavalues=product_sku:2001	qty=${1}
    : FOR    ${INDEX}    IN RANGE    1    150
    	\	${Order_output}=	PPS_hasOrder	${order_modify.host}	${order_modify.ssh_username}	${order_modify.password}	${ppspick}	${order_id}
    	\    Exit For Loop If    ${Order_output} == True
    	\    Log	${INDEX}
    Log    Exited
	Log	${Order_output}
	${response_json}=	update_orderline_quantity	${order_modify.host}	${order_modify.user}	${order_modify.password}	${order_id}	${orderline_id}	qty=${0}
	${request_id}=	fetch_request_id	${order_modify.host}	${order_modify.user}	${order_modify.password}	${response_json}	
	${list}=	getModificationRequestStatus	${order_modify.host}	${request_id}
	${output}=	Get From List	${list}	0
	Log	${output}
	${items}=    Get Dictionary Items    ${output}
	Log List    ${items}
    ${request_result}=	List Should Contain Value    ${items}    failure
    Log	${request_result} 
    ${current_qty}=	get_Orderline_Quantity	${order_modify.host}	${order_id}	${orderline_id}	
    ${output}=	Get From List	${current_qty}	0
	Log	${output}
	${items}=    Get Dictionary Items    ${output}
	Log List    ${items}
    ${result}=	List Should Contain Value    ${items}    1
    Log	${request_result} 
    pick_front_process	${order_modify.host}	${order_modify.user}	${order_modify.password}	${ppspick}
    pick_back_process	${order_modify.host}	${order_modify.user}	${order_modify.password}	${ppspick}	
    Run Keyword if	'${request_result}'!='None'	and	'${result}'!='None'	Fail	"not modified"     
 

Testcase 9 :Modify single quantity order to multi quantity when rack has reached pps(single line order)         
    [Documentation]		change quantity when rack has reached pps 
    ${order_id}	${orderline_id}=	add_single_order	${order_modify.host}	${order_modify.user}	${order_modify.password}	pdfavalues=product_sku:2001	qty=${1}
    : FOR    ${INDEX}    IN RANGE    1    150
    	\	${Order_output}=	PPS_hasOrder	${order_modify.host}	${order_modify.ssh_username}	${order_modify.password}	${ppspick}	${order_id}
    	\    Exit For Loop If    ${Order_output} == True
    	\    Log    ${INDEX}
    Log    Exited
	Log	${Order_output}
	${response_json}=	update_orderline_quantity	${order_modify.host}	${order_modify.user}	${order_modify.password}	${order_id}	${orderline_id}	qty=${5}
	${request_id}=	fetch_request_id	${order_modify.host}	${order_modify.user}	${order_modify.password}	${response_json}	
	${list}=	getModificationRequestStatus	${order_modify.host}	${request_id}
	${output}=	Get From List	${list}	0
	Log	${output}
	${items}=    Get Dictionary Items    ${output}
	Log List    ${items}
    ${request_result}=	List Should Contain Value    ${items}    failure
    Log	${request_result} 
    ${current_qty}=	get_Orderline_Quantity	${order_modify.host}	${order_id}	${orderline_id}	
    ${output}=	Get From List	${current_qty}	0
	Log	${output}
	${items}=    Get Dictionary Items    ${output}
	Log List    ${items}
    ${result}=	List Should Contain Value    ${items}    1
    Log	${request_result} 
    pick_front_process	${order_modify.host}	${order_modify.user}	${order_modify.password}	${ppspick}
    pick_back_process	${order_modify.host}	${order_modify.user}	${order_modify.password}	${ppspick}	
    Run Keyword if	'${request_result}'!='None'	and	'${result}'!='None'	Fail	"not modified"  
    
Testcase 10: Modify multi quantity order to single quantity when rack has reached pps(single line order)         
    [Documentation]		change quantity when rack has reached pps 
    ${order_id}	${orderline_id}=	add_single_order	${order_modify.host}	${order_modify.user}	${order_modify.password}	pdfavalues=product_sku:2001	qty=${10}
    : FOR    ${INDEX}    IN RANGE    1    150
    	\	${Order_output}=	PPS_hasOrder	${order_modify.host}	${order_modify.ssh_username}	${order_modify.password}	${ppspick}	${order_id}
    	\    Exit For Loop If    ${Order_output} == True
    	\    Log    ${INDEX}
    Log    Exited
	Log	${Order_output}
	Sleep	10
	${response_json}=	update_orderline_quantity	${order_modify.host}	${order_modify.user}	${order_modify.password}	${order_id}	${orderline_id}	qty=${1}
	${request_id}=	fetch_request_id	${order_modify.host}	${order_modify.user}	${order_modify.password}	${response_json}	
	${list}=	getModificationRequestStatus	${order_modify.host}	${request_id}
	${output}=	Get From List	${list}	0
	Log	${output}
	${items}=    Get Dictionary Items    ${output}
	Log List    ${items}
    ${request_result}=	List Should Contain Value    ${items}    failure
    Log	${request_result} 
    ${current_qty}=	get_Orderline_Quantity	${order_modify.host}	${order_id}	${orderline_id}	
    ${output}=	Get From List	${current_qty}	0
	Log	${output}
	${items}=    Get Dictionary Items    ${output}
	Log List    ${items}
    ${result}=	List Should Contain Value    ${items}    10
    Log	${request_result} 
    pick_front_process	${order_modify.host}	${order_modify.user}	${order_modify.password}	${ppspick}
    pick_back_process	${order_modify.host}	${order_modify.user}	${order_modify.password}	${ppspick}	
    Run Keyword if	'${request_result}'!='None'	and	'${result}'!='None'	Fail	"not modified" 

   
Testcase 11:Modify multi quantity order to zero quantity when rack has reached pps(single line order)         
    [Documentation]		change quantity when rack has reached pps 
    ${order_id}	${orderline_id}=	add_single_order	${order_modify.host}	${order_modify.user}	${order_modify.password}	pdfavalues=product_sku:2001	qty=${5}
    : FOR    ${INDEX}    IN RANGE    1    150
    	\	${Order_output}=	PPS_hasOrder	${order_modify.host}	${order_modify.ssh_username}	${order_modify.password}	${ppspick}	${order_id}
    	\    Exit For Loop If    ${Order_output} == True
    	\    Log    ${INDEX}
    Log    Exited
	Log	${Order_output}
	Sleep	10
	${response_json}=	update_orderline_quantity	${order_modify.host}	${order_modify.user}	${order_modify.password}	${order_id}	${orderline_id}	qty=${0}
	${request_id}=	fetch_request_id	${order_modify.host}	${order_modify.user}	${order_modify.password}	${response_json}	
	${list}=	getModificationRequestStatus	${order_modify.host}	${request_id}
	${output}=	Get From List	${list}	0
	Log	${output}
	${items}=    Get Dictionary Items    ${output}
	Log List    ${items}
    ${request_result}=	List Should Contain Value    ${items}    failure
    Log	${request_result} 
    ${current_qty}=	get_Orderline_Quantity	${order_modify.host}	${order_id}	${orderline_id}	
    ${output}=	Get From List	${current_qty}	0
	Log	${output}
	${items}=    Get Dictionary Items    ${output}
	Log List    ${items}
    ${result}=	List Should Contain Value    ${items}    5
    Log	${result} 
    pick_front_process	${order_modify.host}	${order_modify.user}	${order_modify.password}	${ppspick}
    pick_back_process	${order_modify.host}	${order_modify.user}	${order_modify.password}	${ppspick}	
    Run Keyword if	'${request_result}'!='None'	and	'${result}'!='None'	Fail	"not modified"   

   
Testcase 12: modifying quantity from 0 to any other quantity(single-line order)  
	[Documentation]		verify that quantity can't be changed from 0
    ${order_id}	${orderline_id}=	add_single_order	${order_modify.host}	${order_modify.user}	${order_modify.password}	pdfavalues=product_sku:2001	qty=${5}
    update_orderline_quantity	${order_modify.host}	${order_modify.user}	${order_modify.password}	${order_id}	${orderline_id}	qty=${0}
    ${response_json}=	update_orderline_quantity	${order_modify.host}	${order_modify.user}	${order_modify.password}	${order_id}	${orderline_id}	qty=${5}
	${request_id}=	fetch_request_id	${order_modify.host}	${order_modify.user}	${order_modify.password}	${response_json}	
    ${current_qty}=	get_Orderline_Quantity	${order_modify.host}	${order_id}	${orderline_id}	
    ${output}=	Get From List	${current_qty}	0
	Log	${output}
	${items}=    Get Dictionary Items    ${output}
	Log List    ${items}
    ${result}=	List Should Contain Value    ${items}    0
    Run Keyword if	'${request_id}'!='None'	and	'${result}'!='None'	Fail	"order got modified from 0 quantity"  
    

Testcase 13: modifying quantity from single to any other quantity(multi-line order)  
	[Documentation]		change quantity of an orderline from single to multi (multi-line order)
	@{list_tags}	Create List	1	2	3	1
	@{list_quantity}	Create List	1	1	3	4
    ${order_id}=	add_order_multiple_orderline	host=${order_modify.host}	username=${order_modify.user}	password=${order_modify.password}	filename=${order_modify.configfile}	spreadsheet=${order_modify.product_spreadsheet}	list_tags=${list_tags}	list_quantity=${list_quantity}
	Log	${order_id}
	${orderlines}=	get_orderlines_of_order	host=${order_modify.host}	order_id=${order_id}	
	${output}=	Get From List	${orderlines}	0
	${items}=	Get Dictionary Items    ${output}	
	${orderline_id}=	Get From List	${items}	1
	Log	${orderline_id}	
	${response_json}=	update_orderline_quantity	${order_modify.host}	${order_modify.user}	${order_modify.password}	${order_id}	${orderline_id}	qty=${5}
	${request_id}=	fetch_request_id	${order_modify.host}	${order_modify.user}	${order_modify.password}	${response_json}	
	${list}=	getModificationRequestStatus	${order_modify.host}	${request_id}
	${output}=	Get From List	${list}	0
	Log	${output}
	${items}=    Get Dictionary Items    ${output}
	Log List    ${items}
    ${request_result}=	List Should Contain Value    ${items}    success
    Log	${request_result} 
    ${current_qty}=	get_Orderline_Quantity	${order_modify.host}	${order_id}	${orderline_id}	
    ${output}=	Get From List	${current_qty}	0
	Log	${output}
	${items}=    Get Dictionary Items    ${output}
	Log List    ${items}
    ${result}=	List Should Contain Value    ${items}    5
    Log	${result} 
	pick_front_process	${order_modify.host}	${order_modify.user}	${order_modify.password}	${ppspick}
    Run Keyword if	'${request_result}'!='None'	and	'${result}'!='None'	Fail	"not modified"                                                     
    

Testcase 14: modifying quantity from multi to single quantity when rack has not reached pps(multi-line order)  
	[Documentation]		change quantity of an orderline from multi to single (multi-line order)
	@{list_tags}	Create List	1	3	2	1
	@{list_quantity}	Create List	4	5	1	2
    ${order_id}=	add_order_multiple_orderline	host=${order_modify.host}	username=${order_modify.user}	password=${order_modify.password}	filename=${order_modify.configfile}	spreadsheet=${order_modify.product_spreadsheet}	list_tags=${list_tags}	list_quantity=${list_quantity}
	Log	${order_id}
	${orderlines}=	get_orderlines_of_order	host=${order_modify.host}	order_id=${order_id}	
	${output}=	Get From List	${orderlines}	0
	${items}=	Get Dictionary Items    ${output}	
	${orderline_id}=	Get From List	${items}	1
	Log	${orderline_id}	
	${response_json}=	update_orderline_quantity	${order_modify.host}	${order_modify.user}	${order_modify.password}	${order_id}	${orderline_id}	qty=${1}
	${request_id}=	fetch_request_id	${order_modify.host}	${order_modify.user}	${order_modify.password}	${response_json}	
	${list}=	getModificationRequestStatus	${order_modify.host}	${request_id}
	${output}=	Get From List	${list}	0
	Log	${output}
	${items}=    Get Dictionary Items    ${output}
	Log List    ${items}
    ${request_result}=	List Should Contain Value    ${items}    success
    Log	${request_result} 
    ${current_qty}=	get_Orderline_Quantity	${order_modify.host}	${order_id}	${orderline_id}	
    ${output}=	Get From List	${current_qty}	0
	Log	${output}
	${items}=    Get Dictionary Items    ${output}
	Log List    ${items}
    ${result}=	List Should Contain Value    ${items}    1
    Log	${result} 
	pick_front_process	${order_modify.host}	${order_modify.user}	${order_modify.password}	${ppspick}
    Run Keyword if	'${request_result}'!='None'	and	'${result}'!='None'	Fail	"not modified"                                                     
 
 

Testcase 15: modifying quantity of an orderline to 0 when rack has not reached pps (multi-line order)  
	[Documentation]		change quantity of an orderline from single to multi (multi-line order)
	@{list_tags}	Create List	2	1	1	1
	@{list_quantity}	Create List	4	1	3	4
    ${order_id}=	add_order_multiple_orderline	host=${order_modify.host}	username=${order_modify.user}	password=${order_modify.password}	filename=${order_modify.configfile}	spreadsheet=${order_modify.product_spreadsheet}	list_tags=${list_tags}	list_quantity=${list_quantity}
	Log	${order_id}
	${orderlines}=	get_orderlines_of_order	host=${order_modify.host}	order_id=${order_id}	
	${output}=	Get From List	${orderlines}	0
	${items}=	Get Dictionary Items    ${output}	
	${orderline_id}=	Get From List	${items}	1
	Log	${orderline_id}	
	${response_json}=	update_orderline_quantity	${order_modify.host}	${order_modify.user}	${order_modify.password}	${order_id}	${orderline_id}	qty=${0}
	${request_id}=	fetch_request_id	${order_modify.host}	${order_modify.user}	${order_modify.password}	${response_json}	
	${list}=	getModificationRequestStatus	${order_modify.host}	${request_id}
	${output}=	Get From List	${list}	0
	Log	${output}
	${items}=    Get Dictionary Items    ${output}
	Log List    ${items}
    ${request_result}=	List Should Contain Value    ${items}    success
    Log	${request_result} 
    ${current_qty}=	get_Orderline_Quantity	${order_modify.host}	${order_id}	${orderline_id}	
    ${output}=	Get From List	${current_qty}	0
	Log	${output}
	${items}=    Get Dictionary Items    ${output}
	Log List    ${items}
    ${result}=	List Should Contain Value    ${items}    0
    Log	${result} 
	pick_front_process	${order_modify.host}	${order_modify.user}	${order_modify.password}	${ppspick}
    Run Keyword if	'${request_result}'!='None'	and	'${result}'!='None'	Fail	"not modified"                                                     
 

Testcase 16: modifying quantity of an orderline when rack has reached pps(order from single rack) (multi-line order)  
   [Documentation]		modifying quantity of an orderline when rack has reached pps (multi-line order)
	@{list_tags}	Create List	3	2	2	3
	@{list_quantity}	Create List	4	1	3	4
    ${order_id}=	add_order_multiple_orderline	host=${order_modify.host}	username=${order_modify.user}	password=${order_modify.password}	filename=${order_modify.configfile}	spreadsheet=${order_modify.product_spreadsheet}	list_tags=${list_tags}	list_quantity=${list_quantity}
	Log	${order_id}  
	${orderlines}=	get_orderlines_of_order	host=${order_modify.host}	order_id=${order_id}	
	${output}=	Get From List	${orderlines}	0
	${items}=	Get Dictionary Items    ${output}	
	${orderline_id}=	Get From List	${items}	1
	Log	${orderline_id}  
	: FOR    ${INDEX}    IN RANGE    1    150
    	\	${Order_output}=	PPS_hasOrder	${order_modify.host}	${order_modify.ssh_username}	${order_modify.password}	${ppspick}	${order_id}
    	\    Exit For Loop If    ${Order_output} == True
    	\    Log    ${INDEX}
    Log    Exited
	Log	${Order_output}
	Sleep	10
	${response_json}=	update_orderline_quantity	${order_modify.host}	${order_modify.user}	${order_modify.password}	${order_id}	${orderline_id}	qty=${2}
	${request_id}=	fetch_request_id	${order_modify.host}	${order_modify.user}	${order_modify.password}	${response_json}	
	${list}=	getModificationRequestStatus	${order_modify.host}	${request_id}
	${output}=	Get From List	${list}	0
	Log	${output}
	${items}=    Get Dictionary Items    ${output}
	Log List    ${items}
    ${request_result}=	List Should Contain Value    ${items}    failure
    Log	${request_result} 
    ${current_qty}=	get_Orderline_Quantity	${order_modify.host}	${order_id}	${orderline_id}	
    ${output}=	Get From List	${current_qty}	0
	Log	${output}
	${items}=    Get Dictionary Items    ${output}
	Log List    ${items}
    ${result}=	List Should Contain Value    ${items}    4
    Log	${request_result} 
    pick_front_process	${order_modify.host}	${order_modify.user}	${order_modify.password}	${ppspick}
    pick_back_process	${order_modify.host}	${order_modify.user}	${order_modify.password}	${ppspick}	
    Run Keyword if	'${request_result}'!='None'	and	'${result}'!='None'	Fail	"not modified" 
	    
   