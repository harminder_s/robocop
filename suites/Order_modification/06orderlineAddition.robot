***Settings ***
Library	apiHelper.py
Library	orderModify.py
Library	Collections	
Library	pgsqlHelper.py


*** Test Cases ***    

Testcase1:Change PPS Mode
	[Documentation]	Changes the PPS Mode.
    ...                The keyword changes the PPS mode as specified in the config file.
    ${host}=	Get Variable Value	${vm_ip}	${order_modify.host}
    alignPpsMode	${host}	${order_modify.user}	${order_modify.password}	${order_modify.put}	${order_modify.pick}	${order_modify.audit}
	${outMsg}=	Catenate	Modes requested as, Put:  ${order_modify.put}    Pick:    ${order_modify.pick}    Audit:   ${order_modify.audit} 
    Pass Execution	${outMsg} 
	
	
	
Testcase2: Get PPS ID and Mode
	[Documentation]	Get all PPS ID and Mode.
    ...                The keyword returns a dictionary with "put","pick" and "audit" as keys and their values as list.
    ${host}=	Get Variable Value	${vm_ip}	${order_modify.host}
	${PpsDict}=	getPpsIdMode	${host}	${order_modify.user}	${order_modify.password}
	@{put}=	Get From Dictionary	${PpsDict}	put
	@{pick}=	Get From Dictionary	${PpsDict}	pick
	
	${ppsput}=	Get From List	${put}	0
	${ppspick}=	Get From List	${pick}	0
	
	Set Suite Variable	${ppsput}
	Set Suite Variable	${ppspick}
	${outMsg}=	Catenate	Modes, Put	${ppsput}	Pick:	${ppspick} 
    Pass Execution	${outMsg}
  

Testcase 3: Add orderline to a single line order when rack has not reached pps
	[Documentation]	Add orderline to a single line order when rack has not reached pps
	${order_id}	${orderline_id}=	add_single_order	${order_modify.host}	${order_modify.user}	${order_modify.password}	pdfavalues=product_sku:2001	qty=${1}
	add_orderline	${order_modify.host}	${order_modify.user}	${order_modify.password}	${order_id}	pdfavalues=product_sku:2001	qty=${2}	
	@{orderlines}=	get_orderlines_of_order	host=${order_modify.host}	order_id=${order_id}
	${count}=	Get Length	${orderlines}
	pick_front_process	${order_modify.host}	${order_modify.user}	${order_modify.password}	${ppspick}
    pick_back_process	${order_modify.host}	${order_modify.user}	${order_modify.password}	${ppspick}
    ${order_status_result}=	get_Order_Status	${order_modify.host}	${order_id}
    ${output}=	Get From List	${order_status_result}	0
    Log	${output}
    ${items}=    Get Dictionary Items    ${output}
    Log List    ${items}
    ${result}=	List Should Contain Value	${items}	completed
    Run Keyword if	'${result}'!='None'	or	'${count}'!=2	Fail	"not modified"

*** comment *** 
Testcase 4: Add orderline to a multi line order when rack has not reached pps
	[Documentation]	Add orderline to a multi line order when rack has not reached pps
	@{list_tags}	Create List	1	1	1	1
	@{list_quantity}	Create List	4	5	1	2
    ${order_id}=	add_order_multiple_orderline	host=${order_modify.host}	username=${order_modify.user}	password=${order_modify.password}	filename=${order_modify.products_sheet}	spreadsheet=${order_modify.spreadsheet}	list_tags=${list_tags}	list_quantity=${list_quantity}
	Log	${order_id}
	add_orderline	${order_modify.host}	${order_modify.user}	${order_modify.password}	${order_id}	pdfavalues=product_sku:2001	qty=${2}	
	${orderlines}=	get_orderlines_of_order	host=${order_modify.host}	order_id=${order_id}
	${count}=	Get Length	${orderlines}	
	pick_front_process	${order_modify.host}	${order_modify.user}	${order_modify.password}	${ppspick}
    pick_back_process	${order_modify.host}	${order_modify.user}	${order_modify.password}	${ppspick}
    ${order_status_result}=	get_Order_Status	${order_modify.host}	${order_id}
    ${output}=	Get From List	${order_status_result}	0
    Log	${output}
    ${items}=    Get Dictionary Items    ${output}
    Log List    ${items}
    ${result}=	List Should Contain Value	${items}	completed
    Run Keyword if	'${result}'!='None'	and	'${count}'!=5	'Fail	"not modified"
    

Testcase 5: Add orderline to a multi line order when rack has reached pps
	[Documentation]	Add orderline to a single line order when rack has reached pps
	${order_id}	${orderline_id}=	add_single_order	${order_modify.host}	${order_modify.user}	${order_modify.password}	pdfavalues=product_sku:2001	qty=${1}
	: FOR    ${INDEX}    IN RANGE    1    150
    	\	${Order_output}=	PPS_hasOrder	${order_modify.host}	${order_modify.ssh_username}	${order_modify.password}	${ppspick}	${order_id}
    	\    Exit For Loop If    ${Order_output} == True
    	\    Log    ${INDEX}
    Log    Exited
	Log	${Order_output}
	Sleep	10
	add_orderline	${order_modify.host}	${order_modify.user}	${order_modify.password}	${order_id}	pdfavalues=product_sku:2001	qty=${2}	
	@{orderlines}=	get_orderlines_of_order	host=${order_modify.host}	order_id=${order_id}
	${count}=	Get Length	${orderlines}
	pick_front_process	${order_modify.host}	${order_modify.user}	${order_modify.password}	${ppspick}
    pick_back_process	${order_modify.host}	${order_modify.user}	${order_modify.password}	${ppspick}
    ${order_status_result}=	get_Order_Status	${order_modify.host}	${order_id}
    ${output}=	Get From List	${order_status_result}	0
    Log	${output}
    ${items}=    Get Dictionary Items    ${output}
    Log List    ${items}
    ${result}=	List Should Contain Value	${items}	completed
    Run Keyword if	'${result}'!='None'	and	'${count}'!=1	Fail	"not modified"	

Testcase 6: Add orderline to a single line order when rack has reached pps
	[Documentation]	Add orderline to a single line order when rack has reached pps
	${order_id}	${orderline_id}=	add_single_order	${order_modify.host}	${order_modify.user}	${order_modify.password}	pdfavalues=product_sku:2001	qty=${1}
	: FOR    ${INDEX}    IN RANGE    1    150
    	\	${Order_output}=	PPS_hasOrder	${order_modify.host}	${order_modify.ssh_username}	${order_modify.password}	${ppspick}	${order_id}
    	\    Exit For Loop If    ${Order_output} == True
    	\    Log    ${INDEX}
    Log    Exited
	Log	${Order_output}
	Sleep	10
	add_orderline	${order_modify.host}	${order_modify.user}	${order_modify.password}	${order_id}	pdfavalues=product_sku:2001	qty=${2}	
	@{orderlines}=	get_orderlines_of_order	host=${order_modify.host}	order_id=${order_id}
	${count}=	Get Length	${orderlines}
	pick_front_process	${order_modify.host}	${order_modify.user}	${order_modify.password}	${ppspick}
    pick_back_process	${order_modify.host}	${order_modify.user}	${order_modify.password}	${ppspick}
    ${order_status_result}=	get_Order_Status	${order_modify.host}	${order_id}
    ${output}=	Get From List	${order_status_result}	0
    Log	${output}
    ${items}=    Get Dictionary Items    ${output}
    Log List    ${items}
    ${result}=	List Should Contain Value	${items}	completed
    Run Keyword if	'${result}'!='None'	and	'${count}'!=1	Fail	"not modified"
 
	
    
Testcase 7: Add orderline to a single line order and make qty as zero for one of the orderline
	[Documentation]	Add orderline to a single line order when rack has reached pps
	${order_id}	${orderline_id}=	add_single_order	${order_modify.host}	${order_modify.user}	${order_modify.password}	pdfavalues=product_sku:2001	qty=${5}
	add_orderline	${order_modify.host}	${order_modify.user}	${order_modify.password}	${order_id}	pdfavalues=product_sku:2001	qty=${2}	
	@{orderlines}=	get_orderlines_of_order	host=${order_modify.host}	order_id=${order_id}
	${count}=	Get Length	${orderlines}
	${response_json}=	update_orderline_quantity	${order_modify.host}	${order_modify.user}	${order_modify.password}	${order_id}	${orderline_id}	qty=${0}
	${request_id}=	fetch_request_id	${order_modify.host}	${order_modify.user}	${order_modify.password}	${response_json}	
	${list}=	getModificationRequestStatus	${order_modify.host}	${request_id}
	${output}=	Get From List	${list}	0
	Log	${output}
	${items}=    Get Dictionary Items    ${output}
	Log List    ${items}
    ${request_result}=	List Should Contain Value    ${items}    success
    Log	${request_result} 
    ${current_qty}=	get_Orderline_Quantity	${order_modify.host}	${order_id}	${orderline_id}	
    ${output}=	Get From List	${current_qty}	0
	Log	${output}
	${items}=    Get Dictionary Items    ${output}
	Log List    ${items}
    ${result}=	List Should Contain Value    ${items}    0
    Log	${result} 
	pick_front_process	${order_modify.host}	${order_modify.user}	${order_modify.password}	${ppspick}
    pick_back_process	${order_modify.host}	${order_modify.user}	${order_modify.password}	${ppspick}
    ${order_status_result}=	get_Order_Status	${order_modify.host}	${order_id}
    ${output}=	Get From List	${order_status_result}	0
    Log	${output}
    ${items}=    Get Dictionary Items    ${output}
    Log List    ${items}
    ${result}=	List Should Contain Value	${items}	completed
    Run Keyword if	'${result}'!='None'	and	'${count}'!=2	Fail	"not modified"    