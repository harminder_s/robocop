***Settings ***
Library	apiHelper.py
Library	Collections	

*** Test Cases ***
Bulk Add Sku
	[Documentation]    Add Bulk Sku by reading from the spreadsheet
	
	add_bulk_sku	${testOrderlines.host}	username=${testOrderlines.user}	password=${testOrderlines.password}	filename=${testOrderlines.configfile}	spreadsheet=${testOrderlines.sku_spreadsheet}	item_tags=${testOrderlines.sku_tags}
	