***Settings ***
Library	apiHelper.py
Library	Collections	

*** Test Cases ***
Bulk Add Products
	[Documentation]    Add Bulk Products by reading from the spreadsheet
	
	add_bulk_product	${testOrderlines.host}	username=${testOrderlines.user}	password=${testOrderlines.password}	filename=${testOrderlines.configfile}	spreadsheet=${testOrderlines.product_spreadsheet}	item_tags=${testOrderlines.product_tags}
