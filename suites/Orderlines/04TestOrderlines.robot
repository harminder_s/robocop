***Settings ***
Library	apiHelper.py
Library	orderlines.py
Library	Collections

*** Test Cases ***

Change PPS Mode
	[Documentation]    Changes the PPS Mode.
    ...                The keyword changes the PPS mode as specified in the config file.
    alignPpsMode	${testOrderlines.host}	${testOrderlines.user}	${testOrderlines.password}	${testOrderlines.put}	${testOrderlines.pick}	${testOrderlines.audit}
	${outMsg}=    Catenate    Modes requested as, Put:  ${testOrderlines.put}    Pick:    ${testOrderlines.pick}    Audit:   ${testOrderlines.audit} 
    Pass Execution    ${outMsg}
    
    
*** Test Cases ***
Multiple Orderlines
	[Documentation]    Performs bulk and parallel put operations on all PPS station in PUT mode
    ...                The keyword gets the products list from the excel sheet.
    
    testOrderlines	${testOrderlines.host}	username=${testOrderlines.user}	password=${testOrderlines.password}	filename=${testOrderlines.configfile}	testdata_spreadsheet=${testOrderlines.test_data_spreadsheet}	product_spreadsheet=${testOrderlines.product_spreadsheet}	pick=${testOrderlines.pick}	put=${testOrderlines.put}
	${outMsg}=    Catenate    Successfully completed Put Back and Front Operations
    Pass Execution    ${outMsg}
