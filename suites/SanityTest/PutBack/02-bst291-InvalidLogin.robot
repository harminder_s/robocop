***Settings ***
Library	apiHelper.py
Library	Collections	

*** Test Cases ***

Change PPS Mode
	[Documentation]	Changes the PPS Mode.
    ...                The keyword changes the PPS mode as specified in the config file.
    ${host}=	Get Variable Value	${vm_ip}	${sanity.host}
    alignPpsMode	${host}	${sanity.user}	${sanity.password}	${sanity.put}	${sanity.pick}	${sanity.audit}
	${outMsg}=	Catenate	Modes requested as, Put:  ${sanity.put}    Pick:    ${sanity.pick}    Audit:   ${sanity.audit} 
    Pass Execution	${outMsg} 
	
	
Get PPS ID and Mode
	[Documentation]	Get all PPS ID and Mode.
    ...                The keyword returns a dictionary with "put","pick" and "audit" as keys and their values as list.
    ${host}=	Get Variable Value	${vm_ip}	${sanity.host}
	${PpsDict}=	getPpsIdMode	${host}	${sanity.user}	${sanity.password}
	@{put}=	Get From Dictionary	${PpsDict}	put
	@{pick}=	Get From Dictionary	${PpsDict}	pick
	
	${ppsput}=	Get From List	${put}	0
	${ppspick}=	Get From List	${pick}	0
	
	Set Suite Variable	${ppsput}
	Set Suite Variable	${ppspick}
	${outMsg}=	Catenate	Modes, Put	${ppsput}	Pick:	${ppspick} 
    Pass Execution	${outMsg}
    
    
Login to Put Back Station with Invalid Password
	[Documentation]	Verify that user is not able to login with invalid password
					
	${host}=	Get Variable Value	${vm_ip}	${sanity.host}
	Log	${host}
	${invalid_password}=	set variable	apj0702420
	${output}=	loginApi	${host}	${sanity.user}	${invalid_password}	${ppsput}	pps_side=back
	Log	${output}
	
	${successfulMsg}=	Catenate	Unable to login with incorrect password
	${failureMsg}=	Catenate	Able to login with incorrect password
	Run Keyword If	${output} is False	Pass Execution	${successfulMsg}	ELSE	Fail	${failureMsg}


Login to Put Back Station with Invalid Username
	[Documentation]	Verify that user is not able to login with invalid username
					
	${host}=	Get Variable Value	${vm_ip}	${sanity.host}
	Log	${host}
	${invalid_username}=	set variable	admin420
	${output}=	loginApi	${host}	${invalid_username}	${sanity.password}	${ppsput}	pps_side=back
	Log	${output}
	
	${successfulMsg}=	Catenate	Unable to login with incorrect username
	${failureMsg}=	Catenate	Able to login with incorrect username
	Run Keyword If	${output} is False	Pass Execution	${successfulMsg}	ELSE	Fail	${failureMsg}
	
	
	
