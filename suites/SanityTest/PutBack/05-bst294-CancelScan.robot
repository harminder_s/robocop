***Settings ***
Library	apiHelper.py
Library	Collections	

*** Test Cases ***

Change PPS Mode
	[Documentation]	Changes the PPS Mode.
    ...                The keyword changes the PPS mode as specified in the config file.
    ${host}=	Get Variable Value	${vm_ip}	${sanity.host}
    alignPpsMode	${host}	${sanity.user}	${sanity.password}	${sanity.put}	${sanity.pick}	${sanity.audit}
	${outMsg}=	Catenate	Modes requested as, Put:  ${sanity.put}    Pick:    ${sanity.pick}    Audit:   ${sanity.audit} 
    Pass Execution	${outMsg} 
	
	
	
Get PPS ID and Mode
	[Documentation]	Get all PPS ID and Mode.
    ...                The keyword returns a dictionary with "put","pick" and "audit" as keys and their values as list.
    ${host}=	Get Variable Value	${vm_ip}	${sanity.host}
	${PpsDict}=	getPpsIdMode	${host}	${sanity.user}	${sanity.password}
	@{put}=	Get From Dictionary	${PpsDict}	put
	@{pick}=	Get From Dictionary	${PpsDict}	pick
	
	${ppsput}=	Get From List	${put}	0
	${ppspick}=	Get From List	${pick}	0
	
	Set Suite Variable	${ppsput}
	Set Suite Variable	${ppspick}
	${outMsg}=	Catenate	Modes, Put	${ppsput}	Pick:	${ppspick} 
    Pass Execution	${outMsg}
    
    
    
Login to Put Back Station and scan valid SKU
	[Documentation]	Verify that user is able to scan valid sku after login with valid credentials
					
	${host}=	Get Variable Value	${vm_ip}	${sanity.host}
	Log	${host}
	
	${output}=	loginApi	${host}	${sanity.user}	${sanity.password}	${ppsput}	pps_side=back
	
	@{list1}=	Convert To List	${output}
	${status}=	Get From List	${list1}	0
	Log	${status}
	
	${outMsg}=	Catenate	Unable able to login to ppsput back station
	Run Keyword If	${status} != 200	Fail	${outMsg}
	
	${output}=	processBarcode	${host}	${sanity.user}	${sanity.password}	${sanity.barcode}	${ppsput}	pps_side=back
 	Sleep	5
 	Log	${output}
 	
 	${successfulMsg}=	Catenate	Able to scan valid SKU
 	${failureMsg}=	Catenate	Unable to scan valid SKU
 	Run Keyword If	${output} is True	Pass Execution	${successfulMsg}	ELSE	Fail	${failureMsg}
	

Cancel Scan
	[Documentation]	Verify cancel scan is working fine
	Sleep	5
					
	${host}=	Get Variable Value	${vm_ip}	${sanity.host}
	Log	${host}
	
	${item_uid}=	cancelScan	${host}	${sanity.user}	${sanity.password}	${ppsput}	pps_side=back
	
	${successfulMsg}=	Catenate	Cancel Scan completed successfully
	Pass Execution	${successfulMsg}



