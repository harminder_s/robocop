***Settings ***
Library	apiHelper.py
Library	Collections	

*** Test Cases ***

Change PPS Mode
	[Documentation]	Changes the PPS Mode.
    ...                The keyword changes the PPS mode as specified in the config file.
    ${host}=	Get Variable Value	${vm_ip}	${sanity.host}
    alignPpsMode	${host}	${sanity.user}	${sanity.password}	${sanity.put}	${sanity.pick}	${sanity.audit}
	${outMsg}=	Catenate	Modes requested as, Put:  ${sanity.put}    Pick:    ${sanity.pick}    Audit:   ${sanity.audit} 
    Pass Execution	${outMsg} 
	
	
	
Get PPS ID and Mode
	[Documentation]	Get all PPS ID and Mode.
    ...                The keyword returns a dictionary with "put","pick" and "audit" as keys and their values as list.
    ${host}=	Get Variable Value	${vm_ip}	${sanity.host}
	${PpsDict}=	getPpsIdMode	${host}	${sanity.user}	${sanity.password}
	@{put}=	Get From Dictionary	${PpsDict}	put
	@{pick}=	Get From Dictionary	${PpsDict}	pick
	
	${ppsput}=	Get From List	${put}	0
	${ppspick}=	Get From List	${pick}	0
	
	Set Suite Variable	${ppsput}
	Set Suite Variable	${ppspick}
	${outMsg}=	Catenate	Modes, Put	${ppsput}	Pick:	${ppspick} 
    Pass Execution	${outMsg}
    
    
    
Login to Put Back Station, scan valid SKU and stage one item
	[Documentation]	Verify that user is able to scan valid sku after login with valid credentials
					
	${host}=	Get Variable Value	${vm_ip}	${sanity.host}
	Log	${host}
	
	${output}=	loginApi	${host}	${sanity.user}	${sanity.password}	${ppsput}	pps_side=back
	
	@{list1}=	Convert To List	${output}
	${status}=	Get From List	${list1}	0
	Log	${status}
	
	${outMsg}=	Catenate	Unable able to login to ppsput back station
	Run Keyword If	${status} != 200	Fail	${outMsg}
	
	${output}=	processBarcode	${host}	${sanity.user}	${sanity.password}	${sanity.barcode}	${ppsput}	pps_side=back
 	Sleep	5
 	Log	${output}
 	
 	${failureMsg}=	Catenate	Unable to scan valid SKU
 	Run Keyword If	${output} is False	Fail	${failureMsg}
 	
 	${pps_bin_id}=	processPpsbinEvent	${host}	${sanity.user}	${sanity.password}	ppsbin_state=empty	ppsbin_event=secondary_button_press	ppsid=${ppsput}	pps_side=back
    Sleep	5
    
    ${successMsg}=	Catenate	Successfully staged one item
    stageAllEvent	ppsid=${ppsput}	pps_side=back
    Pass Execution	${successMsg}
    
    
Verify Put Front
    [Documentation]    Verify Put Front Operation by adding items of one SKU.
    ...                The keyword returns a boolean. Pass is True and Fail is False.
    ${host}=    Get Variable Value    ${vm_ip}    ${sanity.host}
    Log	${host}
    
	loginApi	${host}	${sanity.user}	${sanity.password}	${ppsput}	pps_side=front
 	Sleep	5
 	
 	waitForMSU	${ppsput}	pps_side=front
 	Sleep	5
 	
 	${barcode}=	getProductBarcodeToScan	${host}	${sanity.user}	${sanity.password}	${ppsput}	pps_side=front
 	
  	processBarcode	${host}	${sanity.user}	${sanity.password}	${barcode}	${ppsput}	pps_side=front
  	Sleep	5
  	
  	@{rack_slots}=	getRackSlotBarcode	${host}	${sanity.user}	${sanity.password}	${ppsput}	pps_side=front	
  	${rack_barcode}=	Get From List	${rack_slots}	0
  	
 	processBarcode	${host}	${sanity.user}	${sanity.password}	${rack_barcode}	${ppsput}	pps_side=front
    Sleep	5
    
    ${outMsg}=    Catenate    Successfully placed item at Rack: ${rack_barcode}
    Pass Execution    ${outMsg}
 	