***Settings ***
Library	apiHelper.py
Library	Collections	

*** Test Cases ***

Change PPS Mode
	[Documentation]	Changes the PPS Mode.
    ...                The keyword changes the PPS mode as specified in the config file.
    ${host}=	Get Variable Value	${vm_ip}	${sanity.host}
    alignPpsMode	${host}	${sanity.user}	${sanity.password}	${sanity.put}	${sanity.pick}	${sanity.audit}
	${outMsg}=	Catenate	Modes requested as, Put:  ${sanity.put}    Pick:    ${sanity.pick}    Audit:   ${sanity.audit} 
    Pass Execution	${outMsg} 
	
	
	
Get PPS ID and Mode
	[Documentation]	Get all PPS ID and Mode.
    ...                The keyword returns a dictionary with "put","pick" and "audit" as keys and their values as list.
    ${host}=	Get Variable Value	${vm_ip}	${sanity.host}
	${PpsDict}=	getPpsIdMode	${host}	${sanity.user}	${sanity.password}
	@{put}=	Get From Dictionary	${PpsDict}	put
	@{pick}=	Get From Dictionary	${PpsDict}	pick
	
	${ppsput}=	Get From List	${put}	0
	${ppspick}=	Get From List	${pick}	0
	
	Set Suite Variable	${ppsput}
	Set Suite Variable	${ppspick}
	${outMsg}=	Catenate	Modes, Put	${ppsput}	Pick:	${ppspick} 
    Pass Execution	${outMsg}

    
Login to Put Back Station, scan valid SKU and stage one item
	[Documentation]	Verify that user is able to scan valid sku after login with valid credentials
					
	${host}=	Get Variable Value	${vm_ip}	${sanity.host}
	Log	${host}
	
	${output}=	loginApi	${host}	${sanity.user}	${sanity.password}	${ppsput}	pps_side=back
	
	@{list1}=	Convert To List	${output}
	${status}=	Get From List	${list1}	0
	Log	${status}
	
	${outMsg}=	Catenate	Unable able to login to ppsput back station
	Run Keyword If	${status} != 200	Fail	${outMsg}
	
	${output}=	processBarcode	${host}	${sanity.user}	${sanity.password}	${sanity.barcode}	${ppsput}	pps_side=back
 	Sleep	5
 	Log	${output}
 	
 	${failureMsg}=	Catenate	Unable to scan valid SKU
 	Run Keyword If	${output} is False	Fail	${failureMsg}
 	
 	updateKQ	${host}	${sanity.user}	${sanity.password}	${ppsput}	pps_side=back	qty=3
 	Sleep	5
 	
 	${pps_bin_id}=	processPpsbinEvent	${host}	${sanity.user}	${sanity.password}	ppsbin_state=empty	ppsbin_event=secondary_button_press	ppsid=${ppsput}	pps_side=back
    Sleep	5
    
    ${successMsg}=	Catenate	Successfully staged one item
    stageOneItem	${host}	${sanity.user}	${sanity.password}	ppsid=${ppsput}	pps_side=back	pps_bin_id=${pps_bin_id}
    Pass Execution	${successMsg}
    
    
 
 
*** Test Cases *** 	
Login to PutFront, navigate to Exception and Cancel Exception

	[Documentation]	bst-319.Cancel Exception
	
	${host}=	Get Variable Value	${vm_ip}	${sanity.host}
	Log	${host}
	
	${output}=	loginApi	${host}	${sanity.user}	${sanity.password}	${ppsput}	pps_side=front
	
	@{list1}=	Convert To List	${output}
	${status}=	Get From List	${list1}	0
	Log	${status}
	
	${outMsg}=	Catenate	Unable able to login to ppsput front station
	Run Keyword If	${status} != 200	Fail	${outMsg}
	
	waitForMSU	${ppsput}	pps_side=front
 	Sleep	5 
	
	navigateToException	   ${host}	${sanity.user}	${sanity.password}	${ppsput}	pps_side=front	event_name=exception	exception_name=damaged_or_missing
	Sleep	2
	
	cancelException	${host}	${sanity.user}	${sanity.password}	${ppsput}	pps_side=front
	Sleep	2
	
	

Login to PutFront, navigate to Exception Raise Item Missing or Damaged Exception

	[Documentation]	bst-318.Put Front Exception - Item Missing or Damaged
	
	${host}=	Get Variable Value	${vm_ip}	${sanity.host}
	Log	${host}
	
	${output}=	loginApi	${host}	${sanity.user}	${sanity.password}	${ppsput}	pps_side=front
	
	@{list1}=	Convert To List	${output}
	${status}=	Get From List	${list1}	0
	Log	${status}
	
	${outMsg}=	Catenate	Unable able to login to ppsput front station
	Run Keyword If	${status} != 200	Fail	${outMsg}
	
	waitForMSU	${ppsput}	pps_side=front
 	Sleep	5 
	
	navigateToException	   ${host}	${sanity.user}	${sanity.password}	${ppsput}	pps_side=front	event_name=exception	exception_name=damaged_or_missing
	Sleep	2
	
	finishDamagedOrMissingException	${host}	${sanity.user}	${sanity.password}	${ppsput}	pps_side=front	event_name=put_front_exception	exception_name=damaged_or_missing	evnet_action=confirm_quantity_update	good_quantity=1	damaged_quantity=1	missing_quantity=1
	Sleep	2
	
	
	
	${barcode}=	getProductBarcodeToScan	${host}	${sanity.user}	${sanity.password}	${ppsput}	pps_side=front
 	
  	${output}=	processBarcode	${host}	${sanity.user}	${sanity.password}	${barcode}	${ppsput}	pps_side=front
  	Sleep	5
  	Log	${output}
 	
 	${failureMsg}=	Catenate	Unable to scan valid SKU
 	Run Keyword If	${output} is False	Fail	${failureMsg}
 	
 	${qty}=	getKQ	${host}	${sanity.user}	${sanity.password}	${ppsput}	pps_side=front
 	Sleep	5
 	
 	updateKQ	${host}	${sanity.user}	${sanity.password}	${ppsput}	pps_side=front	qty=${qty}
 	Sleep	5
  	
  	@{rack_slots}=	getRackSlotBarcode	${host}	${sanity.user}	${sanity.password}	${ppsput}	pps_side=front	
  	${rack_barcode}=	Get From List	${rack_slots}	0
  	
  	${output}=	processBarcode	${host}	${sanity.user}	${sanity.password}	${rack_barcode}	${ppsput}	pps_side=front
    Sleep	5
    
    Log	${output}
	
	
	