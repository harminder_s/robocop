***Settings ***
Library    jenkinoperations.py
Library    deployment.py

*** Test Cases ***
FireBuilds On JenkinServer
    [Documentation]    To Fire builds on Jenkin server and fetch the latest package
    connectJenkin
    ${createNew}=    Get Variable Value    ${createBuild}    False
    ${branch}=    Get Variable Value	${build.branch}	"dev"
    ${buildName}=    getlatestPackagesBuild    ${createNew}	${branch}
    Set Global Variable    ${buildName} 
    ${outMsg}=    catenate       ${buildName}    "created successfully"
    Pass Execution    ${outMsg}  
    