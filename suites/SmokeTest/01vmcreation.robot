***Settings ***
Library    ovirtvm.py
*** Test Cases ***
Create vm and get IP
    [Documentation]    To create and launch a VM and return the ip to be used in subsequent steps
    
    @{vm_info}=	Run Keyword If	'${vmtype.type}'=='ovirt'	createVM    ${ovirt.ip}    ${ovirt.user}    ${ovirt.password}    ${ovirt.vmname}    ${ovirt.instancetype}    ${ovirt.templatename}    ${ovirt.clustername}	ELSE	createVMWARE	${vmware.ip}	${vmware.user}	${vmware.password}	${vmware.vmname}	${vmware.templatename}
    #Run Keyword If	'${vmtype.type}'=='ovirt'	Log	${vmtype.type}	ELSE	Log	${vmtype.type}
    ${vm_ip}=	Set Variable	@{vm_info}[0]
    ${vmname}=	Set Variable	@{vm_info}[1]
   Log    ${vm_ip}
    Set Global Variable    ${vm_ip}   # 192.168.8.43
    ${outMsg}=    catenate       ${vmtype.type}-${vmname}    created successfully and the  IP is:    ${vm_ip}   
    Pass Execution    ${outMsg} 