***Settings ***
Library    deployment.py
Library    butlerserver.py

*** Test Cases ***

Prepare environment
   [Documentation]    To prepare environment and apt-get upgrade
     ${host}=    Get Variable Value    ${vm_ip}    ${smokeapi.host}
     initEnv   ${host}
    
Install Build on server
    [Documentation]    To fetch build from jenkin and install on server
    ${host}=    Get Variable Value    ${vm_ip}    ${smokeapi.host}
    ${build}=    Get Variable Value    ${buildName}    ${smokeapi.buildPath}
    Log    ${build}  
    installBuild   ${build}    ${host} 
    ${outMsg}=    catenate       ${build}    installed successfully on    ${host} 
    Pass Execution    ${outMsg} 
    
Update Default Seat Types Required
	[Documentation]	To update default_types_required in sys.config
	${host}=	Get Variable Value	${vm_ip}	${smokeapi.host}
	${out}=	updateDefaultTypesRequired	${host}
	

    
Configure Auth in files
    [Documentation]    To fetch build from jenkin and install on server
    ${host}=    Get Variable Value    ${vm_ip}    ${smokeapi.host}
    ${auth}=	Get Variable Value	${smokeapi.auth}
    configureAuth	${auth}	${host} 
    ${outMsg}=    catenate	Auth IP added successfully on    ${host} 
    Pass Execution    ${outMsg} 
    
Configure Build on server
    [Documentation]    To fetch build from jenkin and install on server
    ${host}=    Get Variable Value    ${vm_ip}    ${smokeapi.host}
    configureBuild	${host} 
    ${outMsg}=    catenate	Build configured successfully on    ${host} 
    Pass Execution    ${outMsg} 
    
Installation Validation
    [Documentation]    To check whether all the services up after installation or not 
    ${host}=    Get Variable Value    ${vm_ip}    ${smokeapi.host}
    ${out}=    allservices   ${host} 
    Run keyword if    ${out} is False   Fatal Error    "Services are not up. Exiting..." 
    Sleep    180 
    
Add Butlers
    [Documentation]    To check whether able to add butlers or not 
    ${host}=    Get Variable Value    ${vm_ip}    ${smokeapi.host}
    ${out}=    addBots    ${host}    ${smokeapi.butlerCount}
    Run keyword if    ${out} is False   Fatal Error    "Not Able to Add Bots...." 
