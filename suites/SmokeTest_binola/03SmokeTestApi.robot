***Settings ***
Library	apiHelper.py
Library	Collections	
Library    ppsoperations.py


*** Test Cases ***	
Change PPS Mode
	[Documentation]    Changes the PPS Mode.
    ...                The keyword changes the PPS mode as specified in the config file.
    Sleep    30
    ${host}=    Get Variable Value    ${vm_ip}    ${smokeapi.host}
    alignPpsMode	${host}	${smokeapi.user}	${smokeapi.password}	${smokeapi.put}	${smokeapi.pick}	${smokeapi.audit}
	${outMsg}=    Catenate    Modes requested as, Put:  ${smokeapi.put}    Pick:    ${smokeapi.pick}    Audit:   ${smokeapi.audit} 
    Pass Execution    ${outMsg} 
	
	
Get PPS ID and Mode
	[Documentation]    Get all PPS ID and Mode.
    ...                The keyword returns a dictionary with "put","pick" and "audit" as keys and their values as list.
    ${host}=    Get Variable Value    ${vm_ip}    ${smokeapi.host}
	${PpsDict}=	getPpsIdMode	${host}	${smokeapi.user}	${smokeapi.password}
	@{put}=	Get From Dictionary	${PpsDict}	put
	@{pick}=	Get From Dictionary	${PpsDict}	pick
	
	${ppsput}=	Get From List	${put}	0
	${ppspick}=	Get From List	${pick}	0
	
	Set Suite Variable    ${ppsput}
	Set Suite Variable    ${ppspick}
	${outMsg}=    Catenate    Modes, Put:  ${ppsput}    Pick:    ${ppspick} 
    Pass Execution    ${outMsg}
	
	
Verify Put Back
    [Documentation]    Verify Put Back Operation by adding items of one SKU.
    ...                The keyword returns a boolean. Pass is True and Fail is False.
    ${host}=    Get Variable Value    ${vm_ip}    ${smokeapi.host}
    Log	${host}
    loginApi	${host}	${smokeapi.user}	${smokeapi.password}	${ppsput}	pps_side=back   
    Sleep	5
    
 	processBarcode	${host}	${smokeapi.user}	${smokeapi.password}	${smokeapi.barcode}	${ppsput}	pps_side=back
 	Sleep	5
 	
 	${pps_bin_id}=	processPpsbinEvent	${host}	${smokeapi.user}	${smokeapi.password}	ppsbin_state=empty	ppsbin_event=secondary_button_press	ppsid=${ppsput}	pps_side=back
    Sleep	5
        
    stageAllEvent	ppsid=${ppsput}	pps_side=back
    Sleep	5
    
    ${outMsg}=    Catenate    Staged Item placed on PPS Bin Id ${pps_bin_id}
    Pass Execution    ${outMsg}
    
Verify Put Front
    [Documentation]    Verify Put Front Operation by adding items of one SKU.
    ...                The keyword returns a boolean. Pass is True and Fail is False.
    ${host}=    Get Variable Value    ${vm_ip}    ${smokeapi.host}
    Log	${host}
    
	loginApi	${host}	${smokeapi.user}	${smokeapi.password}	${ppsput}	pps_side=front
 	Sleep	5
 	
 	waitForMSU	${ppsput}	pps_side=front
 	Sleep	5
 	
 	${barcode}=	getProductBarcodeToScan	${host}	${smokeapi.user}	${smokeapi.password}	${ppsput}	pps_side=front
 	
  	processBarcode	${host}	${smokeapi.user}	${smokeapi.password}	${barcode}	${ppsput}	pps_side=front
  	Sleep	5
  	
  	@{rack_slots}=	getRackSlotBarcode	${host}	${smokeapi.user}	${smokeapi.password}	${ppsput}	pps_side=front	
  	${rack_barcode}=	Get From List	${rack_slots}	0
  	
 	processBarcode	${host}	${smokeapi.user}	${smokeapi.password}	${rack_barcode}	${ppsput}	pps_side=front
    Sleep	5
    
    ${outMsg}=    Catenate    Successfully placed item at Rack: ${rack_barcode}
    Pass Execution    ${outMsg}

Place Order
	[Documentation]    Add One order by reading from the config File.
    ...                The keyword returns a boolean. Pass is True and Fail is False.
    ${host}=    Get Variable Value    ${vm_ip}    ${smokeapi.host}
	@{orders_list}=	add_order	${host}	${smokeapi.user}	${smokeapi.password}	${smokeapi.configfile}	spreadsheet1=orders_demo	spreadsheet2=orderlines_demo
 	Sleep	5
 	
 	${outMsg}=    Catenate    Placed Orders: @{orders_list}
    Pass Execution    ${outMsg}
 	
Verify Pick Front
	[Documentation]    Verify Pick Front Mode by picking items of one order.
    ...                The keyword returns a boolean. Pass is True and Fail is False.
	${host}=    Get Variable Value    ${vm_ip}    ${smokeapi.host}
	Log	${host}
	${pick_obj}=    ppspickfrontlogin    ${host}    ${ppspick}    ${smokeapi.user}    ${smokeapi.password}
	Set Suite Variable    ${pick_obj}   
	
    loginApi	${host}	${smokeapi.user}	${smokeapi.password}	${ppspick}	pps_side=front   
    Sleep	5
    
    waitForMSU	${ppspick}	pps_side=front
    Sleep	5
    
    ${barcode}=	getProductBarcodeToScan	${host}	${smokeapi.user}	${smokeapi.password}	${ppspick}	pps_side=front
 	
  	processBarcode	${host}	${smokeapi.user}	${smokeapi.password}	${barcode}	${ppspick}	pps_side=front
  	Sleep	5
  	
  	${pps_bin_id}=	processPpsbinEvent	${host}	${smokeapi.user}	${smokeapi.password}	ppsbin_state=IN USE	ppsbin_event=primary_button_press	ppsid=${ppspick}	pps_side=front
    Sleep	5
    
    ${outMsg}=    Catenate    Successfully picked item ${barcode} and placed at PPS Bin Id: ${pps_bin_id}
    Pass Execution    ${outMsg}
    
Verify Pick Back
	[Documentation]    Verify Pick Back Mode by picking items of one order.
    ...                The keyword returns a boolean. Pass is True and Fail is False.
    
    ${host}=    Get Variable Value    ${vm_ip}    ${smokeapi.host}
    Log	${host}
    loginApi	${host}	${smokeapi.user}	${smokeapi.password}	${ppspick}	pps_side=back   
    Sleep	5
        
    ${pps_bin_id}=	processPpsbinEvent	${host}	${smokeapi.user}	${smokeapi.password}	ppsbin_state=pick_processed	ppsbin_event=secondary_button_press	ppsid=${ppspick}	pps_side=back
    Sleep	5
    ppspickfrontlogout    ${pick_obj}
    
    ${outMsg}=    Catenate    Successfully picked item from PPS Bin Id: ${pps_bin_id}
    Pass Execution    ${outMsg}  
    
    