***Settings ***
Library    butlerserver.py

*** Test Cases ***
  
Add Butlers
    [Documentation]    To check whether able to add butlers or not 
    ${host}=    Get Variable Value    ${vm_ip}    ${smokeapi.host}
    ${out}=    addBots    ${host}    ${smokeapi.butlerCount}
    Run keyword if    ${out} is False   Fatal Error    "Not Able to Add Bots...." 
