***Settings ***
Library	apiHelper.py
Library	Collections	

*** Test Cases ***
Bulk Add Sku
	[Documentation]    Add Bulk Sku by reading from the spreadsheet
	
	add_bulk_sku	${soakTest.host}	username=${soakTest.user}	password=${soakTest.password}	filename=${soakTest.configfile}	spreadsheet=${soakTest.sku_spreadsheet}	item_tags=${soakTest.sku_tags}
	