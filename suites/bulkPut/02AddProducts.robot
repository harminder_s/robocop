***Settings ***
Library	apiHelper.py
Library	Collections	

*** Test Cases ***
Bulk Add Products
	[Documentation]    Add Bulk Products by reading from the spreadsheet
	
	add_bulk_product	${soakTest.host}	username=${soakTest.user}	password=${soakTest.password}	filename=${soakTest.configfile}	spreadsheet=${soakTest.product_spreadsheet}	item_tags=${soakTest.product_tags}
