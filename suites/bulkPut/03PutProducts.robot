***Settings ***
Library	apiHelper.py
Library	bulkPut.py
Library	Collections	

*** Test Cases ***

Change PPS Mode
	[Documentation]    Changes the PPS Mode.
    ...                The keyword changes the PPS mode as specified in the config file.
    alignPpsMode	${soakTest.host}	${soakTest.user}	${soakTest.password}	${soakTest.put}	${soakTest.pick}	${soakTest.audit}
	${outMsg}=    Catenate    Modes requested as, Put:  ${soakTest.put}    Pick:    ${soakTest.pick}    Audit:   ${soakTest.audit} 
    Pass Execution    ${outMsg}

*** Test Cases ***
Bulk Put Operations
	[Documentation]    Performs bulk and parallel put operations on all PPS station in PUT mode
    ...                The keyword gets the products list from the excel sheet.
    
    @{error_sku}=	bulk_put_operations	${soakTest.host}	username=${soakTest.user}	password=${soakTest.password}	filename=${soakTest.configfile}	spreadsheet=${soakTest.test_data_spreadsheet}	pick=${soakTest.pick}	put=${soakTest.put}	item_tags=all
	${outMsg}=    Catenate    Successfully completed Put Back and Front Operations
    Pass Execution    ${outMsg}