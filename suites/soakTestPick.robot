***Settings ***
Library	apiHelper.py
Library	bulkPick.py
Library	Collections	

*** Test Cases ***

Change PPS Mode
	[Documentation]    Changes the PPS Mode.
    ...                The keyword changes the PPS mode as specified in the config file.
    alignPpsMode	${soakTest.host}	${soakTest.user}	${soakTest.password}	${soakTest.put}	${soakTest.pick}	${soakTest.audit}
	${outMsg}=    Catenate    Modes requested as, Put:  ${soakTest.put}    Pick:    ${soakTest.pick}    Audit:   ${soakTest.audit} 
    Pass Execution    ${outMsg}
    
Get PPS ID and Mode
	[Documentation]    Get all PPS ID and Mode.
    ...                The keyword returns a dictionary with "put","pick" and "audit" as keys and their values as list.
    
    ${PpsDict}=	getPpsIdMode	${soakTest.host}	${soakTest.user}	${soakTest.password}
	@{put}=	Get From Dictionary	${PpsDict}	put
	@{pick}=	Get From Dictionary	${PpsDict}	pick
	
	Set Suite Variable    @{put}
	Set Suite Variable    @{pick}
	${outMsg}=    Catenate    Modes, Put:  @{put}    Pick:    @{pick} 
    Pass Execution    ${outMsg} 


*** Test Cases ***  	
Place Bulk Order
	[Documentation]    Place bulk order by reading from a spreadsheet
    
    add_bulk_order	${soakTest.host}	username=${soakTest.user}	password=${soakTest.password}	filename=${soakTest.configfile}	spreadsheet=${soakTest.spreadsheet}	item_tags=all
	${outMsg}=    Catenate    Successfully created orders
    Pass Execution    ${outMsg}
    
*** Test Cases ***     
Bulk Pick Operations
	[Documentation]    Performs bulk and parallel pick operations on all PPS station in PICK mode
    
    bulk_pick_operations	${soakTest.host}	username=${soakTest.user}	password=${soakTest.password}	pick=${soakTest.pick}	put=${soakTest.put}
	${outMsg}=    Catenate    Successfully completed Pick Back and Front Operations
    Pass Execution    ${outMsg}