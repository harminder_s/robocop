***Settings ***
Library	bulkPut.py
Library	Collections	

*** Test Cases ***
Bulk Put Operations
	[Documentation]    Performs bulk and parallel put operations on all PPS station in PUT mode
    ...                The keyword gets the products list from the excel sheet.
    
    @{error_sku}=	bulk_put	192.168.8.89	username=admin	password=apj0702	filename=config_binola.xls	spreadsheet=products_test3	pick=1	put=2,4,5
	${outMsg}=    Catenate    Successfully completed Put Back and Front Operations
    Pass Execution    ${outMsg}