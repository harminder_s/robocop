***Settings ***
Library    apiHelper.py
Library    Collections	
Library    ppsoperations.py


*** Test Cases ***	
	

	
Get PPS ID and Mode
	[Documentation]    Get all PPS ID and Mode.
    ...                The keyword returns a dictionary with "put","pick" and "audit" as keys and their values as list.
    ${host}=    Get Variable Value    ${vm_ip}    ${smokeapi.host}
	${PpsDict}=	getPpsIdMode	${host}	${smokeapi.user}	${smokeapi.password}
	@{put}=	Get From Dictionary	${PpsDict}	put
	@{pick}=	Get From Dictionary	${PpsDict}	pick
	
	${ppsput}=	Get From List	${put}	0
	${ppspick}=	Get From List	${pick}	0
	
	Set Suite Variable    ${ppsput}
	Set Suite Variable    ${ppspick}
	${outMsg}=    Catenate    Modes, Put:  ${ppsput}    Pick:    ${ppspick} 
    Pass Execution    ${outMsg}
	
	
Verify Put Back
    [Documentation]    Verify Put Back Operation by adding items of one SKU.
    ...                The keyword returns a boolean. Pass is True and Fail is False.
   
    ${host}=    Get Variable Value    ${vm_ip}    ${smokeapi.host}
    Log	${host}
    ${binNum}=    ppsputback    ${smokeapi.barcode}    ${host}    ${ppsput}    ${smokeapi.user}    ${smokeapi.password}  
    ${outMsg}=    Catenate    Barcode Scanned  ${smokeapi.barcode}    :placed at bin    ${binNum}
    Pass Execution    ${outMsg} 
    

Verify Put Front
    [Documentation]    Verify Put Front Operation by adding items of one SKU.
    ...                The keyword returns a boolean. Pass is True and Fail is False.
    
    ${host}=    Get Variable Value    ${vm_ip}    ${smokeapi.host}
    Log	${host}
    ${slot}=    ppsputfront    ${smokeapi.barcode}    ${host}    ${ppsput}    ${smokeapi.user}    ${smokeapi.password}   
    ${outMsg}=    Catenate    Barcode Scanned  ${smokeapi.barcode}    :placed at slot   ${slot}
    Pass Execution    ${outMsg} 

Place Order
	[Documentation]    Add One order by reading from the config File.
    ...                The keyword returns a boolean. Pass is True and Fail is False.
    ${host}=    Get Variable Value    ${vm_ip}    ${smokeapi.host}
    Log	${host}
	add_order	${host}	${smokeapi.user}	${smokeapi.password}	${smokeapi.configfile}	spreadsheet1=orders_demo	spreadsheet2=orderlines_demo
 	
 	
Verify Pick Front
	[Documentation]    Verify Pick Front Mode by picking items of one order.
    ...                The keyword returns a boolean. Pass is True and Fail is False.
	
	${host}=    Get Variable Value    ${vm_ip}    ${smokeapi.host}
    Log	${host}
    ${binNum}=    ppspickfront    ${smokeapi.barcode}    ${host}    ${ppspick}    ${smokeapi.user}    ${smokeapi.password} 
    ${outMsg}=    Catenate    Barcode Scanned  ${smokeapi.barcode}    :placed at bin   ${binNum}
    Pass Execution    ${outMsg}   
    
    
Verify Pick Back
	[Documentation]   Verify Pick Back Mode by picking items of one order.
    ...                The keyword returns a boolean. Pass is True and Fail is False.
    
    ${host}=    Get Variable Value    ${vm_ip}    ${smokeapi.host}
    Log	${host}
    ${binNum}=    ppspickback    ${host}    ${ppspick}    ${smokeapi.user}    ${smokeapi.password}   
    ${outMsg}=    Catenate    pptl pressed for    ${binNum}
    Pass Execution    ${outMsg}
    
    
    