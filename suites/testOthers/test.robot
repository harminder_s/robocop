***Settings ***
Library    a.py


*** Keywords ***
Create vm and get IP
    [Documentation]    To create and launch a VM and return the ip to be used in subsequent steps
    @{vm_info}=    afunc
    ${vm_ip}=	Set Variable	@{vm_info}[0]
    ${vmname}=	Set Variable	@{vm_info}[1]
    Log    ${vm_ip}
    Set Global Variable    ${vm_ip}   # 192.168.8.43
    ${outMsg}=    catenate       ${vmname}    created successfully. IP :    ${vm_ip}  
    
    Pass Execution    ${outMsg} 
  
*** Testcases ***  
BST01
	Create vm and get IP
	Set Suite Variable	&{results}	${TEST_NAME}=pass
	
BST02
	&{results}=	get variables
	Log	&{results}
	
    
    
    




