***Settings ***
Library    ovirtvm.py
Library    deployment.py

*** Test Cases ***
Create vm and get IP
    [Documentation]    To create and launch a VM and return the ip to be used in subsequent steps
    ${vm_ip}=    createVM    ${ovirt.ip}    ${ovirt.user}    ${ovirt.password}    ${ovirt.vmname}    ${ovirt.instancetype}    ${ovirt.templatename}    ${ovirt.clustername}
    Log    ${vm_ip}
    Set Suite Variable    ${vm_ip}    #192.168.3.163
    
Prepare environment
    [Documentation]    To prepare environment and apt-get upgrade
     initEnv   ${vm_ip}
    
Install Build on server
    [Documentation]    To fetch build from jenkin and install on server
    #${build}=    getlatestBuildPath    ${jenkin.ip}    ${jenkin.buildpath}    ${jenkin.user}    ${jenkin.password}
    Set Suite Variable    ${build}    /home/demo/workspace/test/goal/tmp/27-04-2016/installPackage.tgz
    Log    ${build}  
    installBuild   ${build}    ${vm_ip} 
    